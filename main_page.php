<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
require_once "config.php";

?>

<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title>Облік виробітку</title>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <link rel="javascript" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" media="screen and (min-device-width: 501px)" href="css/main.css" />
    
    <link rel="stylesheet" type="text/css" href="/css/main.css">
    <script type="application/javascript" src="js/main.js"></script>
    <script type="application/javascript" src="js/tabs.js"></script>
    <script type="application/javascript" src="js/ajax.js"></script>

</head>
<body>
<div id="printableArea"> <svg id="barcode"  class="print_barcode"></svg></div>
    <div class="menu">
        <div class="title simple"><i class="fas fa-chevron-right"></i></div>
        <div class="page-header">

            <div class="top-bar">
                <b>Привіт <br> <span><?php echo htmlspecialchars($_SESSION["username"]);  echo  $_GET['test'];?></span></b>
                <a href="logout.php" class="logout">Вийти</a>
            </div>
            <div class="navbar">
                <div class="dropdown1">
                    <button class="dropbtn">Склади </button>
                    <div class="dropdown-skladu">
                        <ul>
                            <li> <a onclick="get_coming_stor1(); addContentComingToStor1()">Прихід на Склад 1</a></li>
                            <li> <a onclick="get_coming_stor2(); addContentComingToStor2()">Прихід на Склад 2</a></li>
                            <li><a onclick="coming_stor1(); addContentComingStor1()">Склад 1</a></li>
                            <li><a onclick="get_consumption_stor1(); addContentConsumptionFromStor1()">Видача швеї</a></li>
                            <li><a onclick="coming_stor2(); addContentComingStor2()">Склад 2</a></li>
                            <li><a onclick="get_consumption_stor2(); addContentConsumptionFromStor2()">Операція шиття</a></li>
                            <li><a onclick="coming_stor3(); addContentComingStor3()">Склад 3</a></li>
                            <li><a onclick="coming_stor4(); addContentComingStor4()">Склад 4</a></li>
                        </ul>
                       
                       
                    </div>
                     
                </div>
                <div class="dropdown">
                    <button class="dropbtn">Облік Виробітку</button>
                </div>
                 <div class="dropdown">
                     <button class="dropbtn">Пряжа</button>
                 </div>
                 <div class="dropdown">
                     <button class="dropbtn">Пакувальні</button>
                 </div>
                 <div class="dropdown">
                     <button class="dropbtn">Зарплата</button>
                 </div>
                 <div class="dropdown">
                    <button class="dropbtn">Довідник</button>
                    <ul class="dropdown-dovidnuku">
                        <li><a onclick="get_operation(); addContentOperation()">Операції</a></li>
                        <li><a onclick="get_employees(); addContentEmployees()">Працівники</a></li>
                        
                        <li class="active"><a>Параметри <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <div class="submenu submenu-paramet">
                              <ul>
                                  <li><a onclick="get_type(); addContentType()">Типи</a></li>
                                  <li><a onclick="get_assortment(); addContentAssortment()">Асортименти</a></li>
                                  <li><a onclick="get_class(); addContentClass()">Класи</a></li>
                                  <li><a onclick="get_season(); addContentSeason()">Сезони</a></li>
                                  <li><a onclick="get_picture(); addContentPicture()">Зображення</a></li>
                                  <li><a onclick="get_size(); addContentSize()">Розміри</a></li>
                                  <li><a onclick="get_color(); addContentColor()">Кольори</a></li>
                                  <li><a onclick="get_article(); addContentArticle()">Артикули</a></li>
                              </ul>
                          </div>
                        </li>
                         <li class="active"><a>Пакувальні матеріали <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                <div class="submenu">
                                    <ul>
                                        <li><a onclick="get_packingMaterials(); addContentPackingMaterials()">Пакувальні матеріали</a></li>
                                        <li><a onclick="get_packingMaterialsProvider(); addContentPackingMaterialsProvider()">Постачальники</a></li>
                                        <li><a onclick="get_packingMaterialsConsumption(); addContentPackingMaterialsConsumption()">Споживачі</a></li>
                                        <li><a onclick="get_packingMaterialsTerm(); addContentPackingMaterialsTerm()">Розміри пакувальних матеріалів</a></li>
                                        <li><a onclick="get_packingMaterialsMeta(); addContentPackingMaterialsMeta()">Прихід / Рохід</a></li>
                                        <li><a onclick="get_packingMaterialsTermMeta(); addContentPackingMaterialsTermMeta()">Види розмірів</a></li>
                                    </ul>
                                </div>
                           </li>
   
                        
                        <li><a onclick="get_price(); addContentPrice()">Розцінка</a></li>
                        <li class="active"><a><span>Обладнання</span> <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            <div class="submenu">
                                <ul>
                                    <li><a  onclick="get_models(); addContentModels()">Моделі</a></li>
                                    <li><a  onclick="get_needles(); addContentNeedless()">Голки</a></li>
                                    <li><a  onclick="get_inches(); addContentInches()">Дюйми</a></li>
                                    <li><a  onclick="get_knitting_machines(); addContentKnittingMachines()">Машини</a></li>                                                      
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="dropdown2">
                    <button class="dropbtn">Адміністрування </button>
                    <div class="dropdown-administ">
                        <a  href="register.php">Добавити користувача</a>
                    </div>
                </div>

            </div>

        </div>
    </div>

   

    <div id="wrapperBtns">
        <ul id="BtnsUl" class="tabs clearfix">
            <li id="operationsBtn" onclick="get_operation()"><a onclick="addContentOperation(); changeTabs()">Операції</a><span class="close">&times;</span></li>
            <li id="employeesBtn" onclick="get_employees()"><a onclick="addContentEmployees(); changeTabs()">Працівники</a><span class="close">&times;</span></li>
            <li id="modelsBtn" onclick="get_models()"><a onclick="addContentModels(); changeTabs()">Моделі</a><span class="close">&times;</span></li>
            <li id="needlesBtn" onclick="get_needles()"><a onclick="addContentNeedless(); changeTabs()">Голки</a><span class="close">&times;</span></li>
            <li id="inchesBtn" onclick="get_inches()"><a onclick="addContentInches(); changeTabs()">Дюйми</a><span class="close">&times;</span></li>
            <li id="machinesBtn" onclick="get_knitting_machines()"><a onclick="addContentKnittingMachines(); changeTabs()">Машини</a><span class="close">&times;</span></li>
            <li id="priceBtn" onclick="get_price()"><a onclick="addContentPrice(); changeTabs()">Розцінка</a><span class="close">&times;</span></li>
            <li id="comingToStor1Btn" onclick="get_coming_stor1()"><a onclick="addContentComingToStor1(); changeTabs()">Прихід на Склад 1</a><span class="close">&times;</span></li>
            <li id="comingStor1Btn" onclick="coming_stor1()"><a onclick="addContentComingStor1(); changeTabs()">Прихід - Склад 1</a><span class="close">&times;</span></li>
            <li id="typeBtn" onclick="get_type()"><a onclick="addContentType(); changeTabs()">Типи</a><span class="close">&times;</span></li>
           <li id="assortmentBtn" onclick="get_assortment()"><a onclick="addContentAssortment(); changeTabs()">Асортименти</a><span class="close">&times;</span></li>
           <li id="classBtn" onclick="get_class()"><a onclick="addContentClass(); changeTabs()">Класи</a><span class="close">&times;</span></li>
           <li id="seasonBtn" onclick="get_season()"><a onclick="addContentSeason(); changeTabs()">Сезони</a><span class="close">&times;</span></li>
           <li id="pictureBtn" onclick="get_picture()"><a onclick="addContentPicture(); changeTabs()">Зображення</a><span class="close">&times;</span></li>
           <li id="sizeBtn" onclick="get_size()"><a onclick="addContentSize(); changeTabs()">Розміри</a><span class="close">&times;</span></li>
           <li id="colorBtn" onclick="get_color()"><a onclick="addContentColor(); changeTabs()">Кольори</a><span class="close">&times;</span></li>
           <li id="articleBtn" onclick="get_article()"><a onclick="addContentArticle(); changeTabs()">Артикули</a><span class="close">&times;</span></li>
           <li id="packingMaterialsBtn" onclick="get_packingMaterials()"><a onclick="addContentPackingMaterials(); changeTabs()">Пакувальні матеріали</a><span class="close">&times;</span></li>
           <li id="packingMaterialsProviderBtn" onclick="get_packingMaterialsProvider()"><a onclick="addContentPackingMaterialsProvider(); changeTabs()">Постачальники</a><span class="close">&times;</span></li>
           <li id="packingMaterialsConsumptionBtn" onclick="get_packingMaterialsConsumption()"><a onclick="addContentPackingMaterialsConsumption(); changeTabs()">Споживачі</a><span class="close">&times;</span></li>
           <li id="packingMaterialsTermBtn" onclick="get_packingMaterialsTerm()"><a onclick="addContentPackingMaterialsTerm(); changeTabs()">Розміри пакувальних матеріалів</a><span class="close">&times;</span></li>                           
           <li id="packingMaterialsMetaBtn" onclick="get_packingMaterialsMeta()"><a onclick="addContentPackingMaterialsMeta(); changeTabs()">Прихід / Рохід</a><span class="close">&times;</span></li>
           <li id="packingMaterialsTermMetaBtn" onclick="get_packingMaterialsTermMeta()"><a onclick="addContentPackingMaterialsTermMeta(); changeTabs()">Види розмірів</a><span class="close">&times;</span></li>
           <li id="ConsumptionStor1Btn" onclick="consumption_stor1()"><a onclick="addContentConsumptionStor1(); changeTabs()">Розхід - Склад 1</a><span class="close">&times;</span></li>
           <li id="ConsumptionFromStor1Btn" onclick="get_consumption_stor1()"><a onclick="addContentConsumptionFromStor1(); changeTabs()">Видача мішка швеї</a><span class="close">&times;</span></li>
           <li id="remainderStor1Btn" onclick="get_remainder_stor1()"><a onclick="addContentRemainderStor1(); changeTabs()">Залишок - Склад 1</a><span class="close">&times;</span></li>
            <li id="comingStor2Btn" onclick="coming_stor2()"><a onclick="addContentComingStor2(); changeTabs()">Прихід - Склад 2</a><span class="close">&times;</span></li>
             <li id="comingToStor2Btn" onclick="get_coming_stor2()"><a onclick="addContentComingToStor2(); changeTabs()">Прихід на Склад 2</a><span class="close">&times;</span></li>
            <li id="ConsumptionStor2Btn" onclick="consumption_stor2()"><a onclick="addContentConsumptionStor2(); changeTabs()">Розхід - Склад 2</a><span class="close">&times;</span></li>
            <li id="ConsumptionFromStor2Btn" onclick="get_consumption_stor2()"><a onclick="addContentConsumptionFromStor2(); changeTabs()">Операція шиття</a><span class="close">&times;</span></li>
            <li id="remainderStor2Btn" onclick="get_remainder_stor2()"><a onclick="addContentRemainderStor2(); changeTabs()">Залишок - Склад 2</a><span class="close">&times;</span></li>

            <li id="comingStor3Btn" onclick="coming_stor3()"><a onclick="addContentComingStor3(); changeTabs()">Прихід - Склад 3</a><span class="close">&times;</span></li>
            <li id="ConsumptionStor3Btn" onclick="consumption_stor3()"><a onclick="addContentConsumptionStor3(); changeTabs()">Розхід - Склад 3</a><span class="close">&times;</span></li>
            <li id="remainderStor3Btn" onclick="get_remainder_stor3()"><a onclick="addContentRemainderStor3(); changeTabs()">Залишок - Склад 3</a><span class="close">&times;</span></li>

             <li id="comingStor4Btn" onclick="coming_stor4()"><a onclick="addContentComingStor4(); changeTabs()">Прихід - Склад 4</a><span class="close">&times;</span></li>
            <li id="ConsumptionStor4Btn" onclick="consumption_stor4()"><a onclick="addContentConsumptionStor4(); changeTabs()">Розхід - Склад 4</a><span class="close">&times;</span></li>
            <li id="remainderStor4Btn" onclick="get_remainder_stor4()"><a onclick="addContentRemainderStor4(); changeTabs()">Залишок - Склад 4</a><span class="close">&times;</span></li>
        </ul>
    </div>

    <script>

        var closebtns = document.getElementsByClassName("close");
        var i;

        for (i = 0; i < closebtns.length; i++) {
            closebtns[i].addEventListener("click", function() {
            var container = document.getElementById("conteiner");
            this.parentElement.style.display = "none";
            container.style.display = "none";
            });
        }

    </script>


    <div id="conteiner"> </div>
    

</body>

</html>