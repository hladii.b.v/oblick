<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

//tables

$table_employees = 'dov_employees';
$table_employees_meta = 'employees_meta';

//**selects


//employeers
$result_seamstress = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='4' ORDER BY e.employees_id");


//**return


//employeers
$all_seamstress = '';
if ($result_seamstress->num_rows > 0) {
	while($row_seamstress = $result_seamstress->fetch_assoc()) {
		$full_name = '';
		$first_name = substr($row_seamstress['first_name'], 0, 2);
		$surname =  substr($row_seamstress['surname'], 0, 2);

		$full_name =  $row_seamstress['last_name'].' '.$first_name.'.'. $surname.'.';
		$all_seamstress .= '<option value="'. $row_seamstress['employees_id'].'">'. $full_name .'</option>';
	}
}
else {
	$all_seamstress = '<option>Немає Швей</option>';
}

?>
<link rel="stylesheet" href="css/main.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div class="content">
	<h3>Видача мішка швеї</h3>
	<hr>
	<div>
		<label>Код міщка: <input type="number" name="bag_id" id="bag_id" ></label>
		<hr>
		<label>Дата приходу: <input type="date" name="add_date" id="add_date" value="<?echo $today;?>"></label>
		<label>Швея: <select id="add_seamstress"><?echo $all_seamstress;?></select></label>
		<label><button class="addBtn" onclick="add_new_consumption_from_stor1()">Додати прихід</button></label>
	</div>
</div>



