<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

//tables
$table_knitting_machines = 'dov_knitting_machines';
$table_needles = 'dov_needles';
$table_models = 'dov_models';
$table_employees = 'dov_employees';
$table_employees_meta = 'employees_meta';
$table_article = 'dov_article';
$table_picture = 'dov_picture';
$table_size = 'dov_size';
$table_color = 'dov_color';
//**selects

//machines
$result_machines = $link->query("SELECT k.machines_id, k.machines_number, m.model_name, n.needles_name FROM $table_knitting_machines k LEFT JOIN  $table_models m ON k.model_id=m.model_id LEFT JOIN  $table_needles n ON k.needles_id=n.needles_id WHERE k.machines_status='1' ORDER BY k.machines_number");

//employeers
$result_master = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='2' ORDER BY e.employees_id");
$result_knitting = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='3'  ORDER BY e.employees_id ");

//parameters
$result_article = $link->query("SELECT * FROM $table_article ");
$result_picture = $link->query("SELECT * FROM $table_picture ");
$result_size = $link->query("SELECT * FROM $table_size ");
$result_color = $link->query("SELECT * FROM $table_color ");

$result_max_id = $link->query("SELECT bag_id  FROM bag_table ORDER BY bag_id DESC LIMIT 1 ");
while($row_max_id = $result_max_id->fetch_assoc()) {
		
		$max_id = $row_max_id['bag_id'] +1;
}

//**return

//machines
$all_machines = '';
if ($result_machines->num_rows > 0) {
	while($row_machines = $result_machines->fetch_assoc()) {
		
		$all_machines .= '<option value="'. $row_machines['machines_id'].'">#'.$row_machines['machines_number'].' '.$row_machines['model_name'].' ('. $row_machines['needles_name'].' г)</option>';
	}
}
else {
	$all_machines = '<option>Немає машин';
}

//employeers
$all_master = '';
if ($result_master->num_rows > 0) {
	while($row_master = $result_master->fetch_assoc()) {
		$full_name = '';
		$first_name = substr($row_master['first_name'], 0, 2);
		$surname =  substr($row_master['surname'], 0, 2);

		$full_name =  $row_master['last_name'].' '.$first_name.'.'. $surname.'.';
		$all_master .= '<option value="'. $row_master['employees_id'].'">'. $full_name .'</option>';
	}
}
else {
	$all_master = '<option>Немає Майстрів</option>';
}
$all_knitting = '';
if ($result_knitting->num_rows > 0) {
	while($row_knitting = $result_knitting->fetch_assoc()) {
		$full_name = '';
		$first_name = substr($row_knitting['first_name'], 0, 2);
		$surname =  substr($row_knitting['surname'], 0, 2);

		$full_name =  $row_knitting['last_name'].' '.$first_name.'.'. $surname.'.';
		$all_knitting .= '<option value="'. $row_knitting['employees_id'].'">'. $full_name .'</option>';
	}
}
else {
	$all_knitting = '<option>Немає Вязальниць</option>';
}

//parameters
$all_article = '';
if ($result_article->num_rows > 0) {
	while($row_article = $result_article->fetch_assoc()) {
		
		$all_article .= '<option value="'. $row_article['article_id'].'">'. $row_article['article_name'] .'</option>';
	}
}
else {
	$all_article = '<option>Немає Артикулів</option>';
}

$all_picture = '';
if ($result_picture->num_rows > 0) {
	while($row_picture = $result_picture->fetch_assoc()) {
		
		$all_picture .= '<option value="'. $row_picture['picture_id'].'">'. $row_picture['picture_name'] .'</option>';
	}
}
else {
	$all_picture = '<option>Немає Малюнків</option>';
}

$all_size = '';
if ($result_size->num_rows > 0) {
	while($row_size = $result_size->fetch_assoc()) {
		
		$all_size .= '<option value="'. $row_size['size_id'].'">'. $row_size['size_name'] .'</option>';
	}
}
else {
	$all_size = '<option>Немає Ромірів</option>';
}
$all_color = '';
if ($result_color->num_rows > 0) {
	while($row_color = $result_color->fetch_assoc()) {
		
		$all_color .= '<option value="'. $row_color['color_id'].'">'. $row_color['color_name'] .'</option>';
	}
}
else {
	$all_color = '<option>Немає Кльорів</option>';
}



?>
<link rel="stylesheet" href="css/main.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>




<div class="content">
	<h3>Прихід на Склад 1 </h3>
	<hr>
	<div>
		<label>Дата приходу: <input type="date" name="add_date" id="add_date" value="<?echo $today;?>"></label>
		<hr>
		<label>Вязальна машина: <select id="add_machines"><?echo $all_machines;?></select></label>
		<label>Майстер: <select id="add_master"><?echo $all_master;?></select></label>
		<label>Вязальниця: <select id="add_knitting"><?echo $all_knitting;?></select></label>
		<hr>
		<label>Артикул: <select id="add_article"><?echo $all_article;?></select></label>
		<label>Малюнок: <select id="add_picture"><?echo $all_picture;?></select></label>
		<label>Розмір: <select id="add_size"><?echo $all_size;?></select></label>
		<label>Колір: <select id="add_color"><?echo $all_color;?></select></label>
		<hr>
		<label>1 Гатунок: <input type="number" id="gatunoc_1"></label>
		<label>2 Гатунок: <input type="number" id="gatunoc_2"></select></label>
		<label>3 Гатунок: <input type="number" id="gatunoc_3"></label>
		<hr>
		<label><button class="addBtn" onclick="add_new_coming_to_stor1(<?echo $max_id;?>)">Додати прихід</button></label>
	</div>
</div>


