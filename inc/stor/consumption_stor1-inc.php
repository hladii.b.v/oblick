<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}


$get_consumption = $link->query("SELECT * FROM $store1_consumption_table WHERE `date`>='$datepicker_first' AND `date`<='$datepicker_last'" );
?>
<link rel="stylesheet" href="css/main.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div class="content">
	<h3>Склад 1 - Розхід </h3>
	<hr>
	<div class="date_set">
		<b><a onclick="coming_stor1(); addContentComingStor1()"> Прихід</a> <a class="select"  onclick="consumption_stor1()">Розхід</a> <a onclick="get_remainder_stor1(); addContentRemainderStor1()">Залишок</a></b><br>
		<hr>
		<b>Період з:<input type="date" id="datepicker_first" value="<?echo $datepicker_first?>"> Період до:<input type="date" id="datepicker_last" value="<?echo $datepicker_last?>"><a class="select_date_btn"  onclick="consumption_stor1_date()"> Обрати</a></b>
	</div>
</div>
<hr>

<div id="consumption-modal" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати працівника</h1>
    <h6>*введіть значення у поля</h6>
    <div class="add-form">
		<label>Дата приходу: <input type="date" name="add_date" id="add_date" value="<?echo $date;?>"></label>
		<hr>
		<label>Швеї: <select id="add_accepted"><?echo $all_accepted;?></select></label>		
		<hr>
		<label><button class="addBtn" onclick="add_new_consumption_to_stor1(<?echo $consumption_id?>)">Оновити розхід</button></label>
	</div>
  </div>
</div>
<?

if ($get_consumption->num_rows > 0) {
	echo '<table id="myTable" class="tableStor">
	    			
	    		<thead>
                     <tr>
	    				<th onclick="sortingAscDesc()"><input type="text" id="txt_code" class="filterinput" onclick="filtersort() "placeholder="Код"><br><span onclick="sortingAscDesc()">Код</span></th>
	    				<th onclick="sortingAscDesc()"><input type="text" class="filterinput" id="search_col2" onclick="filtersort()" placeholder="Дата"><br><span onclick="sortingAscDesc()">Дата</span></th>
	    				<th><input type="text" id="search_col3" class="filterinput" onclick="filtersort()" placeholder="Ділянка"><br>Ділянка</th>
	    				<th><input type="text" id="search_col4" class="filterinput" onclick="filtersort()" placeholder="Швея"><br>Швея</th>
	    				<th><input type="text" id="search_col5" class="filterinput" onclick="filtersort()" placeholder="Артикул"><br>Артикул</th>
	    				<th><input type="text" id="search_col6" class="filterinput" onclick="filtersort()" placeholder="Клас"><br>Клас</th>
	    				<th><input type="text" id="search_col7" class="filterinput" onclick="filtersort()" placeholder="Тип"><br>Тип</th>
	    				<th><input type="text" id="search_col8" class="filterinput" onclick="filtersort()" placeholder="Асортимент"><br>Асортимент</th>
	    				<th><input type="text" id="search_col9" class="filterinput" onclick="filtersort()" placeholder="Малюнок"><br>Малюнок</th>
	    				<th><input type="text" id="search_col10" class="filterinput" onclick="filtersort()" placeholder="Колір"><br>Колір</th>
	    				<th><input type="text" id="search_col11" class="filterinput" onclick="filtersort()" placeholder="Розмір"><br>Розмір</th>
	    				<th style="display:none"></th>
	    				<th><input type="text" id="search_col13" class="filterinput gatun_input" onclick="filtersort()" ><br>1г</th>
	    				<th><input type="text" id="search_col14" class="filterinput gatun_input" onclick="filtersort()" ><br>2г</th>
	    				<th><input type="text" id="search_col15" class="filterinput gatun_input" onclick="filtersort()" ><br>3г</th>
	    				<th><input type="text" id="search_col16" class="filterinput gatun_input" onclick="filtersort()" ><br>Разом</th>
	    				<th><input type="text" id="search_col17" class="filterinput" onclick="filtersort()" placeholder="Дата редагування"><br>Дата редагування</th>
	    				<th><input type="text" id="search_col18" class="filterinput" onclick="filtersort()" placeholder="Операто"><br>Операто</th>
	    				<th></th>
	    			</tr>

                </thead><tbody>';
    $quanteti_total = 0;
    $coun1_total = 0;
	$coun2_total = 0;
	$coun3_total = 0;
	$bag_sum_total = 0;	
    while($row = $get_consumption->fetch_assoc()) {
    	$consumption_id = $row['consumption_id'];
    	$bag_id = $row['bag_id'];
    	$accepted = $row['accepted'];
    	$user_id = $row['user_id'];

    
    	$result_accepted = $link->query("SELECT first_name, last_name, surname FROM  $table_employees WHERE  employees_id='$accepted'");
    	$result_username = $link->query("SELECT username FROM  $table_users WHERE  id='$user_id'");
    	$result_bag = $link->query("SELECT * FROM  $bag_table WHERE  bag_id='$bag_id'");

    	

    	
		while($row_accepted= $result_accepted->fetch_assoc()) {
			$full_name = '';
			$first_name = substr($row_accepted['first_name'], 0, 2);
			$surname =  substr($row_accepted['surname'], 0, 2);

			$full_name_accepted =  $row_accepted['last_name'].' '.$first_name.'.'. $surname.'.';
			
		}
		while($row_result_username = $result_username->fetch_assoc()) {			
			$username = $row_result_username['username'];
		}

		while($row_result_bag = $result_bag->fetch_assoc()) {
			$article_id = $row_result_bag['article_id'];
			$picture_id = $row_result_bag['picture_id'];
			$size_id = $row_result_bag['size_id'];
			$color_id = $row_result_bag['color_id'];
			$coun1 = $row_result_bag['coun1'];
			$coun2 = $row_result_bag['coun2'];
			$coun3 = $row_result_bag['coun3'];
			$bag_sum = 	$coun1 + $coun2 + $coun3;		
		}


		$result_article = $link->query("SELECT * FROM $table_article WHERE `article_id`='$article_id'");			
		while($row_result_article = $result_article->fetch_assoc()) {			
			$article_name = $row_result_article['article_name'];
			$type_id = $row_result_article['type_id'];
			$assortment_id = $row_result_article['assortment_id'];
			$class_id = $row_result_article['class_id'];
		}

		$result_picture = $link->query("SELECT * FROM $table_picture WHERE `picture_id`='$picture_id'");			
		while($row_result_picture = $result_picture->fetch_assoc()) {			
			$picture_name = $row_result_picture['picture_name'];
		}

		$result_size = $link->query("SELECT * FROM $table_size WHERE `size_id`='$size_id'");			
		while($row_result_size = $result_size->fetch_assoc()) {			
			$size_name = $row_result_size['size_name'];
		}

		$result_color = $link->query("SELECT * FROM $table_color WHERE `color_id`='$color_id'");			
		while($row_result_color = $result_color->fetch_assoc()) {			
			$color_name = $row_result_color['color_name'];
		}

		$result_class = $link->query("SELECT * FROM $table_class WHERE `class_id`='$class_id'");			
		while($row_result_class = $result_class->fetch_assoc()) {			
			$class_name = $row_result_class['class_name'];
		}

		$result_type = $link->query("SELECT * FROM $table_type WHERE `type_id`='$type_id'");			
		while($row_result_type = $result_type->fetch_assoc()) {			
			$type_name = $row_result_type['type_name'];
		}

		$result_assortment = $link->query("SELECT * FROM $table_assortment WHERE `assortment_id`='$assortment_id'");			
		while($row_result_assortment = $result_assortment->fetch_assoc()) {			
			$assortment_name = $row_result_assortment['assortment_name'];
		}

		while ( strlen($bag_id) < 6) {
			$bag_id = '0' . $bag_id;
		}
        echo  '<tr>
        			<td >' . $bag_id . '</td>
        			<td>' . $row['date'] . '</td>
        			<td>' . $row['area'] . '</td>
        			<td>'. $full_name_accepted . '</td>
        			<td>'.$article_name.'</td>
        			<td>'.$class_name.'</td>
        			<td>'.$type_name.'</td>
        			<td>'.$assortment_name.'</td>
        			<td>'.$picture_name.'</td>
        			<td>'.$color_name.'</td>
        			<td>'.$size_name.'</td>
	    			<td style="display:none"></td>
        			<td >'.$coun1.'</td>
        			<td>'.$coun2.'</td>
        			<td>'.$coun3.'</td>
        			<td>'.$bag_sum.'</td>
        			<td>'. $row['curent_date'] . '</td>
        			<td>'. $username . '</td>
        			<td class="editionofempl"> 
        				<button class="edit_row editBtn" onclick="edit_consumption_modal1('. $row['bag_id'].')">Редагувати</button>
        				<button class="delete_row deleteBtn" onclick="delete_consumption1('. $row['bag_id'].')">Видалити</button>
        			</td>
        		</tr>';
        $quanteti_total++;
		$coun1_total = $coun1_total + $coun1;
		$coun2_total = $coun2_total + $coun2;
		$coun3_total = $coun3_total + $coun3;
		$bag_sum_total = $bag_sum_total + $bag_sum;	
        
  	 }

  	echo '</tbody><tfoot><tr><td  id="val">'. $quanteti_total.'</td><td></td><td></td><td></td><td></td><td></td><td><td </td><td style="display:none"></td></td><td></td><td></td><td></td><td id="row_index1">'. $coun1_total.'</td><td  id="row_index2">'.$coun2_total.'</td><td  id="row_index3">'.$coun3_total.'</td><td  id="row_index4">'.$bag_sum_total.'</td><td></td><td></td><td></td></tr></tfoot></tbody></table>';
    
	    
}
else {
	   echo "Немає Розходу";
}