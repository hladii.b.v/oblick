<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

//tables

$table_employees = 'dov_employees';
$table_employees_meta = 'employees_meta';
$table_article = 'dov_article';
$table_picture = 'dov_picture';
$table_size = 'dov_size';
$table_color = 'dov_color';

//**selects


//employeers
$result_seamstress = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='4' ORDER BY e.employees_id");

//parameters
$result_article = $link->query("SELECT * FROM $table_article ");
$result_picture = $link->query("SELECT * FROM $table_picture ");
$result_size = $link->query("SELECT * FROM $table_size ");
$result_color = $link->query("SELECT * FROM $table_color ");


//**return


//employeers
$all_seamstress = '';
if ($result_seamstress->num_rows > 0) {
	while($row_seamstress = $result_seamstress->fetch_assoc()) {
		$full_name = '';
		$first_name = substr($row_seamstress['first_name'], 0, 2);
		$surname =  substr($row_seamstress['surname'], 0, 2);

		$full_name =  $row_seamstress['last_name'].' '.$first_name.'.'. $surname.'.';
		$all_seamstress .= '<option value="'. $row_seamstress['employees_id'].'">'. $full_name .'</option>';
	}
}
else {
	$all_seamstress = '<option>Немає Швей</option>';
}

//parameters
$all_article = '';
if ($result_article->num_rows > 0) {
	while($row_article = $result_article->fetch_assoc()) {
		
		$all_article .= '<option value="'. $row_article['article_id'].'">'. $row_article['article_name'] .'</option>';
	}
}
else {
	$all_article = '<option>Немає Артикулів</option>';
}

$all_picture = '';
if ($result_picture->num_rows > 0) {
	while($row_picture = $result_picture->fetch_assoc()) {
		
		$all_picture .= '<option value="'. $row_picture['picture_id'].'">'. $row_picture['picture_name'] .'</option>';
	}
}
else {
	$all_picture = '<option>Немає Малюнків</option>';
}

$all_size = '';
if ($result_size->num_rows > 0) {
	while($row_size = $result_size->fetch_assoc()) {
		
		$all_size .= '<option value="'. $row_size['size_id'].'">'. $row_size['size_name'] .'</option>';
	}
}
else {
	$all_size = '<option>Немає Ромірів</option>';
}
$all_color = '';
if ($result_color->num_rows > 0) {
	while($row_color = $result_color->fetch_assoc()) {
		
		$all_color .= '<option value="'. $row_color['color_id'].'">'. $row_color['color_name'] .'</option>';
	}
}
else {
	$all_color = '<option>Немає Кльорів</option>';
}
?>
<link rel="stylesheet" href="css/main.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div class="content">
	<h3>Видача мішка швеї</h3>
	<hr>
	<div>
		<label>Дата приходу: <input type="date" name="add_date" id="add_date" value="<?echo $today;?>"></label>
		<label>Швея: <select id="add_seamstress"><?echo $all_seamstress;?></select></label>
		<hr>
		<label>Артикул: <select id="add_article"><?echo $all_article;?></select></label>
		<label>Малюнок: <select id="add_picture"><?echo $all_picture;?></select></label>
		<label>Розмір: <select id="add_size"><?echo $all_size;?></select></label>
		<label>Колір: <select id="add_color"><?echo $all_color;?></select></label>
		<hr>
		<label>1 Гатунок: <input type="number" id="gatunoc_1"></label>
		<label>2 Гатунок: <input type="number" id="gatunoc_2"></select></label>
		<label>3 Гатунок: <input type="number" id="gatunoc_3"></label>
		<label><button class="addBtn" onclick="add_new_coming_from_stor2()">Додати прихід</button></label>
	</div>
</div>



