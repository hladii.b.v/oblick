<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}


$get_consumption = $link->query("SELECT * FROM $stor2_remainder_table WHERE `coming_date`<='$datepicker_first' AND (`consumption_date`>'$datepicker_first' OR `consumption_date` IS NULL ) " );
?>
<link rel="stylesheet" href="css/main.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div class="content">
	<h3>Склад 2 - Залишок </h3>
	<hr>
	<div class="date_set">
		<b><a onclick="coming_stor2(); addContentComingStor2()"> Прихід</a> <a onclick="consumption_stor2(); addContentConsumptionStor2()">Розхід</a> <a class="select"  onclick="get_remainder_stor2()">Залишок</a></b><br>
		<hr>
		<b>На дату:<input type="date" id="datepicker_first" value="<?echo $datepicker_first?>"><a  onclick="remainder_stor2_date()" class="select_date_btn"> Обрати</a></b>
	</div>
</div>
<hr >


<?

if ($get_consumption->num_rows > 0) {
	echo '<table id="myTable" class="tableStor">
	    			
	    		<thead>
                     <tr>
	    				<th onclick="sortingAscDesc()"><input type="text" id="txt_code" class="filterinput" onclick="filtersort() "placeholder="Код"><br><span onclick="sortingAscDesc()">Код</span></th>
	    				<th onclick="sortingAscDesc()"><input type="text" class="filterinput" id="search_col2" onclick="filtersort()" placeholder="Дата"><br><span onclick="sortingAscDesc()">Дата приходу</span></th>	    				
	    				<th><input type="text" id="search_col3" class="filterinput" onclick="filtersort()" placeholder="Артикул"><br>Артикул</th>
	    				<th><input type="text" id="search_col4" class="filterinput" onclick="filtersort()" placeholder="Клас"><br>Клас</th>
	    				<th><input type="text" id="search_col5" class="filterinput" onclick="filtersort()" placeholder="Тип"><br>Тип</th>
	    				<th><input type="text" id="search_col6" class="filterinput" onclick="filtersort()" placeholder="Асортимент"><br>Асортимент</th>
	    				<th><input type="text" id="search_col7" class="filterinput" onclick="filtersort()" placeholder="Малюнок"><br>Малюнок</th>
	    				<th><input type="text" id="search_col8" class="filterinput" onclick="filtersort()" placeholder="Колір"><br>Колір</th>
	    				<th><input type="text" id="search_col9" class="filterinput" onclick="filtersort()" placeholder="Розмір"><br>Розмір</th>
	    				<th><input type="text" id="search_col13" class="filterinput gatun_input" onclick="filtersort()" ><br>1г</th>
	    				<th><input type="text" id="search_col14" class="filterinput gatun_input" onclick="filtersort()" ><br>2г</th>
	    				<th><input type="text" id="search_col15" class="filterinput gatun_input" onclick="filtersort()" ><br>3г</th>
	    				<th><input type="text" id="search_col16" class="filterinput gatun_input" onclick="filtersort()" ><br>Разом</th>
	    			</tr>

                </thead><tbody>';
    $quanteti_total = 0;
    $coun1_total = 0;
	$coun2_total = 0;
	$coun3_total = 0;
	$bag_sum_total = 0;	
    while($row = $get_consumption->fetch_assoc()) {
    	$consumption_id = $row['remainder_id'];
    	$bag_id = $row['bag_id'];

    	$result_username = $link->query("SELECT username FROM  $table_users WHERE  id='$user_id'");
    	$result_bag = $link->query("SELECT * FROM  $bag_table WHERE  bag_id='$bag_id'");

    	

		while($row_result_bag = $result_bag->fetch_assoc()) {
			$article_id = $row_result_bag['article_id'];
			$picture_id = $row_result_bag['picture_id'];
			$size_id = $row_result_bag['size_id'];
			$color_id = $row_result_bag['color_id'];
			$coun1 = $row_result_bag['coun1'];
			$coun2 = $row_result_bag['coun2'];
			$coun3 = $row_result_bag['coun3'];
			$bag_sum = 	$coun1 + $coun2 + $coun3;		
		}


		$result_article = $link->query("SELECT * FROM $table_article WHERE `article_id`='$article_id'");			
		while($row_result_article = $result_article->fetch_assoc()) {			
			$article_name = $row_result_article['article_name'];
			$type_id = $row_result_article['type_id'];
			$assortment_id = $row_result_article['assortment_id'];
			$class_id = $row_result_article['class_id'];
		}

		$result_picture = $link->query("SELECT * FROM $table_picture WHERE `picture_id`='$picture_id'");			
		while($row_result_picture = $result_picture->fetch_assoc()) {			
			$picture_name = $row_result_picture['picture_name'];
		}

		$result_size = $link->query("SELECT * FROM $table_size WHERE `size_id`='$size_id'");			
		while($row_result_size = $result_size->fetch_assoc()) {			
			$size_name = $row_result_size['size_name'];
		}

		$result_color = $link->query("SELECT * FROM $table_color WHERE `color_id`='$color_id'");			
		while($row_result_color = $result_color->fetch_assoc()) {			
			$color_name = $row_result_color['color_name'];
		}

		$result_class = $link->query("SELECT * FROM $table_class WHERE `class_id`='$class_id'");			
		while($row_result_class = $result_class->fetch_assoc()) {			
			$class_name = $row_result_class['class_name'];
		}

		$result_type = $link->query("SELECT * FROM $table_type WHERE `type_id`='$type_id'");			
		while($row_result_type = $result_type->fetch_assoc()) {			
			$type_name = $row_result_type['type_name'];
		}

		$result_assortment = $link->query("SELECT * FROM $table_assortment WHERE `assortment_id`='$assortment_id'");			
		while($row_result_assortment = $result_assortment->fetch_assoc()) {			
			$assortment_name = $row_result_assortment['assortment_name'];
		}

		while ( strlen($bag_id) < 6) {
			$bag_id = '0' . $bag_id;
		}
        echo  '<tr>
        			<td >' . $bag_id . '</td>
        			<td>' . $row['coming_date'] . '</td>
        			<td>'.$article_name.'</td>
        			<td>'.$class_name.'</td>
        			<td>'.$type_name.'</td>
        			<td>'.$assortment_name.'</td>
        			<td>'.$picture_name.'</td>
        			<td>'.$color_name.'</td>
        			<td>'.$size_name.'</td>
        			<td style="display:none"></td>
        			<td style="display:none"></td>
        			<td style="display:none"></td>
        			<td >'.$coun1.'</td>
        			<td>'.$coun2.'</td>
        			<td>'.$coun3.'</td>
        			<td>'.$bag_sum.'</td>    			
        		</tr>';
        $quanteti_total++;
		$coun1_total = $coun1_total + $coun1;
		$coun2_total = $coun2_total + $coun2;
		$coun3_total = $coun3_total + $coun3;
		$bag_sum_total = $bag_sum_total + $bag_sum;	
        
  	 }

  	echo '</tbody><tfoot><tr><td  id="val">'. $quanteti_total.'</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td id="row_index1">'. $coun1_total.'</td><td  id="row_index2">'.$coun2_total.'</td><td  id="row_index3">'.$coun3_total.'</td><td  id="row_index4" style="display:none"></td><td></td<td style="display:none"></td></tfoot></tbody></table>';
    
	    
}
else {
	   echo "Немає Залишку";
}