<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_price = 'dov_price';

$table_price_meta = 'price_meta';

$table_needles = 'dov_needles';
$table_size = 'dov_size';
$table_assortment = 'dov_assortment';
$table_class = 'dov_class';
$table_type = 'dov_type';
$table_article = 'dov_article';
$table_operation = 'dov_operation';

?>
<link rel="stylesheet" href="css/main.css">
<div id="content_price">
	<h3>Довідник Розцінок</h3>
	<hr>
	
	<button id="button_action" onclick="window.location.href = '#global_price';">Додати розцінку</button>
	<div id="global_price" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати розцінку</h1>
    <h6>*введіть значення у поле</h6>
    <div class="add-form">
		<label>Дата початку: <input type="date" id="start_date"></label>
	    <label>Дата завершення: <input type="date" id="end_date"></label>
	    <label>Ціна: <input type="number" id="price_value"></label>
	    <label>Назва: <input type="text" id="price_name"></label>
		<label><button class="addBtn" onclick="add_new_price()">Додати розцінку</button></label>
	</div>
	</div>
  </div>


  <!--modals-->
<div id="add_price_needles" class="modal-window">
	<div>
		
		<a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
		<h1>Додати голки</h1>
		<h6>*оберіть дію</h6>
		<div class="add-form">
			<form action="#" method="post">
				<?
				$get_needles_full = $link->query("SELECT * FROM $table_needles " );
				
				while($row_needles_full = $get_needles_full->fetch_assoc()){

					
					$needles_id = $row_needles_full['needles_id'];
					$get_needles_to_add = $link->query( "SELECT meta_value FROM $table_price_meta  WHERE  price_id='$price_id' AND  meta_key='needles_id' AND meta_value='$needles_id' " );
					if ($get_needles_to_add->num_rows > 0) {
						echo '<label><input value="'. $needles_id .'" class="needles" type="checkbox" checked/>' . $row_needles_full['needles_name'].'</label>' ;
					}
					else {
						echo '<label><input value="'. $needles_id .'" class="needles" type="checkbox" />' . $row_needles_full['needles_name'].'</label>' ;
					}
				}
			  ?>
			</form>
			<br />
		<input type = "button" value = "Зберегти" onclick = "safe_price_needles(<?echo $price_id;?>)">

		</div>
	</div>
</div>
<div id="add_price_gat" class="modal-window">
	<div>
		
		<a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
		<h1>Додати Гатунок</h1>
		<h6>*оберіть дію</h6>
		<div class="add-form">
			<form action="#" method="post">
				<?
				$get_gatunock_full = array(1 , 2 , 3);
				
				foreach ( $get_gatunock_full as $get_gatunock){
					
					
					$get_gatunock_to_add = $link->query( "SELECT meta_value FROM $table_price_meta  WHERE  price_id='$price_id' AND  meta_key='gatunock' AND meta_value='$get_gatunock' " );
					if ($get_gatunock_to_add->num_rows > 0) {
						echo '<label><input value="'. $get_gatunock .'" class="gatunock" type="checkbox" checked/>' . $get_gatunock.'</label>' ;
					}
					else {
						echo '<label><input value="'. $get_gatunock .'" class="gatunock" type="checkbox" />' . $get_gatunock.'</label>' ;
					}
				}
			  ?>
			</form>
			<br />
		<input type = "button" value = "Зберегти" onclick = "safe_price_gatunock(<?echo $price_id;?>)">

		</div>
	</div>
</div>
<div id="add_price_size" class="modal-window">
	<div>
		
		<a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
		<h1>Додати розмір</h1>
		<h6>*оберіть дію</h6>
		<div class="add-form">
			<form action="#" method="post">
				<?
				$get_size_full = $link->query("SELECT * FROM $table_size " );
				
				while($row_size_full = $get_size_full->fetch_assoc()){

					
					$size_id = $row_size_full['size_id'];
					$get_size_to_add = $link->query( "SELECT meta_value FROM $table_price_meta  WHERE  price_id='$price_id' AND  meta_key='size_id' AND meta_value='$size_id' " );
					if ($get_size_to_add->num_rows > 0) {
						echo '<label><input value="'. $size_id .'" class="size" type="checkbox" checked/>' . $row_size_full['size_name'].'</label>' ;
					}
					else {
						echo '<label><input value="'. $size_id .'" class="size" type="checkbox" />' . $row_size_full['size_name'].'</label>' ;
					}
				}
			  ?>
			</form>
			<br />
		<input type = "button" value = "Зберегти" onclick = "safe_price_size(<?echo $price_id;?>)">

		</div>
	</div>
</div>
<div id="add_price_class" class="modal-window">
	<div>
		
		<a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
		<h1>Додати клас</h1>
		<h6>*оберіть дію</h6>
		<div class="add-form">
			<form action="#" method="post">
				<?
				$get_class_full = $link->query("SELECT * FROM $table_class " );
				
				while($row_class_full = $get_class_full->fetch_assoc()){

					
					$class_id = $row_class_full['class_id'];
					$get_class_to_add = $link->query( "SELECT meta_value FROM $table_price_meta  WHERE  price_id='$price_id' AND  meta_key='class_id' AND meta_value='$class_id' " );
					if ($get_class_to_add->num_rows > 0) {
						echo '<label><input value="'. $class_id .'" class="class" type="checkbox" checked/>' . $row_class_full['class_name'].'</label>' ;
					}
					else {
						echo '<label><input value="'. $class_id .'" class="class" type="checkbox" />' . $row_class_full['class_name'].'</label>' ;
					}
				}
			  ?>
			</form>
			<br />
		<input type = "button" value = "Зберегти" onclick = "safe_price_class(<?echo $price_id;?>)">

		</div>
	</div>
</div>
<div id="add_price_asort" class="modal-window">
	<div>
		
		<a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
		<h1>Додати асортимент</h1>
		<h6>*оберіть дію</h6>
		<div class="add-form">
			<form action="#" method="post">
				<?
				$get_assortment_full = $link->query("SELECT * FROM $table_assortment " );
				
				while($row_assortment_full = $get_assortment_full->fetch_assoc()){

					
					$assortment_id = $row_assortment_full['assortment_id'];
					$get_assortment_to_add = $link->query( "SELECT meta_value FROM $table_price_meta  WHERE  price_id='$price_id' AND  meta_key='assortment_id' AND meta_value='$assortment_id' " );
					if ($get_assortment_to_add->num_rows > 0) {
						echo '<label><input value="'. $assortment_id .'" class="asort" type="checkbox" checked/>' . $row_assortment_full['assortment_name'].'</label>' ;
					}
					else {
						echo '<label><input value="'. $assortment_id .'" class="asort" type="checkbox" />' . $row_assortment_full['assortment_name'].'</label>' ;
					}
				}
			  ?>
			</form>
			<br />
		<input type = "button" value = "Зберегти" onclick = "safe_price_asort(<?echo $price_id;?>)">

		</div>
	</div>
</div>
<div id="add_price_type" class="modal-window">
	<div>
		
		<a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
		<h1>Додати тип</h1>
		<h6>*оберіть дію</h6>
		<div class="add-form">
			<form action="#" method="post">
				<?
				$get_type_full = $link->query("SELECT * FROM $table_type " );
				
				while($row_type_full = $get_type_full->fetch_assoc()){

					
					$type_id = $row_type_full['type_id'];
					$get_type_to_add = $link->query( "SELECT meta_value FROM $table_price_meta  WHERE  price_id='$price_id' AND  meta_key='type_id' AND meta_value='$type_id' " );
					if ($get_type_to_add->num_rows > 0) {
						echo '<label><input value="'. $type_id .'" class="paraneter_type" type="checkbox" checked/>' . $row_type_full['type_name'].'</label>' ;
					}
					else {
						echo '<label><input value="'. $type_id .'" class="paraneter_type" type="checkbox" />' . $row_type_full['type_name'].'</label>' ;
					}
				}
			  ?>
			</form>
			<br />
		<input type = "button" value = "Зберегти" onclick = "safe_price_type(<?echo $price_id;?>)">

		</div>
	</div>
</div>
<div id="add_price_art" class="modal-window">
	<div>
		
		<a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
		<h1>Додати артикул</h1>
		<h6>*оберіть дію</h6>
		<div class="add-form">
			<form action="#" method="post">

				<?
				$get_article_full = $link->query("SELECT * FROM $table_article " );
				
				while($row_article_full = $get_article_full->fetch_assoc()){

					
					$article_id = $row_article_full['article_id'];
					$get_article_to_add = $link->query( "SELECT meta_value FROM $table_price_meta  WHERE  price_id='$price_id' AND  meta_key='article_id' AND meta_value='$article_id' " );
					if ($get_article_to_add->num_rows > 0) {
						echo '<label><input value="'. $article_id .'" class="article" type="checkbox" checked/>' . $row_article_full['article_name'].'</label>' ;
					}
					else {
						echo '<label><input value="'. $article_id .'" class="article" type="checkbox" />' . $row_article_full['article_name'].'</label>' ;
					}
				}
			  ?>
			</form>
			<br />
		<input type = "button" value = "Зберегти" onclick = "safe_price_art(<?echo $price_id;?>)">

		</div>
	</div>
</div>
<div id="add_price_oper" class="modal-window">
	<div>
		
		<a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
		<h1>Додати операції</h1>
		<h6>*оберіть дію</h6>
		<div class="add-form">
			<form action="#" method="post">
				<?
				$get_operation_full = $link->query("SELECT * FROM $table_operation " );
				
				while($row_operation_full = $get_operation_full->fetch_assoc()){

					
					$operation_id = $row_operation_full['operation_id'];
					$get_operation_to_add = $link->query( "SELECT meta_value FROM $table_price_meta  WHERE  price_id='$price_id' AND  meta_key='operation_id' AND meta_value='$operation_id' " );
					if ($get_operation_to_add->num_rows > 0) {
						echo '<label><input value="'. $operation_id .'" class="names" type="checkbox" checked/>' . $row_operation_full['operation_name'].'</label>' ;
					}
					else {
						echo '<label><input value="'. $operation_id .'" class="names" type="checkbox" />' . $row_operation_full['operation_name'].'</label>' ;
					}
				}
			  ?>
			</form>
			<br />
		<input type = "button" value = "Зберегти" onclick = "safe_price_opr(<?echo $price_id;?>)">

		</div>
	</div>
</div>
<!--***-->
</div>
	<hr>
	<?
	$sql = "SELECT * FROM $table_price";
	$result = $link->query($sql);
	
	if ($result->num_rows > 0) {
		
	    echo '<div id="wrapperPriceTable">

	    		<div class="wrapperPriceTableHeader">
	    			<div id="priceTable1" style="padding: 0px;">
		    			<div class="HRov" style="width: 100%;">
		    				<div class="col">Код</div>
		    				<div class="col">Дата початку</div>
		    				<div class="col">Дата закінчення</div>
		    				<div class="col">Ціна</div>
		    				<div class="col">Назва</div>
		    				<div class="col"></div>
		    			</div>
		    		</div>
		    		<div id="priceTable2" style="padding: 0px; margin-left: 10px">
		    			<div class="HRov" style="width: 100%;">
		    				<div class="col">Операції</div>
		    				<div class="col">Артикул</div>
		    				<div class="col">Тип</div>
		    				<div class="col">Асортимент</div>
		    				<div class="col">Клас</div>
		    				<div class="col">Розмір</div>
		    				<div class="col">Гатунок</div>
		    				<div class="col">Голки</div>			
		    			</div> 
		    		</div>

	    		</div>
	    		<div class="wrapperPriceTableBody"><div id="priceTableBody1" style="padding: 0px; display:block;">';

	    while($row = $result->fetch_assoc()) {


	        echo  '<div class="Rov">
	        			<div class="item">' . $row['price_id'] . '</div>
	        			<div class="item"><input type="date" id="start_date' . $row['price_id'] . '" value="' . $row['start_date'] . '"></div>
	        			<div class="item"><input type="date" id="end_date' . $row['price_id'] . '" value="' . $row['end_date'] . '"></div>
	        			<div class="item"><input type="number" id="price_value' . $row['price_id'] . '" value="' . $row['price_value'] . '"></div>
	        			<div class="item"><input type="text" id="price_name' . $row['price_id'] . '" value="'.$row['price_name'].'"></div>
	        			<div class="item itemBtns">
	        				<button class="OperBtn priceBtn" onclick="get_price('. $row['price_id'].')">Update</button>
	        				<button class="edit_row editBtn priceBtn" onclick="edit_price('. $row['price_id'].')">Edit</button>
	        				<button class="delete_row deleteBtn priceBtn" onclick="delete_price('. $row['price_id'].')">DEL</button>
	        			</div>
	        		</div>';
	    }
	    echo '</div><div id="priceTableBody2" style="margin-left: 10px;"><div class="item2" style="width: 100% margin: 0;"><h3>Розцінка №'.$price_id.'</h3></div><div class="item priceTableBody2Content" style="width: 100%;">';

    	

    	$result_operation_id = $link->query("SELECT meta_value FROM $table_price_meta WHERE price_id='$price_id' AND meta_key='operation_id'");		
    	$result_article_id = $link->query("SELECT meta_value FROM $table_price_meta WHERE price_id='$price_id' AND meta_key='article_id'");
    	$result_type_id = $link->query("SELECT meta_value FROM $table_price_meta WHERE price_id='$price_id' AND meta_key='type_id'");
    	$result_assortment_id = $link->query("SELECT meta_value FROM $table_price_meta WHERE price_id='$price_id' AND meta_key='assortment_id'");
    	$result_class_id = $link->query("SELECT meta_value FROM $table_price_meta WHERE price_id='$price_id' AND meta_key='class_id'");
    	$result_size_id = $link->query("SELECT meta_value FROM $table_price_meta WHERE price_id='$price_id' AND meta_key='size_id'");
    	$result_gatunock = $link->query("SELECT meta_value FROM $table_price_meta WHERE price_id='$price_id' AND meta_key='gatunock'");
    	$result_needles_id = $link->query("SELECT meta_value FROM $table_price_meta WHERE price_id='$price_id' AND meta_key='needles_id'");

    	echo '<div class="col" style="padding: 0px;  display:block;">';
		while($row_operation_id = $result_operation_id->fetch_assoc()) {
			$operation_id = $row_operation_id['meta_value'];
			$result_operation_name = $link->query("SELECT operation_name FROM $table_operation WHERE operation_id='$operation_id'");
			$row_operation_name = $result_operation_name->fetch_assoc();
			echo '<div class="item3" style="width: 100% display: block;">'.$row_operation_name['operation_name'].'<button class="delOperationBtn" onclick="delete_price_oper('.$price_id.', '.$operation_id.')">X</button></div>';
		}
		echo '<div class="item3" style="width: 100%;"><button class="addOperationBtn" onclick="add_price_oper('.$price_id.')">+</button></div>';

		echo '</div><div class="col" style="padding: 0px;  display:block;">';
		while($row_article_id = $result_article_id->fetch_assoc()) {
			$article_id = $row_article_id['meta_value'];
			$result_article_name = $link->query("SELECT article_name FROM $table_article WHERE article_id='$article_id'");
			
			$row_article_name = $result_article_name->fetch_assoc();
			echo '<div class="item3" style="width: 100%;">'.$row_article_name['article_name'].'<button class="delOperationBtn" onclick="delete_price_art('.$price_id.', '.$article_id.')">X</button></div>';
		}
		echo '<div class="item3" style="width: 100%;"><button class="addOperationBtn" onclick="add_price_art('.$price_id.')">+</button></div>';

		echo '</div><div class="col" style="padding: 0px;  display:block;">';
		while($row_type_id = $result_type_id->fetch_assoc()) {
			$type_id = $row_type_id['meta_value'];
			$result_type_name = $link->query("SELECT type_name FROM $table_type WHERE type_id='$type_id'");
			
			$row_type_name = $result_type_name->fetch_assoc();
			echo '<div class="item3" style="width: 100%;">'.$row_type_name['type_name'].'<button class="delOperationBtn" onclick="delete_price_type('.$price_id.', '.$type_id.')">X</button></div>';
		}
		echo '<div class="item3" style="width: 100%;"><button class="addOperationBtn" onclick="add_price_type('.$price_id.')">+</button></div>';

		echo '</div><div class="col" style="padding: 0px;  display:block;">';
		while($row_assortment_id = $result_assortment_id->fetch_assoc()) {
			$assortment_id = $row_assortment_id['meta_value'];
			$result_assortment_name = $link->query("SELECT assortment_name FROM $table_assortment WHERE assortment_id='$assortment_id'");
			
			$row_assortment_name = $result_assortment_name->fetch_assoc();
			echo '<div class="item3" style="width: 100%;">'.$row_assortment_name['assortment_name'].'<button class="delOperationBtn" onclick="delete_price_asort('.$price_id.', '.$assortment_id.')">X</button></div>';
		}
		echo '<div class="item3" style="width: 100%;"><button class="addOperationBtn" onclick="add_price_asort('.$price_id.')">+</button></div>';

		echo '</div><div class="col" style="padding: 0px;  display:block;">';
		while($row_class_id = $result_class_id->fetch_assoc()) {
			$class_id = $row_class_id['meta_value'];
			$result_class_name = $link->query("SELECT class_name FROM $table_class WHERE class_id='$class_id'");
			
			$row_class_name = $result_class_name->fetch_assoc();
			echo '<div class="item3" style="width: 100%;">'.$row_class_name['class_name'].'<button class="delOperationBtn" onclick="delete_price_class('.$price_id.', '.$class_id.')">X</button></div>';
		}
		echo '<div class="item3" style="width: 100%;"><button class="addOperationBtn" onclick="add_price_class('.$price_id.')">+</button></div>';

		echo '</div><div class="col" style="padding: 0px;  display:block;">';
		while($row_size_id= $result_size_id->fetch_assoc()) {
			$size_id = $row_size_id['meta_value'];
			$result_size_name = $link->query("SELECT size_name FROM $table_size WHERE size_id='$size_id'");
			
			$row_size_name = $result_size_name->fetch_assoc();
			echo '<div class="item3" style="width: 100%;">'.$row_size_name['size_name'].'<button class="delOperationBtn" onclick="delete_price_size('.$price_id.', '.$size_id.')">X</button></div>';
		}
		echo '<div class="item3" style="width: 100%;"><button class="addOperationBtn" onclick="add_price_size('.$price_id.')">+</button></div>';

		echo '</div><div class="col" style="padding: 0px;  display:block;">';
		while($row_gatunock = $result_gatunock->fetch_assoc()) {
			echo '<div class="item3" style="width: 100%;">'.$row_gatunock['meta_value'].'<button class="delOperationBtn" onclick="delete_price_gat('.$price_id.', '.$row_gatunock['meta_value'].')">X</button></div>';
		}
		echo '<div class="item3" style="width: 100%;"><button class="addOperationBtn" onclick="add_price_gat('.$price_id.')">+</button></div>';

		echo '</div><div class="col" style="padding: 0px;  display:block;">';
		while($row_needles_id = $result_needles_id->fetch_assoc()) {
			$needles_id = $row_needles_id['meta_value'];
			$result_needles_name = $link->query("SELECT needles_name FROM $table_needles WHERE needles_id='$needles_id'");
			
			$row_needles_name = $result_needles_name->fetch_assoc();
			echo '<div class="item3" style="width: 100%;">'.$row_needles_name['needles_name'].'<button class="delOperationBtn" class="delOperation" onclick="delete_price_needles('.$price_id.', '.$needles_id.')">X</button></div>';
		}
		echo '<div class="item3" style="width: 100%, border-right: 1px solid transparent;"><button class="addOperationBtn" onclick="add_price_needles('.$price_id.')">+</button></div>';

		echo '</div>';

	    
	    echo '</div></div></div></div>';
	} else {
	    echo "Немає Розцінок";
	}
	$link->close();

?></div>
<?