<?php
// Initialize the session
    session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_packingMaterialsMeta = 'packing_materials_meta';

?>

<link rel="stylesheet" href="css/main.css">
<div id="contentPackingMaterialsMeta">
    <h3>Розхід / Прихід пакувальних матеріал</h3>
    <hr>
    
    <button id="button_action" onclick="window.location.href = '#global_packingMaterialsMeta'; packingMaterialsMeta_select_key()">Додати</button>
    <div id="global_packingMaterialsMeta" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати</h1>
    <h6>*введіть значення у поля</h6>
    <div class="add-form">
        <label>
            Пакувальний матеріал: <?php
                $sql_packing_materials = "SELECT * FROM dov_packing_materials ";
                $result_packing_materials = $link->query($sql_packing_materials);

                if ($result_packing_materials->num_rows > 0) {
                    echo '<select id="packing_materials_select" name="packing_materials_select">';
                    while($row = $result_packing_materials->fetch_assoc()) {
                        echo  '<option value="' . $row['packing_materials_id'] . '">' . $row['packing_materials_name'] . '</option>';
                    }
                    echo '</select>';
                }
                else {
                    echo '<select id="packing_materials_select" name="packing_materials_select"><option value="-1">Not found</option></select>';
                }
            ?>
        </label>
        <label>
            <select id="select_meta_key" name="select_meta_key" onchange="packingMaterialsMeta_select_key()">
                <option value="Постачальники" selected="selected">Постачальники</option>
                <option value="Споживачі" selected="selected">Споживачі</option>
            </select>
        </label>
        <label id='label_packing_materials_provider_select'>
            Постачальники: <?php
                $sql_packing_materials_provider = "SELECT * FROM dov_packing_materials_provider ";
                $result_packing_materials_provider = $link->query($sql_packing_materials_provider);

                if ($result_packing_materials_provider->num_rows > 0) {
                    echo '<select id="packing_materials_provider_select" name="packing_materials_provider_select">';
                    while($row = $result_packing_materials_provider->fetch_assoc()) {
                        echo  '<option value="' . $row['provider_id'] . '">' . $row['provider_name'] . '</option>';
                    }
                    echo '</select>';
                }
                else {
                    echo '<select id="packing_materials_provider_select" name="packing_materials_provider_select"><option value="-1">Not found</option></select>';
                }
            ?>
        </label>
        <label id='label_packing_materials_consumption_select'>
            Споживачі: <?php
                $sql_packing_materials_consumption = "SELECT * FROM dov_packing_materials_consumption ";
                $result_packing_materials_consumption = $link->query($sql_packing_materials_consumption);

                if ($result_packing_materials_consumption->num_rows > 0) {
                    echo '<select id="packing_materials_consumption_select" name="packing_materials_consumption_select">';
                    while($row = $result_packing_materials_consumption->fetch_assoc()) {
                        echo  '<option value="' . $row['consumption_id'] . '">' . $row['consumption_name'] . '</option>';
                    }
                    echo '</select>';
                }
                else {
                    echo '<select id="packing_materials_select_consumption" name="packing_materials_select_consumption"><option value="-1">Not found</option></select>';
                }
            ?>
        </label>
        <label><button class="addBtn" onclick="add_new_packingMaterialsMeta()">Додати</button></label>
    </div>
    </div>
  </div>
  </div>
</div>
    <hr>
    <?
    $sql = "SELECT * FROM $table_packingMaterialsMeta ";
    $result = $link->query($sql);

    if ($result->num_rows > 0) {
        
        echo '<div class="table">
                    <div class="HRov">
                        <div class="col">Код</div>
                        <div class="col">Пакувальний матеріал</div>
                        <div class="col">Постачальник / Споживач</div>
                        <div class="col">Значення</div>
                        <div class="col"></div>                     
                    </div>';
        while($row = $result->fetch_assoc()) {
            $consumption_id = $row['meta_value'];
            $consumption = $link->query("SELECT * FROM dov_packing_materials_consumption WHERE `consumption_id`='$consumption_id'")->fetch_assoc()['consumption_name'];
            $provider_id = $row['meta_value'];
            $provider = $link->query("SELECT * FROM dov_packing_materials_provider WHERE `provider_id`='$provider_id'")->fetch_assoc()['provider_name'];
            if($row['meta_key'] == 'Постачальники') {
                $meta_value = $provider;
            }
            else {
                $meta_value = $consumption;
            }
            echo  '<div class="Rov">
                        <div class="col no_pading">' . $row['meta_id'] . '</div>
                        <div class="col no_pading">' . $row['packing_materials_id'] . '</div>
                        <div class="col no_pading">' . $row['meta_key'] . '</div>
                        <div class="col no_pading">' . $meta_value . '</div>
                        <div class="col no_pading"><button class="edit_row editBtn" onclick="edit_packingMaterialsMeta('. $row['meta_id'].')">Редагувати</button>
                        <button class="delete_row deleteBtn" onclick="delete_packingMaterialsMeta('. $row['meta_id'].')">Видалити</button></div>
                    </div>';
        }
        echo '</div>';
    } else {
        echo "Немає результатів";
    }
    $link->close();

?></div>
<?
