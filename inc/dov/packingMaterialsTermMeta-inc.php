<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_packingMaterialsTermMeta = 'packing_materials_term_meta';

?>
<link rel="stylesheet" href="css/main.css">
<div id="contentPackingMaterialsTermMeta">
	<h3>Розміри пакувальних матеріалів</h3>
	<hr>
	
	<button id="button_action" onclick="window.location.href = '#global_packingMaterialsTermMeta';">Додати</button>
	<div id="global_packingMaterialsTermMeta" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати</h1>
    <h6>*введіть значення у поле</h6>
    <div class="add-form">
		<label>Ім'я: <input packingMaterialsTermMeta="text" name="add_name" id="add_name"></label>
		<label>
            Розмір: <?php
                $sql_packing_materials_term = "SELECT * FROM dov_packing_materials_term ";
                $result_packing_materials_term = $link->query($sql_packing_materials_term);

                if ($result_packing_materials_term->num_rows > 0) {
                    echo '<select id="packing_materials_term_select" name="packing_materials_term_select">';
                    while($row = $result_packing_materials_term->fetch_assoc()) {
                        echo  '<option value="' . $row['term_id'] . '">' . $row['term_name'] . '</option>';
                    }
                    echo '</select>';
                }
                else {
                    echo '<select id="packing_materials_term_select" name="packing_materials_term_select"><option value="-1">Not found</option></select>';
                }
            ?>
        </label>
		<label><button class="addBtn" onclick="add_new_packingMaterialsTermMeta()">Додати</button></label>
	</div>
	</div>
  </div>
  </div>
</div>
	<hr>
	<?
	$sql = "SELECT * FROM $table_packingMaterialsTermMeta ";
	$result = $link->query($sql);

	if ($result->num_rows > 0) {
		
	    echo '<div class="table">
	    			<div class="HRov">
						<div class="col">Код</div>
						<div class="col">Розмір</div>
	    				<div class="col">Назва</div>
	    				<div class="col"></div>	    				
	    			</div>';
	    while($row = $result->fetch_assoc()) {
			$term_id = $row['term_id'];
            $term_name = $link->query("SELECT * FROM dov_packing_materials_term WHERE `term_id`='$term_id'")->fetch_assoc()['term_name'];
	        echo  '<div class="Rov">
						<div class="col no_pading">' . $row['meta_id'] . '</div>
						<div class="col no_pading">' . $term_name . '</div>
	        			<div class="col no_pading"><input type="text" id="update_name' . $row['meta_id'] . '" value="' . $row['name'] . '"></div>        		
	        			<div class="col no_pading"><button class="edit_row editBtn" onclick="edit_packingMaterialsTermMeta('. $row['meta_id'].')">Редагувати</button>
	        			<button class="delete_row deleteBtn" onclick="delete_packingMaterialsTermMeta('. $row['meta_id'].')">Видалити</button></div>
	        		</div>';
	    }
	    echo '</div>';
	} else {
	    echo "Немає результатів";
	}
	$link->close();

?></div>
<?
