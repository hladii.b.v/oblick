<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_packingMaterialsTerm = 'dov_packing_materials_term';

?>
<link rel="stylesheet" href="css/main.css">
<div id="contentPackingMaterialsTerm">
	<h3>Пакувальні матеріали</h3>
	<hr>
	
	<button id="button_action" onclick="window.location.href = '#global_packingMaterialsTerm';">Додати розмір пакувального матеріалу</button>
	<div id="global_packingMaterialsTerm" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати розмір пакувального матеріалу</h1>
    <h6>*введіть значення у поле</h6>
    <div class="add-form">
        <label>Ім'я: <input type="text" name="add_name" id="add_name"></label>
        <label>
            Пакувальний матеріал: <?php
                $sql_packing_materials = "SELECT * FROM dov_packing_materials ";
                $result_packing_materials = $link->query($sql_packing_materials);

                if ($result_packing_materials->num_rows > 0) {
                    echo '<select id="packing_materials_select" name="packing_materials_select">';
                    while($row = $result_packing_materials->fetch_assoc()) {
                        echo  '<option value="' . $row['packing_materials_id'] . '">' . $row['packing_materials_name'] . '</option>';
                    }
                    echo '</select>';
                }
                else {
                    echo '<select id="packing_materials_select" name="packing_materials_select"><option value="-1">Not found</option></select>';
                }
            ?>
        </label>
		<label><button class="addBtn" onclick="add_new_packingMaterialsTerm()">Додати розмір пакувального матеріалу</button></label>
	</div>
	</div>
  </div>
  </div>
</div>
	<hr>
	<?
	$sql = "SELECT * FROM $table_packingMaterialsTerm ";
	$result = $link->query($sql);

	if ($result->num_rows > 0) {
		
	    echo '<div class="table">
	    			<div class="HRov">
	    				<div class="col">Код</div>
                        <div class="col">Назва</div>
                        <div class="col">Пакувальний матеріал</div>
	    				<div class="col"></div>	    				
	    			</div>';
	    while($row = $result->fetch_assoc()) {
            $packing_materials_id = $row['packing_materials_id'];
            $packing_materials_name = $link->query("SELECT * FROM dov_packing_materials WHERE `packing_materials_id`='$packing_materials_id'")->fetch_assoc()['packing_materials_name'];
	        echo  '<div class="Rov">
	        			<div class="col no_pading">' . $row['term_id'] . '</div>
                        <div class="col no_pading"><input type="text" id="update_name' . $row['term_id'] . '" value="' . $row['term_name'] . '"></div>
                        <div class="col no_pading">' . $packing_materials_name . '</div>
	        			<div class="col no_pading"><button class="edit_row editBtn" onclick="edit_packingMaterialsTerm('. $row['term_id'].')">Редагувати</button>
	        			<button class="delete_row deleteBtn" onclick="delete_packingMaterialsTerm('. $row['term_id'].')">Видалити</button></div>
	        		</div>';
	    }
	    echo '</div>';
	} else {
	    echo "Немає розмірів пакувальних матеріалів";
	}
	$link->close();

?></div>
<?
