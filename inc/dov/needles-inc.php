<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_needles = 'dov_needles';

?>
<link rel="stylesheet" href="css/main.css">
<div id="contentneedles">
	<h3>Довідник Голок</h3>
	<hr>
	
	<button id="button_action" onclick="window.location.href = '#global_needles';">Додати голки</button>
	<div id="global_needles" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати голки</h1>
    <h6>*введіть значення у поле</h6>
    <div class="add-form">
		<label>Кількість голок: <input type="number" name="add_name" id="add_name"></label>
		<label><button class="addBtn" onclick="add_new_needles()">Додати голки</button></label>
	</div>
	</div>
  </div>
</div>
	<hr>
	<?
	$sql = "SELECT * FROM $table_needles";
	$result = $link->query($sql);

	if ($result->num_rows > 0) {
		
	    echo '<div class="table">
	    			<div class="HRov">
	    				<div class="col">Код</div>
	    				<div class="col">Кількість голок</div>
	    				<div class="col"></div>	    				
	    			</div>';
	    while($row = $result->fetch_assoc()) {


	        echo  '<div class="Rov">
	        			<div class="col no_pading">' . $row['needles_id'] . '</div>
	        			<div class="col no_pading"><input type="number" id="update_name' . $row['needles_id'] . '" value="' . $row['needles_name'] . '"></div>     		
	        			<div class="col no_pading"><button class="edit_row editBtn" onclick="edit_needles('. $row['needles_id'].')">Редагувати</button>
	        			<button class="delete_row deleteBtn" onclick="delete_needles('. $row['needles_id'].')">Видалити</button></div>
	        		</div>';
	    }
	    echo '</div>';
	} else {
	    echo "Немає голок";
	}
	$link->close();

?></div>
<?