<?php
// Initialize the session
    session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_article = 'dov_article';

?>

<link rel="stylesheet" href="css/main.css">
<div id="contentArticle">
    <h3>Артикул</h3>
    <hr>

    <button id="button_action" onclick="window.location.href = '#global_article';">Додати артикул</button>
    <div id="global_article" class="modal-window">
        <div>
            <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
            <h1>Додати артикул</h1>
            <h6>*введіть значення у поле</h6>
            <div class="add-form">
                <label>Ім'я: <input type="text" name="add_name" id="add_name"></label>
                <label>
                    Тип: <?php
                        $sql_type = "SELECT * FROM dov_type ";
                        $result_type = $link->query($sql_type);

                        if ($result_type->num_rows > 0) {
                            echo '<select id="type_select" name="type_select">';
                            while($row = $result_type->fetch_assoc()) {
                                echo  '<option value="' . $row['type_id'] . '">' . $row['type_name'] . '</option>';
                            }
                            echo '</select>';
                        }
                        else {
                            echo '<select id="type_select" name="type_select"><option value="-1">Not found</option></select>';
                        }
                    ?>
                </label>
                <label>
                    Колір: <?php
                        $sql_color = "SELECT * FROM dov_color ";
                        $result_color = $link->query($sql_color);

                        if ($result_color->num_rows > 0) {
                            echo '<select id="color_select" name="color_select">';
                            while($row = $result_color->fetch_assoc()) {
                                echo  '<option value="' . $row['color_id'] . '">' . $row['color_name'] . '</option>';
                            }
                            echo '</select>';
                        }
                        else {
                            echo '<select id="color_select" name="color_select"><option value="-1">Not found</option></select>';
                        }
                    ?>
                </label>
                <label>
                    Асортимент: <?php
                        $sql_assortment = "SELECT * FROM dov_assortment ";
                        $result_assortment = $link->query($sql_assortment);

                        if ($result_assortment->num_rows > 0) {
                            echo '<select id="assortment_select" name="assortment_select">';
                            while($row = $result_assortment->fetch_assoc()) {
                                echo  '<option value="' . $row['assortment_id'] . '">' . $row['assortment_name'] . '</option>';
                            }
                            echo '</select>';
                        }
                        else {
                            echo '<select id="assortment_select" name="assortment_select"><option value="-1">Not found</option></select>';
                        }
                    ?>
                </label>
                <label>
                    Клас: <?php
                        $sql_class = "SELECT * FROM dov_class ";
                        $result_class = $link->query($sql_class);

                        if ($result_class->num_rows > 0) {
                            echo '<select id="class_select" name="class_select">';
                            while($row = $result_class->fetch_assoc()) {
                                echo  '<option value="' . $row['class_id'] . '">' . $row['class_name'] . '</option>';
                            }
                            echo '</select>';
                        }
                        else {
                            echo '<select id="class_select" name="class_select"><option value="-1">Not found</option></select>';
                        }
                    ?>
                </label>
                <label>
                    Сезон: <?php
                        $sql_season = "SELECT * FROM dov_season ";
                        $result_season = $link->query($sql_season);

                        if ($result_season->num_rows > 0) {
                            echo '<select id="season_select" name="season_select">';
                            while($row = $result_season->fetch_assoc()) {
                                echo  '<option value="' . $row['season_id'] . '">' . $row['season_name'] . '</option>';
                            }
                            echo '</select>';
                        }
                        else {
                            echo '<select id="season_select" name="season_select"><option value="-1">Not found</option></select>';
                        }
                    ?>
                </label>
                <label>Примітка: <input type="text" name="add_noted" id="add_noted"></label>
                <label><button class="addBtn" onclick="add_new_article()">Додати артикул</button></label>
            </div>
        </div>
    </div>
</div>
</div>
<hr>
<?
    $sql = "SELECT * FROM $table_article ";
    $result = $link->query($sql);

    if ($result->num_rows > 0) {
        
        echo '<div class="table">
                    <div class="HRov">
                        <div class="col">Код</div>
                        <div class="col">Назва</div>
                        <div class="col">Тип</div>
                        <div class="col">Асортимент</div>
                        <div class="col">Клас</div>
                        <div class="col">Сезон</div>
                        <div class="col">Примітка</div>
                        <div class="col"></div>                     
                    </div>';
        while($row = $result->fetch_assoc()) {
            $type_id = $row['type_id'];
            $type = $link->query("SELECT * FROM dov_type WHERE `type_id`='$type_id'")->fetch_assoc()['type_name'];
            
            $assortment_id=$row['assortment_id'];
            $assortment = $link->query("SELECT * FROM dov_assortment WHERE `assortment_id`='$assortment_id'")->fetch_assoc()['assortment_name'];
            $class_id=$row['class_id'];
            $class = $link->query("SELECT * FROM dov_class WHERE `class_id`='$class_id'")->fetch_assoc()['class_name'];
            $season_id=$row['season_id'];
            $season = $link->query("SELECT * FROM dov_season WHERE `season_id`='$season_id'")->fetch_assoc()['season_name'];
            echo  '<div class="Rov">
                        <div class="col ">' . $row['article_id'] . '</div>
                        <div class="col no_pading"><input article="text" id="update_name' . $row['article_id'] . '" value="' . $row['article_name'] . '"></div>
                        <div class="col ">' . $type . '</div>
                        <div class="col ">' . $assortment . '</div>
                        <div class="col ">' . $class . '</div>
                        <div class="col ">' . $season . '</div>
                        <div class="col ">' . $row['article_noted'] . '</div>
                        <div class="col no_pading"><button class="edit_row editBtn" onclick="edit_article('. $row['article_id'].')">Редагувати</button></br>
                        <button class="delete_row deleteBtn" onclick="delete_article('. $row['article_id'].')">Видалити</button></div>
                    </div>';
        }
        echo '</div>';
    } else {
        echo "Немає артикулів";
    }
    $link->close();

?>
</div>
<?
