<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_knitting_machines = 'dov_knitting_machines';
$table_needles = 'dov_needles';
$table_models = 'dov_models';
$table_inches = 'dov_inches';



$result_models = $link->query("SELECT * FROM $table_models ");
$result_needles = $link->query("SELECT * FROM $table_needles");
$result_inches = $link->query("SELECT * FROM $table_inches");
while($row_models_all = $result_models->fetch_assoc()) {
	$all_models .= '<option value="'.$row_models_all['model_id'].'">'.$row_models_all['model_name'].'</option>';
}
while($row_needles_all = $result_needles->fetch_assoc()) {
	$all_needles .= '<option value="'.$row_needles_all['needles_id'].'">'.$row_needles_all['needles_name'].'</option>';
}
while($row_inches_all = $result_inches->fetch_assoc()) {
	$all_inches .= '<option value="'.$row_inches_all['inches_id'].'">'.$row_inches_all['inches_name'].'</option>';
}
?>
<link rel="stylesheet" href="css/main.css">
<div id="contentknitting_machines">
	<h3>Довідник Машин</h3>
	<hr>
	
	<button id="button_action" onclick="window.location.href = '#global_knitting_machines';">Додати машину</button>
	<div id="global_knitting_machines" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати машину</h1>
    <h6>*введіть значення у поле</h6>
    <div class="add-form">
		<label>Номер машини: <input type="number" name="add_number" id="add_number"></label>
		<label>Модель машини: <select id="add_models"><?echo $all_models;?></select></label>
		<label>Голки машини: <select id="add_needles"><?echo $all_needles;?></select></label>
		<label>Дюйми машини: <select id="add_inches"><?echo $all_inches;?></select></label>
		<label><button class="addBtn" onclick="add_new_knitting_machines()">Додати машину</button></label>
	</div>
	</div>
  </div>
</div>
	<hr>
	<?
	$sql = "SELECT * FROM $table_knitting_machines";
	$result = $link->query($sql);

	if ($result->num_rows > 0) {
		
	    echo '<div class="table">
	    			<div class="HRov">
	    				<div class="col">Код</div>
	    				<div class="col">Номер	</div>
	    				<div class="col">Модель</div>
	    				<div class="col">Голки</div>
	    				<div class="col">Дюйми</div>
	    				<div class="col">Статус</div>
	    				<div class="col"></div>	    				
	    			</div>';
	    while($row = $result->fetch_assoc()) {
	    	$model_id = $row['model_id'];
	    	$needles_id = $row['needles_id'];
	    	$inches_id	 = $row['inches_id'];

	    	$get_models='';
	    	$get_needles='';
	    	$get_inches='';

	    	$result_models_curent = $link->query("SELECT * FROM $table_models WHERE model_id='$model_id' ");
			$result_models = $link->query("SELECT * FROM $table_models  WHERE model_id!='$model_id'");

			$get_needles_curent = $link->query("SELECT * FROM $table_needles WHERE needles_id='$needles_id' ");
			$result_needles = $link->query("SELECT * FROM $table_needles WHERE needles_id!='$needles_id'");

			$result_inches_curent = $link->query("SELECT * FROM $table_inches WHERE inches_id='$inches_id' ");
			$result_inches = $link->query("SELECT * FROM $table_inches  WHERE inches_id!='$inches_id'");

			if ($row['machines_status'] == 1){
				$status = '<option value="1">Працює</option><option value="0">Не працює</option>';
			}
			else {
				$status = '<option value="0">Не працює</option><option value="1">Працює</option>';
			}

			while($row_models_curent = $result_models_curent->fetch_assoc()) {
				$get_models .= '<option value="'.$row_models_curent['model_id'].'">'.$row_models_curent['model_name'].'</option>';
			}
			while($row_models = $result_models->fetch_assoc()) {
				$get_models .= '<option value="'.$row_models['model_id'].'">'.$row_models['model_name'].'</option>';
			}

			while($row_needles_curent = $get_needles_curent->fetch_assoc()) {
				$get_needles .= '<option value="'.$row_needles_curent['needles_id'].'">'.$row_needles_curent['needles_name'].'</option>';
			}
			while($row_needles = $result_needles->fetch_assoc()) {
				$get_needles .= '<option value="'.$row_needles['needles_id'].'">'.$row_needles['needles_name'].'</option>';
			}

			while($row_inches_curent = $result_inches_curent->fetch_assoc()) {
				$get_inches .= '<option value="'.$row_inches_curent['inches_id'].'">'.$row_inches_curent['inches_name'].'</option>';
			}
			while($row_inches = $result_inches->fetch_assoc()) {
				$get_inches .= '<option value="'.$row_inches['inches_id'].'">'.$row_inches['inches_name'].'</option>';
			}

	        echo  '<div class="Rov">
	        			<div class="col ">' . $row['machines_id'] . '</div>
	        			<div class="col "><input type="number" id="update_number' . $row['machines_id'] . '" value="' . $row['machines_number'] . '"></div>    
	        			<div class="col "><select id="update_models' . $row['machines_id'] .'">'.$get_models.'</select></div>	
	        			<div class="col "><select id="update_needles' . $row['machines_id'] .'">'.$get_needles.'</select></div>
	        			<div class="col "><select id="update_inches' . $row['machines_id'] .'">'.$get_inches.'</select></div>
	        			<div class="col "><select id="update_status' . $row['machines_id'] .'">'.$status.'</select></div>
	        			<div class="col no_pading" style="padding: 11px;"><button class="edit_row editBtn" onclick="edit_knitting_machines('. $row['machines_id'].')">Редагувати</button>
	        			<button class="delete_row deleteBtn" onclick="delete_knitting_machines('. $row['machines_id'].')">Видалити</button></div>
	        		</div>';
	    }
	    echo '</div>';
	} else {
	    echo "Немає машин";
	}
	$link->close();

?></div>
<?