<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_season = 'dov_season';

?>
<link rel="stylesheet" href="css/main.css">
<div id="contentSeason">
	<h3>Сезони</h3>
	<hr>
	
	<button id="button_action" onclick="window.location.href = '#global_season';">Додати сезон</button>
	<div id="global_season" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати сезон</h1>
    <h6>*введіть значення у поле</h6>
    <div class="add-form">
		<label>Ім'я: <input season="text" name="add_name" id="add_name"></label>
		<label><button class="addBtn" onclick="add_new_season()">Додати сезон</button></label>
	</div>
	</div>
  </div>
  </div>
</div>
	<hr>
	<?
	$sql = "SELECT * FROM $table_season ";
	$result = $link->query($sql);

	if ($result->num_rows > 0) {
		
	    echo '<div class="table">
	    			<div class="HRov">
	    				<div class="col">Код</div>
	    				<div class="col">Назва</div>
	    				<div class="col"></div>	    				
	    			</div>';
	    while($row = $result->fetch_assoc()) {


	        echo  '<div class="Rov">
	        			<div class="col no_pading">' . $row['season_id'] . '</div>
	        			<div class="col no_pading"><input season="text" id="update_name' . $row['season_id'] . '" value="' . $row['season_name'] . '"></div>        		
	        			<div class="col no_pading"><button class="edit_row editBtn" onclick="edit_season('. $row['season_id'].')">Редагувати</button>
	        			<button class="delete_row deleteBtn" onclick="delete_season('. $row['season_id'].')">Видалити</button></div>
	        		</div>';
	    }
	    echo '</div>';
	} else {
	    echo "Немає сезонів";
	}
	$link->close();

?></div>
<?