<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_employees = 'dov_employees';
$table_operation = 'dov_operation';
$table_employees_meta = 'employees_meta';

$get_operation_full = $link->query("SELECT * FROM $table_operation " );

?>
<link rel="stylesheet" href="css/main.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div class="content">
	<h3>Довідник Працівників</h3>
	<hr>
	<button id="button_action" onclick="window.location.href = '#open-modal';">Додати працівника</button>
	
<div id="open-modal" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати працівника</h1>
    <h6>*введіть значення у поля</h6>
    <div class="add-form">
		<label for="add_first_name">Ім'я: </label> <input type="text" name="add_first_name" id="add_first_name">
		<label for="add_last_name">Прізвище: </label><input type="text" name="add_last_name" id="add_last_name">
		<label for="add_surname">По-батькові: </label><input type="text"name="add_surname" id="add_surname">
		<button id="button_last_action" onclick="add_new_employees()">Додати Працівника </button>
	</div>
  </div>
</div>


<div id="add_action_for_employee" class="modal-window">
	<div>
		
		<a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
		<h1>Додати операції</h1>
		<h6>*оберіть дію</h6>
		<div class="add-form">
			<form action="#" method="post"><?
				
				
				while($row_operation_full = $get_operation_full->fetch_assoc()){

					
					$operation_id = $row_operation_full['operation_id'];
					$get_operation_to_add = $link->query( "SELECT meta_value FROM $table_employees_meta  WHERE  employees_id='$get_employees_id' AND  meta_key='employees_operation' AND meta_value='$operation_id' " );
					if ($get_operation_to_add->num_rows > 0) {
						echo '<label><input value="'. $operation_id .'" class="names" type="checkbox" checked/>' . $row_operation_full['operation_name'].'</label>' ;
					}
					else {
						echo '<label><input value="'. $operation_id .'" class="names" type="checkbox" />' . $row_operation_full['operation_name'].'</label>' ;
					}
				}
			  ?>
			</form>
			<br />
		<input type = "button" value = "Зберегти" onclick = "Validatecheckbox(<?echo $get_employees_id;?>)">

		</div>
	</div>
</div>	

<div id="add_eding_for_employee" class="modal-window">
	<div>
		
		<a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
		<h1>Редагування працівника</h1>
		<?
			$get_operation_to_add = $link->query( "SELECT * FROM $table_employees  WHERE employees_id='$get_employees_id' " );	
				
				while($row_single_emp = $get_operation_to_add->fetch_assoc()){

					if ($row_single_emp['status'] == 1){
						$status = '<option value="1">Працює</option><option value="0">Звільненно</option>';
					}
					else {
						$status = '<option value="0">Звільненно</option><option value="1">Працює</option>';
					}
					echo '<div class="Rov"><div class="col"><input type="text" id="update_first_name'. $get_employees_id.'" value="'.$row_single_emp['first_name'].'"></div><div class="col"><input type="text" id="update_last_name'. $get_employees_id.'" value="'.$row_single_emp['last_name'].'"></div><div class="col"><input type="text" id="update_surname'. $get_employees_id.'" value="'.$row_single_emp['surname'].'"></div><div class="col"><select id="update_status'. $get_employees_id.'">'.$status.'</select></div></div>';
					
				}
			  ?>
			
			<br />
		
		<button class="edit_row editBtn" onclick="edit_employeesr(<?echo $get_employees_id?>)">Зберегти</button>

		</div>
	</div>
</div>

<style>
.notfound{
  display: none;
}
</style>

<br/>

	<hr>
  <input type="text" id="txt_searchall" class="searcheverywhere" onclick="filtersort()" placeholder='Пошук усюди'>&nbsp;
  	<?
	$sql = "SELECT * FROM $table_employees ";
	$result = $link->query($sql);

	if ($result->num_rows > 0) {
		
	    echo '<table id="myTable" class="order-table tableEmployees">
	    			
	    			<thead>
                          <tr>
	    				<th style="width:40px;"><input type="text" id="txt_code" class="filterinput" onclick="filtersort() "placeholder="Пошук по коду"><br><span onclick="sortingAscDesc()">Код</span></th>
	    				<th onclick="sortingAscDesc()"><input type="text" class="filterinput" id="search_col2" onclick="filtersort()" placeholder="Пошук по імені"><br><span onclick="sortingAscDesc()">Ім`я</span></th>
	    				<th><input type="text" id="search_col3"  class="filterinput" onclick="filtersort()" placeholder="Пошук по прізвищу"><br>Прізвище</th>
	    				<th><input type="text" id="search_col4" class="filterinput" onclick="filtersort()" placeholder="Пошук по-батькові"><br>По-батькові</th>
	    				<th><input type="text" id="search_col5" class="filterinput" onclick="filtersort()" placeholder="Пошук по операціях"><br>Операції</th>
	    				<th><input type="text" id="search_col6" class="filterinput" onclick="filtersort()" placeholder="Пошук по статусу"><br>Статус</th>
	    				<th></th>
	    			</tr>

                        </thead><tbody><tr class="notfound">
     <td colspan="7">Нічого не знайдено =(</td>
   </tr>';
	    while($row = $result->fetch_assoc()) {
	    	$employees_id =$row['employees_id'];
	    	$get_operation = "SELECT meta_value FROM $table_employees_meta  WHERE employees_id='$employees_id' AND meta_key='employees_operation'";
			$result_operation = $link->query($get_operation);
			$employees_operation = '';

			if ($result_operation->num_rows > 0) {
				while($row_operation = $result_operation->fetch_assoc()) {

					$operation_id = $row_operation['meta_value'];
					$get_operation_name = $link->query("SELECT operation_name FROM $table_operation  WHERE operation_id='$operation_id'" );

					while($row_operation_name = $get_operation_name->fetch_assoc()){

						$employees_operation .= $row_operation_name['operation_name']. ', ';
					}
				}
			}
			else {
				$employees_operation = 'Немає операцій';
			}


	    	if ($row['status'] == 1){
				$status = 'Працює';
			}
			else {
				$status = 'Звільненно';
			}
			$employees_operation = substr($employees_operation, 0, -2);
	        echo  '<tr>
	        			<td>' . $row['employees_id'] . '</td>
	        			<td>' . $row['first_name'] . '</td>
	        			<td>' . $row['last_name'] . '</td>
	        			<td>'. $row['surname'] . '</td>
	        			<td>' .$employees_operation. '</td>
	        			<td>' . $status . '</td>
	        			<td class=editionofempl>
	        				
	        				<button class="edit_row OperBtn" onclick="add_emploer_oper('. $row['employees_id'].')">Операції працівника</button>
	        				<button class="edit_row editBtn" onclick="edit_emploer_modal('. $row['employees_id'].')">Редагувати</button>
	        				<button class="delete_row deleteBtn" onclick="delete_employees('. $row['employees_id'].')">Видалити</button>
	        			</td>
	        		</tr>';
	        
	    }
	    //echo '<tfoot><tr><td>FOOTER</td></tr></tfoot></tbody></table>';
	    echo '</tbody></table>';
	} else {
	    echo "Немає Працівників";
	}
	$link->close();

?></div>
