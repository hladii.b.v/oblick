<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_operation = 'dov_operation';

?>
<link rel="stylesheet" href="css/main.css">
<div id="contentOperations">
	<h3>Довідник Операцій</h3>
	<hr>
	
	<button id="button_action" onclick="window.location.href = '#global_operation';">Додати операцію</button>
	<div id="global_operation" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати операцію</h1>
    <h6>*введіть значення у поле</h6>
    <div class="add-form">
		<label>Ім'я: <input type="text" name="add_name" id="add_name"></label>
		<label><button class="addBtn" onclick="add_new_operation()">Додати Операцію</button></label>
	</div>
	</div>
  </div>
</div>
	<hr>
	<?
	$sql = "SELECT * FROM $table_operation ";
	$result = $link->query($sql);

	if ($result->num_rows > 0) {
		
	    echo '<div class="table">
	    			<div class="HRov">
	    				<div class="col">Код</div>
	    				<div class="col">Назва</div>
	    				<div class="col"></div>	    				
	    			</div>';
	    while($row = $result->fetch_assoc()) {


	        echo  '<div class="Rov">
	        			<div class="col no_pading">' . $row['operation_id'] . '</div>
	        			<div class="col no_pading"><input type="text" id="update_name' . $row['operation_id'] . '" value="' . $row['operation_name'] . '"></div>        		
	        			<div class="col no_pading"><button class="edit_row editBtn" onclick="edit_operation('. $row['operation_id'].')">Редагувати</button>
	        			</div>
	        		</div>';
	    }
	    echo '</div>';
	} else {
	    echo "Немає Операцій";
	}
	$link->close();

?></div>
<?