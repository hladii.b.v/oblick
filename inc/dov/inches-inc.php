<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_inches = 'dov_inches';

?>
<link rel="stylesheet" href="css/main.css">
<div id="contentinches">
	<h3>Довідник Дюймів</h3>
	<hr>
	
	<button id="button_action" onclick="window.location.href = '#global_inches';">Додати дюйми</button>
	<div id="global_inches" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати дюйми</h1>
    <h6>*введіть значення у поле</h6>
    <div class="add-form">
		<label>Значення дюймів: <input type="text" name="add_name" id="add_name"></label>
		<label><button class="addBtn" onclick="add_new_inches()">Додати дюйми</button></label>
	</div>
	</div>
  </div>
</div>
	<hr>
	<?
	$sql = "SELECT * FROM $table_inches";
	$result = $link->query($sql);

	if ($result->num_rows > 0) {
		
	    echo '<div class="table">
	    			<div class="HRov">
	    				<div class="col">Код</div>
	    				<div class="col">Значення дюймів</div>
	    				<div class="col"></div>	    				
	    			</div>';
	    while($row = $result->fetch_assoc()) {


	        echo  '<div class="Rov">
	        			<div class="col no_pading">' . $row['inches_id'] . '</div>
	        			<div class="col no_pading"><input type="text" id="update_name' . $row['inches_id'] . '" value="' . $row['inches_name'] . '"></div>     		
	        			<div class="col no_pading"><button class="edit_row editBtn" onclick="edit_inches('. $row['inches_id'].')">Редагувати</button>
	        			<button class="delete_row deleteBtn" onclick="delete_inches('. $row['inches_id'].')">Видалити</button></div>
	        		</div>';
	    }
	    echo '</div>';
	} else {
	    echo "Немає дюймів";
	}
	$link->close();

?></div>
<?