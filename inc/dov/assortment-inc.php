<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_assortment = 'dov_assortment';

?>
<link rel="stylesheet" href="css/main.css">
<div id="contentAssortment">
	<h3>Асортименти</h3>
	<hr>
	
	<button id="button_action" onclick="window.location.href = '#global_assortment';">Додати асортимент</button>
	<div id="global_assortment" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати асортимент</h1>
    <h6>*введіть значення у поле</h6>
    <div class="add-form">
		<label>Ім'я: <input assortment="text" name="add_name" id="add_name"></label>
		<label><button class="addBtn" onclick="add_new_assortment()">Додати асортимент</button></label>
	</div>
	</div>
  </div>
  </div>
</div>
	<hr>
	<?
	$sql = "SELECT * FROM $table_assortment ";
	$result = $link->query($sql);

	if ($result->num_rows > 0) {
		
	    echo '<div class="table">
	    			<div class="HRov">
	    				<div class="col">Код</div>
	    				<div class="col">Назва</div>
	    				<div class="col"></div>	    				
	    			</div>';
	    while($row = $result->fetch_assoc()) {


	        echo  '<div class="Rov">
	        			<div class="col no_pading">' . $row['assortment_id'] . '</div>
	        			<div class="col no_pading"><input assortment="text" id="update_name' . $row['assortment_id'] . '" value="' . $row['assortment_name'] . '"></div>        		
	        			<div class="col no_pading"><button class="edit_row editBtn" onclick="edit_assortment('. $row['assortment_id'].')">Редагувати</button>
	        			<button class="delete_row deleteBtn" onclick="delete_assortment('. $row['assortment_id'].')">Видалити</button></div>
	        		</div>';
	    }
	    echo '</div>';
	} else {
	    echo "Немає асортиментів";
	}
	$link->close();

?></div>
<?