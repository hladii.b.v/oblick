<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
$table_packingMaterialsConsumption = 'dov_packing_materials_consumption';

?>
<link rel="stylesheet" href="css/main.css">
<div id="contentPackingMaterialsConsumption">
	<h3>Споживачі пакувальних матеріалів</h3>
	<hr>
	
	<button id="button_action" onclick="window.location.href = '#global_packingMaterialsConsumption';">Додати споживача пакувальних матеріалів</button>
	<div id="global_packingMaterialsConsumption" class="modal-window">
  <div>
    <a href="#modal-close" title="Закрити" class="modal-close">Закрити &times;</a>
    <h1>Додати споживача пакувальних матеріалів</h1>
    <h6>*введіть значення у поле</h6>
    <div class="add-form">
		<label>Ім'я: <input type="text" name="add_name" id="add_name"></label>
		<label><button class="addBtn" onclick="add_new_packingMaterialsConsumption()">Додати споживача пакувальних матеріалів</button></label>
	</div>
	</div>
  </div>
  </div>
</div>
	<hr>
	<?
	$sql = "SELECT * FROM $table_packingMaterialsConsumption ";
    $result = $link->query($sql);
	if ($result->num_rows > 0) {
		
	    echo '<div class="table">
	    			<div class="HRov">
	    				<div class="col">Код</div>
	    				<div class="col">Назва</div>
	    				<div class="col"></div>	    				
	    			</div>';
	    while($row = $result->fetch_assoc()) {


	        echo  '<div class="Rov">
	        			<div class="col no_pading">' . $row['consumption_id'] . '</div>
	        			<div class="col no_pading"><input type="text" id="update_name' . $row['consumption_id'] . '" value="' . $row['consumption_name'] . '"></div>        		
	        			<div class="col no_pading"><button class="edit_row editBtn" onclick="edit_packingMaterialsConsumption('. $row['consumption_id'].')">Редагувати</button>
	        			<button class="delete_row deleteBtn" onclick="delete_packingMaterialsConsumption('. $row['consumption_id'].')">Видалити</button></div>
	        		</div>';
	    }
	    echo '</div>';
	} else {
	    echo "Немає споживачів пакувальних матеріалів";
	}
	$link->close();

?></div>
<?