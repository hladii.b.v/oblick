/* SEARCH FILTER v2 */
function filtersort(colum_id) {
        
    $(document).ready(function(){

  // Search all columns
  $('#txt_searchall').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    // Count total search result
    var len = $('table tbody tr:not(.notfound) td:contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row
      $('table tbody tr:not(.notfound) td:contains("'+search+'")').each(function(){
        $(this).closest('tr').show();
      });
    }else{
      $('.notfound').show();
    }

  });
  // <--------------------------------------------Search on name column only
 
  // <--------------------------------------------Search on code column only
  $('#txt_code').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    // Count total search result
    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(1):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(1):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=0;
          
    document.getElementById("val").innerHTML=rowCount;    

  });

 

        // <--------------------------------------------Search on last name column only
  $('#search_col2').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(2):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(2):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });
          // <--------------------------------------------Search on last name column only
  $('#search_col3').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();
    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(3):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(3):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });
   // <--------------------------------------------Search on last name column only
  $('#search_col4').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();
    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(4):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(4):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });
   // <--------------------------------------------Search on last name column only
  $('#search_col5').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();
    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(5):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(5):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });

   // <--------------------------------------------Search on last name column only
  $('#search_col6').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(6):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(6):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });

   // <--------------------------------------------Search on last name column only
  $('#search_col7').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    // Count total search result
    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(7):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(7):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });
   // <--------------------------------------------Search on last name column only
  $('#search_col8').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(8):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(8):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });

   // <--------------------------------------------Search on last name column only
  $('#search_col9').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(9):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(9):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });

 // <--------------------------------------------Search on last name column only
  $('#search_col10').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    // Count total search result
    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(10):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(10):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });
 // <--------------------------------------------Search on last name column only
  $('#search_col11').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(11):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(11):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });
   // <--------------------------------------------Search on last name column only
  $('#search_col12').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

   var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(12):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(12):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });
   // <--------------------------------------------Search on last name column only
  $('#search_col13').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

   var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(13):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(13):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });
   // <--------------------------------------------Search on last name column only
  $('#search_col14').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    // Count total search result
    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(14):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(14):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });

   $('#search_col15').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    // Count total search result
    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(15):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(15):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });

  $('#search_col16').keyup(function(){
      // Search Text
      var search = $(this).val();

      // Hide all table tbody rows
      $('table tbody tr').hide();

      // Count total search result
      var theTotal1 = 0;
      var theTotal2 = 0;
      var theTotal3 = 0;
      var theTotal4 = 0;

      var rowCount = 0;
      
      var len = $('table tbody tr:not(.notfound) td:nth-child(16):contains("'+search+'")').length;

      if(len > 0){
        // Searching text in columns and show match row

        $('table tbody tr:not(.notfound) td:nth-child(16):contains("'+search+'")').each(function(){
           $(this).closest('tr').show();  
            rowCount = $('#myTable tr:visible').length - 2;

           $("#myTable tr:visible td:nth-child(13)").each(function () {
                var val1 = $(this).text().replace(" ", "").replace(",-", "");
                
                theTotal1 += ','+val1;
                
            }); 
            $("#myTable tr:visible td:nth-child(14)").each(function () {
                var val2 = $(this).text().replace(" ", "").replace(",-", "");
                
                theTotal2 += ','+val2;
                
            });  
            $("#myTable tr:visible td:nth-child(15)").each(function () {
                var val3 = $(this).text().replace(" ", "").replace(",-", "");
                
                theTotal3 += ','+val3;
                
            });  
            $("#myTable tr:visible td:nth-child(16)").each(function () {
                var val4 = $(this).text().replace(" ", "").replace(",-", "");
                
                theTotal4 += ','+val4;
                
            });     
        
        });
      }else{
        $('.notfound').show();
      } 

      var comma = ',';
      theTotal1 = theTotal1.split( comma);
      theTotal2 = theTotal2.split( comma);
      theTotal3 = theTotal3.split( comma);
      theTotal4 = theTotal4.split( comma);
         
          
      var totalSum1 = 0;
          
        for (let i = rowCount+1; i > 1; i--) {
           totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
        }

      var totalSum2 = 0;
          
        for (let i = rowCount+1; i > 1; i--) {
           totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
        }

      var totalSum3 = 0;
          
        for (let i = rowCount+1; i > 1; i--) {
           totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
        }
      var totalSum4 = 0;
          
        for (let i = rowCount+1; i > 1; i--) {
           totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
        }


      document.getElementById("row_index1").innerHTML=totalSum1;
      document.getElementById("row_index2").innerHTML=totalSum2;
      document.getElementById("row_index3").innerHTML=totalSum3;
      document.getElementById("row_index4").innerHTML=totalSum4;
            
      document.getElementById("val").innerHTML=rowCount;   

  });

     $('#search_col17').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    // Count total search result
    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(17):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(17):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });

      $('#search_col18').keyup(function(){
    // Search Text
    var search = $(this).val();

    // Hide all table tbody rows
    $('table tbody tr').hide();

    // Count total search result
    var theTotal1 = 0;
    var theTotal2 = 0;
    var theTotal3 = 0;
    var theTotal4 = 0;

    var rowCount = 0;
    
    var len = $('table tbody tr:not(.notfound) td:nth-child(18):contains("'+search+'")').length;

    if(len > 0){
      // Searching text in columns and show match row

      $('table tbody tr:not(.notfound) td:nth-child(18):contains("'+search+'")').each(function(){
         $(this).closest('tr').show();  
          rowCount = $('#myTable tr:visible').length - 2;

         $("#myTable tr:visible td:nth-child(13)").each(function () {
              var val1 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal1 += ','+val1;
              
          }); 
          $("#myTable tr:visible td:nth-child(14)").each(function () {
              var val2 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal2 += ','+val2;
              
          });  
          $("#myTable tr:visible td:nth-child(15)").each(function () {
              var val3 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal3 += ','+val3;
              
          });  
          $("#myTable tr:visible td:nth-child(16)").each(function () {
              var val4 = $(this).text().replace(" ", "").replace(",-", "");
              
              theTotal4 += ','+val4;
              
          });     
      
      });
    }else{
      $('.notfound').show();
    } 

    var comma = ',';
    theTotal1 = theTotal1.split( comma);
    theTotal2 = theTotal2.split( comma);
    theTotal3 = theTotal3.split( comma);
    theTotal4 = theTotal4.split( comma);
       
        
    var totalSum1 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum1 = totalSum1+Number(theTotal1[theTotal1.length - i]);
      }

    var totalSum2 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum2 = totalSum2+Number(theTotal2[theTotal2.length - i]);
      }

    var totalSum3 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum3 = totalSum3+Number(theTotal3[theTotal3.length - i]);
      }
    var totalSum4 = 0;
        
      for (let i = rowCount+1; i > 1; i--) {
         totalSum4 = totalSum4+Number(theTotal4[theTotal4.length - i]);
      }


    document.getElementById("row_index1").innerHTML=totalSum1;
    document.getElementById("row_index2").innerHTML=totalSum2;
    document.getElementById("row_index3").innerHTML=totalSum3;
    document.getElementById("row_index4").innerHTML=totalSum4;
          
    document.getElementById("val").innerHTML=rowCount;   

  });


});

// Case-insensitive searching (Note - remove the below script for Case sensitive search )
$.expr[":"].contains = $.expr.createPseudo(function(arg) {
   return function( elem ) {
     return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
   };
});
}

/* SORT TABLE */

function sortingAscDesc(){
    $(function () {
        $('table')
            .on('click', 'th', function () {
                var index = $(this).index(),
                    rows = [],
                    thClass = $(this).hasClass('asc') ? 'desc' : 'asc';

                $('#myTable th').removeClass('asc desc');
                $(this).addClass(thClass);

                $('#myTable tbody tr').each(function (index, row) {
                    rows.push($(row).detach());
                });

                rows.sort(function (a, b) {
                    var aValue = $(a).find('td').eq(index).text(),
                        bValue = $(b).find('td').eq(index).text();

                    return aValue > bValue
                        ? 1
                        : aValue < bValue
                            ? -1
                            : 0;
                });

                if ($(this).hasClass('desc')) {
                    rows.reverse();
                }

                $.each(rows, function (index, row) {
                    $('#myTable tbody').append(row);
                });
            });
    });
}
/* END SORT TABLE */

