

 function Validatecheckbox(id) {

  var checked = document.querySelectorAll('input[type=checkbox].names:checked'),
    
    values = Array.prototype.slice.call(checked, 0)

    .map(function(checkbox) {
      return checkbox.value;
    });

  

    url = 'ajax_page/dov/employees.php?show=add_employees_operation&id='+id+'&values='+values;
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;   
                           
            }
        });
    
 


}
//type dov
function get_type() {

  url = 'ajax_page/dov/type.php?show=get_type';
  ajax_menu(url);

}

function add_new_type() {
  name = document.getElementById("add_name").value;
  if (name == '') {

    alert("Введіть назву");
  }

  else {
    url = 'ajax_page/dov/type.php?show=add_type&name=' + name;
    ajax_add(url);
  }

}

function delete_type(id) {
  text = 'Видалити тип з кодом №' + id;
  url = 'ajax_page/dov/type.php?show=delete_type&type_id=' + id;
  ajax_delete(url, text);

}
function edit_type(id) {

  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/type.php?show=edit_type&name=' + name + '&type_id=' + id;
  ajax_edit(url);
}

//class dov
function get_class() {

  url = 'ajax_page/dov/class.php?show=get_class';
  ajax_menu(url);


}

function add_new_class() {
  name = document.getElementById("add_name").value;
  if (name == '') {

    alert("Введіть назву");
  }

  else {
    url = 'ajax_page/dov/class.php?show=add_class&name=' + name;
    ajax_add(url);
  }

}

function delete_class(id) {
  text = 'Видалити клас з кодом №' + id;
  url = 'ajax_page/dov/class.php?show=delete_class&class_id=' + id;
  ajax_delete(url, text);

}
function edit_class(id) {

  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/class.php?show=edit_class&name=' + name + '&class_id=' + id;
  ajax_edit(url);
}
//assortment dov
function get_assortment() {

  url = 'ajax_page/dov/assortment.php?show=get_assortment';
  ajax_menu(url);

}

function add_new_assortment() {
  name = document.getElementById("add_name").value;
  if (name == '') {

    alert("Введіть назву");
  }

  else {
    url = 'ajax_page/dov/assortment.php?show=add_assortment&name=' + name;
    ajax_add(url);
  }

}

function delete_assortment(id) {
  text = 'Видалити асортимент з кодом №' + id;
  url = 'ajax_page/dov/assortment.php?show=delete_assortment&assortment_id=' + id;
  ajax_delete(url, text);

}
function edit_assortment(id) {

  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/assortment.php?show=edit_assortment&name=' + name + '&assortment_id=' + id;
  ajax_edit(url);
}
//season dov
function get_season() {

  url = 'ajax_page/dov/season.php?show=get_season';
  ajax_menu(url);

}

function add_new_season() {
  name = document.getElementById("add_name").value;
  if (name == '') {

    alert("Введіть назву");
  }

  else {
    url = 'ajax_page/dov/season.php?show=add_season&name=' + name;
    ajax_add(url);
  }

}

function delete_season(id) {
  text = 'Видалити сезон з кодом №' + id;
  url = 'ajax_page/dov/season.php?show=delete_season&season_id=' + id;
  ajax_delete(url, text);

}
function edit_season(id) {

  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/season.php?show=edit_season&name=' + name + '&season_id=' + id;
  ajax_edit(url);
}
//picture dov
function get_picture() {

  url = 'ajax_page/dov/picture.php?show=get_picture';
  ajax_menu(url);

}

function add_new_picture() {
  name = document.getElementById("add_name").value;
  if (name == '') {

    alert("Введіть назву");
  }

  else {
    url = 'ajax_page/dov/picture.php?show=add_picture&name=' + name;
    ajax_add(url);
  }

}

function delete_picture(id) {
  text = 'Видалити зображення з кодом №' + id;
  url = 'ajax_page/dov/picture.php?show=delete_picture&picture_id=' + id;
  ajax_delete(url, text);

}
function edit_picture(id) {

  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/picture.php?show=edit_picture&name=' + name + '&picture_id=' + id;
  ajax_edit(url);
}
//size dov
function get_size() {

  url = 'ajax_page/dov/size.php?show=get_size';
  ajax_menu(url);

}

function add_new_size() {
  name = document.getElementById("add_name").value;
  if (name == '') {

    alert("Введіть назву");
  }

  else {
    url = 'ajax_page/dov/size.php?show=add_size&name=' + name;
    ajax_add(url);
  }

}

function delete_size(id) {
  text = 'Видалити розмір з кодом №' + id;
  url = 'ajax_page/dov/size.php?show=delete_size&size_id=' + id;
  ajax_delete(url, text);

}
function edit_size(id) {

  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/size.php?show=edit_size&name=' + name + '&size_id=' + id;
  ajax_edit(url);
}
//article dov
function get_article() {

  url = 'ajax_page/dov/article.php?show=get_article';
  ajax_menu(url);

}

function add_new_article() {
  name = document.getElementById("add_name").value;
  type_id = document.getElementById("type_select").value;
  color_id = document.getElementById("color_select").value;
  assortment_id = document.getElementById("assortment_select").value;
  class_id = document.getElementById("class_select").value;
  season_id = document.getElementById("season_select").value;
  article_noted = document.getElementById("add_noted").value;
  if (name == '' || type_id == '' || color_id == '' || assortment_id == '' || class_id == '' || season_id == '' || article_noted == '') {

    alert("Заповніть всі поля.");
  }

  else {
    url = 'ajax_page/dov/article.php?show=add_article&name=' + name + '&type_id=' + type_id + '&color_id=' + color_id + '&assortment_id=' + assortment_id + '&class_id=' + class_id + '&season_id=' + season_id + '&article_noted=' + article_noted;
    ajax_add(url);
  }

}

function delete_article(id) {
  text = 'Видалити артикул з кодом №' + id;
  url = 'ajax_page/dov/article.php?show=delete_article&article_id=' + id;
  ajax_delete(url, text);

}
function edit_article(id) {

  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/article.php?show=edit_article&name=' + name + '&article_id=' + id;
  ajax_edit(url);
}
//color dov
function get_color() {

  url = 'ajax_page/dov/color.php?show=get_color';
  ajax_menu(url);

}

function add_new_color() {
  name = document.getElementById("add_name").value;
  if (name == '') {

    alert("Введіть назву");
  }

  else {
    url = 'ajax_page/dov/color.php?show=add_color&name=' + name;
    ajax_add(url);
  }

}

function delete_color(id) {
  text = 'Видалити колір з кодом №' + id;
  url = 'ajax_page/dov/color.php?show=delete_color&color_id=' + id;
  ajax_delete(url, text);

}
function edit_color(id) {

  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/color.php?show=edit_color&name=' + name + '&color_id=' + id;
  ajax_edit(url);
}
//packingMaterials dov
function get_packingMaterials() {

  url = 'ajax_page/dov/packingMaterials.php?show=get_packingMaterials';
  ajax_menu(url);

}

function add_new_packingMaterials() {
  name = document.getElementById("add_name").value;
  if (name == '') {

    alert("Введіть назву");
  }

  else {
    url = 'ajax_page/dov/packingMaterials.php?show=add_packingMaterials&name=' + name;
    ajax_add(url);
  }

}

function delete_packingMaterials(id) {
  text = 'Видалити колір з кодом №' + id;
  url = 'ajax_page/dov/packingMaterials.php?show=delete_packingMaterials&packing_materials_id=' + id;
  ajax_delete(url, text);

}
function edit_packingMaterials(id) {

  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/packingMaterials.php?show=edit_packingMaterials&name=' + name + '&packing_materials_id=' + id;
  ajax_edit(url);
}
//packingMaterialsProvider dov
function get_packingMaterialsProvider() {

  url = 'ajax_page/dov/packingMaterialsProvider.php?show=get_packingMaterialsProvider';
  ajax_menu(url);

}

function add_new_packingMaterialsProvider() {
  name = document.getElementById("add_name").value;
  if (name == '') {

    alert("Введіть назву");
  }

  else {
    url = 'ajax_page/dov/packingMaterialsProvider.php?show=add_packingMaterialsProvider&name=' + name;
    ajax_add(url);
  }

}

function delete_packingMaterialsProvider(id) {
  text = 'Видалити колір з кодом №' + id;
  url = 'ajax_page/dov/packingMaterialsProvider.php?show=delete_packingMaterialsProvider&provider_id=' + id;
  ajax_delete(url, text);

}
function edit_packingMaterialsProvider(id) {

  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/packingMaterialsProvider.php?show=edit_packingMaterialsProvider&name=' + name + '&provider_id=' + id;
  ajax_edit(url);
}
//packingMaterialsConsumption dov
function get_packingMaterialsConsumption() {

  url = 'ajax_page/dov/packingMaterialsConsumption.php?show=get_packingMaterialsConsumption';
  ajax_menu(url);

}

function add_new_packingMaterialsConsumption() {
  name = document.getElementById("add_name").value;
  if (name == '') {

    alert("Введіть назву");
  }

  else {
    url = 'ajax_page/dov/packingMaterialsConsumption.php?show=add_packingMaterialsConsumption&name=' + name;
    ajax_add(url);
  }

}

function delete_packingMaterialsConsumption(id) {
  text = 'Видалити колір з кодом №' + id;
  url = 'ajax_page/dov/packingMaterialsConsumption.php?show=delete_packingMaterialsConsumption&consumption_id=' + id;
  ajax_delete(url, text);

}
function edit_packingMaterialsConsumption(id) {

  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/packingMaterialsConsumption.php?show=edit_packingMaterialsConsumption&name=' + name + '&consumption_id=' + id;
  ajax_edit(url);
}
//packingMaterialsTerm dov
function get_packingMaterialsTerm() {

  url = 'ajax_page/dov/packingMaterialsTerm.php?show=get_packingMaterialsTerm';
  ajax_menu(url);

}

function add_new_packingMaterialsTerm() {
  name = document.getElementById("add_name").value;
  packing_materials_id = document.getElementById("packing_materials_select").value;
  if (name == '') {
    alert("Заповніть всі поля.");
  }
  else {
    url = 'ajax_page/dov/packingMaterialsTerm.php?show=add_packingMaterialsTerm&name=' + name + '&packing_materials_id=' + packing_materials_id;
    ajax_add(url);
  }
}

function delete_packingMaterialsTerm(id) {
  text = 'Видалити колір з кодом №' + id;
  url = 'ajax_page/dov/packingMaterialsTerm.php?show=delete_packingMaterialsTerm&term_id=' + id;
  ajax_delete(url, text);

}
function edit_packingMaterialsTerm(id) {
  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/packingMaterialsTerm.php?show=edit_packingMaterialsTerm&name=' + name + '&term_id=' + id;
  ajax_edit(url);
}
//packingMaterialsTermMeta dov
function get_packingMaterialsTermMeta() {

  url = 'ajax_page/dov/packingMaterialsTermMeta.php?show=get_packingMaterialsTermMeta';
  ajax_menu(url);

}

function add_new_packingMaterialsTermMeta() {
  name = document.getElementById("add_name").value;
  packing_materials_term_select = document.getElementById("packing_materials_term_select").value;
  if (name == '') {
    alert("Заповніть всі поля.");
  }
  else {
    url = 'ajax_page/dov/packingMaterialsTermMeta.php?show=add_packingMaterialsTermMeta&name=' + name + '&packing_materials_term_select=' + packing_materials_term_select;
    ajax_add(url);
  }
}

function delete_packingMaterialsTermMeta(id) {
  text = 'Видалити колір з кодом №' + id;
  url = 'ajax_page/dov/packingMaterialsTermMeta.php?show=delete_packingMaterialsTermMeta&meta_id=' + id;
  ajax_delete(url, text);

}
function edit_packingMaterialsTermMeta(id) {
  name = document.getElementById("update_name" + id).value;

  url = 'ajax_page/dov/packingMaterialsTermMeta.php?show=edit_packingMaterialsTermMeta&name=' + name + '&meta_id=' + id;
  ajax_edit(url);
}
//packingMaterialsMeta dov
function get_packingMaterialsMeta() {

  url = 'ajax_page/dov/packingMaterialsMeta.php?show=get_packingMaterialsMeta';
  ajax_menu(url);

}

function add_new_packingMaterialsMeta() {
  packing_materials_id = document.getElementById("packing_materials_select").value;
  meta_key = document.getElementById("select_meta_key").value;
  meta_value = (meta_key == 'Постачальники') ? document.getElementById("packing_materials_provider_select").value : document.getElementById("packing_materials_consumption_select").value;
  if (packing_materials_id == '') {

    alert("Заповніть всі поля.");
  }

  else {
    url = 'ajax_page/dov/packingMaterialsMeta.php?show=add_packingMaterialsMeta&packing_materials_id=' + packing_materials_id + '&meta_key=' + meta_key + '&meta_value=' + meta_value;
    ajax_add(url);
  }

}

function delete_packingMaterialsMeta(id) {
  text = 'Видалити колір з кодом №' + id;
  url = 'ajax_page/dov/packingMaterialsMeta.php?show=delete_packingMaterialsMeta&meta_id=' + id;
  ajax_delete(url, text);

}
function edit_packingMaterialsMeta(id) {
  url = 'ajax_page/dov/packingMaterialsMeta.php?show=edit_packingMaterialsMeta&meta_id=' + id;
  ajax_edit(url);
}
function packingMaterialsMeta_select_key() {
  meta_key = document.getElementById("select_meta_key").value;
  if (meta_key == 'Постачальники') {
    $('#label_packing_materials_provider_select').show();
    $('#label_packing_materials_consumption_select').hide();
  }
  else {
    $('#label_packing_materials_provider_select').hide();
    $('#label_packing_materials_consumption_select').show();
  }
}

//employees dov
function get_employees(){    
   
    url = 'ajax_page/dov/employees.php?show=get_employees';
    ajax_menu(url);
}

function add_new_employees(){    
    first_name = document.getElementById("add_first_name").value;
    last_name = document.getElementById("add_last_name").value;
    surname = document.getElementById("add_surname").value;
    if (first_name ==''){

        alert("Введіть ім'я");
    }
    else if (last_name ==''){

        alert("Введіть прізвище");
    }
    else if (surname ==''){

        alert("Введіть по-батькові");
    }
    else {
        url = 'ajax_page/dov/employees.php?show=add_employees&first_name='+first_name+'&last_name='+last_name+'&surname='+surname;
        ajax_add(url);
    }      

}

function delete_employees(id){    
    text = 'Видалити працівника з кодом №'+id;
    url = 'ajax_page/dov/employees.php?show=delete_employees&employees_id='+id;
    ajax_delete(url, text);          

}
function edit_employeesr(id){

    first_name = document.getElementById("update_first_name"+id).value;
    last_name = document.getElementById("update_last_name"+id).value;
    surname = document.getElementById("update_surname"+id).value;
    status = document.getElementById("update_status"+id).value;
    
    url = 'ajax_page/dov/employees.php?show=edit_employeesr&first_name='+first_name+'&last_name='+last_name+'&surname='+surname+'&status='+status+'&employees_id='+id;
    ajax_edit(url); 
}   


function add_emploer_oper(id){    
 
  url = 'ajax_page/dov/employees.php?show=get_employees&id='+id;
  modal_id = `#add_action_for_employee`;
  ajax_get_modal(url, modal_id);
  
}

function edit_emploer_modal(id){    
 
  url = 'ajax_page/dov/employees.php?show=get_employees&id='+id;
  modal_id = `#add_eding_for_employee`;
  ajax_get_modal(url, modal_id);
  
}
//operation dov
function get_operation(){    
   
    url = 'ajax_page/dov/operation.php?show=get_operation';
    ajax_menu(url);
          

}

function add_new_operation(){    
    name = document.getElementById("add_name").value;
    if (name ==''){

        alert("Введіть назву");
    }
   
    else {
        url = 'ajax_page/dov/operation.php?show=add_operation&name='+name;
        ajax_add(url);
    }      

}

function delete_operation(id){    
    text = 'Видалити операцію з кодом №'+id;
    url = 'ajax_page/dov/operation.php?show=delete_operation&operation_id='+id;
    ajax_delete(url, text);          

}
function edit_operation(id){

    name = document.getElementById("update_name"+id).value;
    
    url = 'ajax_page/dov/operation.php?show=edit_operation&name='+name+'&operation_id='+id;
    ajax_edit(url); 
}   
//knitting_machines

//models
function get_models(){    
   
    url = 'ajax_page/dov/models.php?show=get_models';
    ajax_menu(url);         

}

function add_new_models(){    
    name = document.getElementById("add_name").value;
    if (name ==''){

        alert("Введіть назву");
    }
   
    else {
        url = 'ajax_page/dov/models.php?show=add_models&name='+name;
        ajax_add(url);
    }      

}

function delete_models(id){    
    text = 'Видалити модель з кодом №'+id;
    url = 'ajax_page/dov/models.php?show=delete_models&models_id='+id;
    ajax_delete(url, text);          

}
function edit_models(id){

    name = document.getElementById("update_name"+id).value;
    
    url = 'ajax_page/dov/models.php?show=edit_models&name='+name+'&models_id='+id;
    ajax_edit(url); 
}  


//needles
function get_needles(){    
   
    url = 'ajax_page/dov/needles.php?show=get_needles';
    ajax_menu(url);         

}

function add_new_needles(){    
    name = document.getElementById("add_name").value;
    if (name ==''){

        alert("Введіть назву");
    }
   
    else {
        url = 'ajax_page/dov/needles.php?show=add_needles&name='+name;
        ajax_add(url);
    }      

}

function delete_needles(id){    
    text = 'Видалити голки з кодом №'+id;
    url = 'ajax_page/dov/needles.php?show=delete_needles&needles_id='+id;
    ajax_delete(url, text);          

}
function edit_needles(id){

    name = document.getElementById("update_name"+id).value;
    
    url = 'ajax_page/dov/needles.php?show=edit_needles&name='+name+'&needles_id='+id;
    ajax_edit(url); 
}  

//inches
function get_inches(){    
   
    url = 'ajax_page/dov/inches.php?show=get_inches';
    ajax_menu(url);         
   
}

function add_new_inches(){    
    name = document.getElementById("add_name").value;
    if (name ==''){

        alert("Введіть назву");
    }
   
    else {
        url = 'ajax_page/dov/inches.php?show=add_inches&name='+name;
        ajax_add(url);
    }      

}

function delete_inches(id){    
    text = 'Видалити дюми з кодом №'+id;
    url = 'ajax_page/dov/inches.php?show=delete_inches&inches_id='+id;
    ajax_delete(url, text);          

}
function edit_inches(id){

    name = document.getElementById("update_name"+id).value;
    
    url = 'ajax_page/dov/inches.php?show=edit_inches&name='+name+'&inches_id='+id;
    ajax_edit(url); 
} 

//machines

function get_knitting_machines(){    
   
    url = 'ajax_page/dov/knitting_machines.php?show=get_knitting_machines';
    ajax_menu(url);         
   
}

function add_new_knitting_machines(){    
    add_number = document.getElementById("add_number").value;
    add_models = document.getElementById("add_models").value;
    add_needles = document.getElementById("add_needles").value;
    add_inches = document.getElementById("add_inches").value;

    if (add_number ==''){

        alert("Введіть номер");
    }
   
    else {
        url = 'ajax_page/dov/knitting_machines.php?show=add_knitting_machines&add_number='+add_number+'&add_models='+add_models+'&add_needles='
        +add_needles+'&add_inches='+add_inches;
        ajax_add(url);
    }      

}

function delete_knitting_machines(id){    
    text = 'Видалити машину з кодом №'+id;
    url = 'ajax_page/dov/knitting_machines.php?show=delete_knitting_machines&machines_id='+id;
    ajax_delete(url, text);          

}
function edit_knitting_machines(id){

    update_number = document.getElementById("update_number"+id).value;
    update_models = document.getElementById("update_models"+id).value;
    update_needles = document.getElementById("update_needles"+id).value;
    update_inches = document.getElementById("update_inches"+id).value;    
    update_status = document.getElementById("update_status"+id).value;
    
    url = 'ajax_page/dov/knitting_machines.php?show=edit_knitting_machines&update_number='+update_number+'&update_models='+update_models+'&update_needles='
    +update_needles+'&update_inches='+update_inches+'&update_status='+update_status+'&machines_id='+id;
    ajax_edit(url); 
} 


//******//
//price

function get_price(id){    
   
   if ( id == undefined) {
     id=1
   }
    url = 'ajax_page/dov/price.php?show=get_price&price_id='+id;
    ajax_menu(url);         

}

function delete_price(id){    
    text = 'Видалити розцінку з кодом №'+id;
    url = 'ajax_page/dov/price.php?show=delete_price&price_id='+id;
    ajax_delete(url, text);          

}

function edit_price(id){

    start_date = document.getElementById("start_date"+id).value;
    end_date = document.getElementById("end_date"+id).value;
    price_value = document.getElementById("price_value"+id).value;
    price_name = document.getElementById("price_name"+id).value;
    
    url = 'ajax_page/dov/price.php?show=edit_price&start_date='+start_date+'&end_date='+end_date+'&price_value='
    +price_value+'&price_name='+price_name+'&price_id='+id;
    ajax_edit(url); 
} 

function add_new_price(){

    start_date = document.getElementById("start_date").value;
    end_date = document.getElementById("end_date").value;
    price_value = document.getElementById("price_value").value;
    price_name = document.getElementById("price_name").value;
    
    url = 'ajax_page/dov/price.php?show=add_price&start_date='+start_date+'&end_date='+end_date+'&price_value='
    +price_value+'&price_name='+price_name;
    ajax_edit(url); 
} 

//price atributs

//delete
function delete_price_oper(price_id, delete_id){    
    text = 'Видалити операцію для розцінки?';
    url = 'ajax_page/dov/price.php?show=delete_price_atribut&key=operation_id&price_id='+price_id+'&delete_id='+delete_id;
    ajax_delete(url, text);          

}
function delete_price_art(price_id, delete_id){    
    text = 'Видалити артикул для розцінки?';
    url = 'ajax_page/dov/price.php?show=delete_price_atribut&key=article_id&price_id='+price_id+'&delete_id='+delete_id;
    ajax_delete(url, text);          

}
function delete_price_type(price_id, delete_id){    
    text = 'Видалити тип для розцінки?';
    url = 'ajax_page/dov/price.php?show=delete_price_atribut&key=type_id&price_id='+price_id+'&delete_id='+delete_id;
    ajax_delete(url, text);          

}
function delete_price_asort(price_id, delete_id){    
    text = 'Видалити асортимент для розцінки?';
    url = 'ajax_page/dov/price.php?show=delete_price_atribut&key=assortment_id&price_id='+price_id+'&delete_id='+delete_id;
    ajax_delete(url, text);          

}
function delete_price_class(price_id, delete_id){    
    text = 'Видалити клас для розцінки?';
    url = 'ajax_page/dov/price.php?show=delete_price_atribut&key=class_id&price_id='+price_id+'&delete_id='+delete_id;
    ajax_delete(url, text);          

}
function delete_price_size(price_id, delete_id){    
    text = 'Видалити розмір для розцінки?';
    url = 'ajax_page/dov/price.php?show=delete_price_atribut&key=size_id&price_id='+price_id+'&delete_id='+delete_id;
    ajax_delete(url, text);          

}
function delete_price_gat(price_id, delete_id){    
    text = 'Видалити гатунок для розцінки?';
    url = 'ajax_page/dov/price.php?show=delete_price_atribut&key=gatunock&price_id='+price_id+'&delete_id='+delete_id;
    ajax_delete(url, text);          

}
function delete_price_needles(price_id, delete_id){    
    text = 'Видалити голки для розцінки?';
    url = 'ajax_page/dov/price.php?show=delete_price_atribut&key=needles_id&price_id='+price_id+'&delete_id='+delete_id;
    ajax_delete(url, text);          

}

//add
function add_price_needles(price_id){    
 
  url = 'ajax_page/dov/price.php?show=get_price&price_id='+price_id;
  modal_id = `#add_price_needles`;
  ajax_get_modal(url, modal_id);
  
}
function add_price_gat(price_id){    
 
  url = 'ajax_page/dov/price.php?show=get_price&price_id='+price_id;
  modal_id = `#add_price_gat`;
  ajax_get_modal(url, modal_id);
  
}
function add_price_size(price_id){    
 
  url = 'ajax_page/dov/price.php?show=get_price&price_id='+price_id;
  modal_id = `#add_price_size`;
  ajax_get_modal(url, modal_id);
  
}
function add_price_class(price_id){    
 
  url = 'ajax_page/dov/price.php?show=get_price&price_id='+price_id;
  modal_id = `#add_price_class`;
  ajax_get_modal(url, modal_id);
  
}
function add_price_asort(price_id){    
 
  url = 'ajax_page/dov/price.php?show=get_price&price_id='+price_id;
  modal_id = `#add_price_asort`;
  ajax_get_modal(url, modal_id);
  
}
function add_price_type(price_id){    
 
  url = 'ajax_page/dov/price.php?show=get_price&price_id='+price_id;
  modal_id = `#add_price_type`;
  ajax_get_modal(url, modal_id);
  
}
function add_price_art(price_id){    
 
  url = 'ajax_page/dov/price.php?show=get_price&price_id='+price_id;
  modal_id = `#add_price_art`;
  ajax_get_modal(url, modal_id);
  
}
function add_price_oper(price_id){    
 
  url = 'ajax_page/dov/price.php?show=get_price&price_id='+price_id;
  modal_id = `#add_price_oper`;
  ajax_get_modal(url, modal_id);
  
}

//safe
function safe_price_opr(price_id) {

    var checked = document.querySelectorAll('input[type=checkbox].names:checked'),    
    values = Array.prototype.slice.call(checked, 0)

    .map(function(checkbox) {
      return checkbox.value;
    });  

    url = 'ajax_page/dov/price.php?show=safe_price_atribut&key=operation_id&price_id='+price_id+'&values='+values;
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;   
                           
            }
     });
}

function safe_price_art(price_id) {

    var checked = document.querySelectorAll('input[type=checkbox].article:checked'),    
    values = Array.prototype.slice.call(checked, 0)

    .map(function(checkbox) {
      return checkbox.value;
    });  

    url = 'ajax_page/dov/price.php?show=safe_price_atribut&key=article_id&price_id='+price_id+'&values='+values;
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;   
                           
            }
     });
}
function safe_price_type(price_id) {

    var checked = document.querySelectorAll('input[type=checkbox].paraneter_type:checked'),    
    values = Array.prototype.slice.call(checked, 0)

    .map(function(checkbox) {
      return checkbox.value;
    });  

    url = 'ajax_page/dov/price.php?show=safe_price_atribut&key=type_id&price_id='+price_id+'&values='+values;
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;   
                           
            }
     });
}
function safe_price_asort(price_id) {

    var checked = document.querySelectorAll('input[type=checkbox].asort:checked'),    
    values = Array.prototype.slice.call(checked, 0)

    .map(function(checkbox) {
      return checkbox.value;
    });  

    url = 'ajax_page/dov/price.php?show=safe_price_atribut&key=assortment_id&price_id='+price_id+'&values='+values;
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;   
                           
            }
     });
}
function safe_price_class(price_id) {

    var checked = document.querySelectorAll('input[type=checkbox].class:checked'),    
    values = Array.prototype.slice.call(checked, 0)

    .map(function(checkbox) {
      return checkbox.value;
    });  

    url = 'ajax_page/dov/price.php?show=safe_price_atribut&key=class_id&price_id='+price_id+'&values='+values;
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;   
                           
            }
     });
}
function safe_price_size(price_id) {

    var checked = document.querySelectorAll('input[type=checkbox].size:checked'),    
    values = Array.prototype.slice.call(checked, 0)

    .map(function(checkbox) {
      return checkbox.value;
    });  

    url = 'ajax_page/dov/price.php?show=safe_price_atribut&key=size_id&price_id='+price_id+'&values='+values;
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;   
                           
            }
     });
}
function safe_price_needles(price_id) {

    var checked = document.querySelectorAll('input[type=checkbox].needles:checked'),    
    values = Array.prototype.slice.call(checked, 0)

    .map(function(checkbox) {
      return checkbox.value;
    });  

    url = 'ajax_page/dov/price.php?show=safe_price_atribut&key=needles_id&price_id='+price_id+'&values='+values;
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;   
                           
            }
  });
}
function safe_price_gatunock(price_id) {

    var checked = document.querySelectorAll('input[type=checkbox].gatunock:checked'),    
    values = Array.prototype.slice.call(checked, 0)

    .map(function(checkbox) {
      return checkbox.value;
    });  

    url = 'ajax_page/dov/price.php?show=safe_price_atribut&key=gatunock&price_id='+price_id+'&values='+values;
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;   
                           
            }
});
}


//******//


//sto1
//coming to stor1

function get_coming_stor1(){    
   
    url = 'ajax_page/stor/coming_to_stor1.php?show=get_coming_to_stor1';
    //ajax_menu(url);   
     $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;

            }
        });     
     
   
}
function add_new_coming_to_stor1(max_id){    
    add_date = document.getElementById("add_date").value;
    add_machines = document.getElementById("add_machines").value;
    add_master = document.getElementById("add_master").value;
    add_knitting = document.getElementById("add_knitting").value;
    add_article = document.getElementById("add_article").value;
    add_picture = document.getElementById("add_picture").value;
    add_size = document.getElementById("add_size").value;
    add_color = document.getElementById("add_color").value;
    gatunoc_1 = document.getElementById("gatunoc_1").value;
    gatunoc_2 = document.getElementById("gatunoc_2").value;
    gatunoc_3 = document.getElementById("gatunoc_3").value;
   


    if ( gatunoc_1 == '') {
       gatunoc_1=0;
    }
    if ( gatunoc_2 == '') {
       gatunoc_2=0;
    }
    if ( gatunoc_3 == '') {
       gatunoc_3=0;
    }
   

    url = 'ajax_page/stor/coming_to_stor1.php?show=add_coming_to_stor1&add_date='+add_date+'&add_machines='+add_machines+'&add_master='+add_master+'&add_knitting='+add_knitting
    +'&add_article='+add_article+'&add_picture='+add_picture+'&add_size='+add_size+'&add_color='+add_color+'&gatunoc_1='+gatunoc_1+'&gatunoc_2='+gatunoc_2+'&gatunoc_3='+gatunoc_3;
    //ajax_add(url); 
     $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;
                $("#barcode").JsBarcode(max_id);
                window.print();
            }
        });          
   
}
//consumption from stor1
function get_consumption_stor1(){    
   
    url = 'ajax_page/stor/consumption_from_stor1.php?show=get_coming_to_stor1';
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;
                 $('#bag_id').focus();
            }
        });         
   
}
function add_new_consumption_from_stor1(){    
    add_date = document.getElementById("add_date").value;
    bag_id = document.getElementById("bag_id").value;
    add_seamstress = document.getElementById("add_seamstress").value;  
    
    url = 'ajax_page/stor/consumption_from_stor1.php?show=add_new_consumption_from_stor1&add_date='+add_date+'&bag_id='+bag_id+'&add_seamstress='+add_seamstress;
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;
                 $('#bag_id').focus();
            }
        });           
   
}

//coming stor1



function coming_stor1(){    
    
    url = 'ajax_page/stor/coming_stor1.php?show=get_coming_stor1';
    //ajax_menu(url);  
     $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;              

            }
        });        
   
}
function coming_stor1_date(){    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value;
    url = 'ajax_page/stor/coming_stor1.php?show=get_coming_stor1&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
    ajax_menu(url);         
   
}

function delete_coming1(coming_id){   
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value; 
    text = 'Видалити прихід зі Складу 1?';
    url = 'ajax_page/stor/coming_stor1.php?show=delete_coming&coming_id='+coming_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
    ajax_delete(url, text);          

}

function edit_coming_modal1(coming_id){    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value; 
  url = 'ajax_page/stor/coming_stor1.php?show=update_coming&coming_id='+coming_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
  modal_id = `#coming-modal`;
  ajax_get_modal(url, modal_id);
  
}

function eding_coming_to_stor1(coming_id, bag_id){    
    add_date = document.getElementById("add_date").value;
    add_machines = document.getElementById("add_machines").value;
    add_master = document.getElementById("add_master").value;
    add_knitting = document.getElementById("add_knitting").value;
    add_article = document.getElementById("add_article").value;
    add_picture = document.getElementById("add_picture").value;
    add_size = document.getElementById("add_size").value;
    add_color = document.getElementById("add_color").value;
    gatunoc_1 = document.getElementById("gatunoc_1").value;
    gatunoc_2 = document.getElementById("gatunoc_2").value;
    gatunoc_3 = document.getElementById("gatunoc_3").value;

    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value;
   

    url = 'ajax_page/stor/coming_stor1.php?show=set_update_coming&add_date='+add_date+'&add_machines='+add_machines+'&add_master='+add_master+'&add_knitting='+add_knitting
    +'&add_article='+add_article+'&add_picture='+add_picture+'&add_size='+add_size+'&add_color='+add_color+'&gatunoc_1='+gatunoc_1+'&gatunoc_2='+gatunoc_2+'&gatunoc_3='+gatunoc_3
    +'&coming_id='+coming_id+'&bag_id='+bag_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
   $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;
                $("#barcode").JsBarcode(bag_id);
                window.print();
            }
        });         
   
}

//consumption stor1

function consumption_stor1(){    
    
    url = 'ajax_page/stor/consumption_stor1.php?show=get_consumption_stor1';
    ajax_menu(url);         
   
}
function consumption_stor1_date(){    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value;
    url = 'ajax_page/stor/consumption_stor1.php?show=get_consumption_stor1&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
    ajax_menu(url);         
   
}

function delete_consumption1(consumption_id){   
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value; 
    text = 'Видалити розхід зі Складу 1?';
    url = 'ajax_page/stor/consumption_stor1.php?show=delete_consumptiong&consumption_id='+consumption_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
    ajax_delete(url, text);          

}

function edit_consumption_modal1(consumption_id){    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value; 
  url = 'ajax_page/stor/consumption_stor1.php?show=update_consumption&consumption_id='+consumption_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
  modal_id = `#consumption-modal`;
  ajax_get_modal(url, modal_id);
  
}

function add_new_consumption_to_stor1(consumption_id){    
    add_date = document.getElementById("add_date").value;
    add_accepted = document.getElementById("add_accepted").value;
    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value;
   

    url = 'ajax_page/stor/consumption_stor1.php?show=set_update_consumption&add_date='+add_date +'&add_accepted='+add_accepted
    +'&consumption_id='+consumption_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
    ajax_add(url);         
   
}

//remainder stor1

function get_remainder_stor1(){    
    
    url = 'ajax_page/stor/remainder_stor1.php?show=get_remainder_stor1';
    ajax_menu(url);         
   
}
function remainder_stor1_date(){    
    datepicker_first = document.getElementById("datepicker_first").value;
    url = 'ajax_page/stor/remainder_stor1.php?show=get_remainder_stor1&datepicker_first='+datepicker_first;
    ajax_menu(url);         
   
}

//coming stor2
function coming_stor2(){    
    
    url = 'ajax_page/stor/coming_stor2.php?show=get_coming_stor2';
    ajax_menu(url);         
   
}
function coming_stor2_date(){    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value;
    url = 'ajax_page/stor/coming_stor2.php?show=get_coming_stor2&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
    ajax_menu(url);         
   
}

function delete_coming2(coming_id){   
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value; 
    text = 'Видалити розхід зі Складу 2?';
    url = 'ajax_page/stor/coming_stor2.php?show=delete_coming&coming_id='+coming_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
    ajax_delete(url, text);          

}

function edit_coming_modal2(coming_id){    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value; 
  url = 'ajax_page/stor/coming_stor2.php?show=update_coming&coming_id='+coming_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
  modal_id = `#coming-modal`;
  ajax_get_modal(url, modal_id);
  
}

function edit_coming_modal2_big(coming_id){    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value; 
  url = 'ajax_page/stor/coming_stor2.php?show=update_coming_big&coming_id='+coming_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
  modal_id = `#coming-modal_big`;
  ajax_get_modal(url, modal_id);
  
}

function add_new_coming_to_stor2(coming_id){    
    add_date = document.getElementById("add_date").value;
    add_accepted = document.getElementById("add_accepted").value;

    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value;
   

    url = 'ajax_page/stor/coming_stor2.php?show=set_update_coming&add_date='+add_date +'&add_accepted='+add_accepted
    +'&coming_id='+coming_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
    ajax_add(url);         
   
}
function add_new_coming_to_stor2_big(coming_id, bag_id){    
    add_date = document.getElementById("add_date").value;
    add_accepted = document.getElementById("add_accepted").value;

    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value;
   
    add_article = document.getElementById("add_article").value;
    add_picture = document.getElementById("add_picture").value;
    add_size = document.getElementById("add_size").value;
    add_color = document.getElementById("add_color").value;
    gatunoc_1 = document.getElementById("gatunoc_1").value;
    gatunoc_2 = document.getElementById("gatunoc_2").value;
    gatunoc_3 = document.getElementById("gatunoc_3").value;

    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value;
   

   

    url = 'ajax_page/stor/coming_stor2.php?show=set_update_coming_big&add_date='+add_date +'&add_accepted='+add_accepted
    +'&coming_id='+coming_id+'&add_article='+add_article+'&add_picture='+add_picture+'&add_size='+add_size+'&add_color='+add_color+'&gatunoc_1='+gatunoc_1+'&gatunoc_2='+gatunoc_2+'&gatunoc_3='+gatunoc_3
    +'&coming_id='+coming_id+'&bag_id='+bag_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
    ajax_add(url);         
   
}

//consumption stor2

function consumption_stor2(){    
    
    url = 'ajax_page/stor/consumption_stor2.php?show=get_consumption_stor2';
    ajax_menu(url);         
   
}
function consumption_stor2_date(){    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value;
    url = 'ajax_page/stor/consumption_stor2.php?show=get_consumption_stor2&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
    ajax_menu(url);         
   
}

function delete_consumption2(consumption_id){   
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value; 
    text = 'Видалити розхід зі Складу 2?';
    url = 'ajax_page/stor/consumption_stor2.php?show=delete_consumptiong&consumption_id='+consumption_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
    ajax_delete(url, text);          

}

function edit_consumption_modal2(consumption_id){    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value; 
  url = 'ajax_page/stor/consumption_stor2.php?show=update_consumption&consumption_id='+consumption_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
  modal_id = `#consumption-modal`;
  ajax_get_modal(url, modal_id);
  
}

function add_new_consumption_to_stor2(consumption_id){    
    add_date = document.getElementById("add_date").value;
    add_accepted = document.getElementById("add_accepted").value;
    
    datepicker_first = document.getElementById("datepicker_first").value;
    datepicker_last = document.getElementById("datepicker_last").value;
   

    url = 'ajax_page/stor/consumption_stor2.php?show=set_update_consumption&add_date='+add_date +'&add_accepted='+add_accepted
    +'&consumption_id='+consumption_id+'&datepicker_first='+datepicker_first+'&datepicker_last='+datepicker_last;
    ajax_add(url);         
   
}
//remainder stor2

function get_remainder_stor2(){    
    
    url = 'ajax_page/stor/remainder_stor2.php?show=get_remainder_stor2';
    ajax_menu(url);         
   
}
function remainder_stor2_date(){    
    datepicker_first = document.getElementById("datepicker_first").value;
    url = 'ajax_page/stor/remainder_stor2.php?show=get_remainder_stor2&datepicker_first='+datepicker_first;
    ajax_menu(url);         
   
}

//coming to stor2

function get_coming_stor2(){    
   
    url = 'ajax_page/stor/coming_to_stor2.php?show=get_coming_to_stor2';
    ajax_menu(url);         
   
}
function add_new_coming_from_stor2(){    
    add_date = document.getElementById("add_date").value;
    add_seamstress = document.getElementById("add_seamstress").value;  
    add_article = document.getElementById("add_article").value;
    add_picture = document.getElementById("add_picture").value;
    add_size = document.getElementById("add_size").value;
    add_color = document.getElementById("add_color").value;
    gatunoc_1 = document.getElementById("gatunoc_1").value;
    gatunoc_2 = document.getElementById("gatunoc_2").value;
    gatunoc_3 = document.getElementById("gatunoc_3").value;

    if ( gatunoc_1 == '') {
       gatunoc_1=0;
    }
    if ( gatunoc_2 == '') {
       gatunoc_2=0;
    }
    if ( gatunoc_3 == '') {
       gatunoc_3=0;
    }

        
    url = 'ajax_page/stor/coming_to_stor2.php?show=add_coming_to_stor2&add_date='+add_date+'&add_seamstress='+add_seamstress+'&add_article='+add_article+'&add_picture='
    +add_picture+'&add_size='+add_size+'&add_color='+add_color+'&gatunoc_1='+gatunoc_1+'&gatunoc_2='+gatunoc_2+'&gatunoc_3='+gatunoc_3;
    ajax_add(url);         
   
}

//consumption from stor2
function get_consumption_stor2(){    
   
    url = 'ajax_page/stor/consumption_from_stor2.php?show=get_coming_to_stor2';
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;
                 $('#bag_id').focus();
            }
        });         
   
}
function add_new_consumption_from_stor2(){    
    add_date = document.getElementById("add_date").value;
    bag_id = document.getElementById("bag_id").value;
    add_seamstress = document.getElementById("add_seamstress").value;  
    
    url = 'ajax_page/stor/consumption_from_stor2.php?show=add_new_consumption_from_stor2&add_date='+add_date+'&bag_id='+bag_id+'&add_seamstress='+add_seamstress;
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;
                 $('#bag_id').focus();
            }
        });           
   
}

//******ajax for all

function ajax_get_modal(get_url, modal_id){
    $.ajax({ 
            url: url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;   
                window.location.href = modal_id;               
            }
        });
}

function ajax_menu(get_url){
    $.ajax({ 
            url: get_url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;
                
            }
        });
}

function ajax_add(get_url){
    $.ajax({ 
            url: get_url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;
                
            }
        });
}


function ajax_delete(request_url, text){
    var retVal = confirm(text);
    if( retVal == true ) {
        $.ajax({ 
            url: request_url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;                  
            }
        });
        return true;
    } 
    else {                  
        return false;
    }
}

function ajax_edit(get_url){
    $.ajax({ 
            url: get_url,
            type: 'POST',
            success: function (response) {
                document.getElementById("conteiner").innerHTML=response;
                
            }
        });
}




