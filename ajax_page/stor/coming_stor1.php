<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";


$bag_table = 'bag_table';
$store1_coming_table = 'store1_coming';
$store1_consumption_table = 'store1_consumption';
$stor1_remainder_table = 'stor1_remainder';

$table_knitting_machines = 'dov_knitting_machines';
$table_needles = 'dov_needles';
$table_models = 'dov_models';
$table_employees = 'dov_employees';
$table_employees_meta = 'employees_meta';
$table_users = 'users';
$table_article = 'dov_article';
$table_picture = 'dov_picture';
$table_size = 'dov_size';
$table_color = 'dov_color';
$table_class = 'dov_class';
$table_type = 'dov_type';
$table_assortment = 'dov_assortment';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show form
	if ( $do_action =='get_coming_stor1') {
		$today = date("Y-m-d",strtotime( "0 days"));

		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];

		if ($datepicker_first == ''  ) {
			$datepicker_first = date("Y-m-01", strtotime($today));
			
		}
		if ( $datepicker_last == '' ) {
			$datepicker_last = date("Y-m-t", strtotime($today));
		}

		Include "../../inc/stor/coming_stor1-inc.php";
	}
	//delete_coming
	elseif ( $do_action =='delete_coming') {

		$bag_id = $_GET['coming_id'];
		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];

		
		$check_to_delete = $link->query("SELECT consumption_id FROM $store1_consumption_table WHERE `bag_id`='$bag_id'");
		if ($check_to_delete->num_rows > 0) {

			echo "Мішок в ". $bag_id." розході";
		}
		else {
			$result_remainder = $link->query("DELETE FROM $stor1_remainder_table WHERE `bag_id`='$bag_id'");
			$sql_delete="DELETE FROM $store1_coming_table WHERE `bag_id`='$bag_id'";
			$result_deletet = $link->query($sql_delete);
		}
		

		Include "../../inc/stor/coming_stor1-inc.php";
	}
	elseif ( $do_action =='set_update_coming') {

		$coming_id = $_GET['coming_id'];
		$bag_id = $_GET['bag_id'];
		$today = date("Y-m-d",strtotime( "0 days"));
		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];
		
		$add_date = $_GET['add_date'];
		$add_machines = $_GET['add_machines'];
		$add_master = $_GET['add_master'];
		$add_knitting = $_GET['add_knitting'];
		$add_article = $_GET['add_article'];
		$add_picture = $_GET['add_picture'];
		$add_size = $_GET['add_size'];
		$add_color = $_GET['add_color'];
		$gatunoc_1 = $_GET['gatunoc_1'];
		$gatunoc_2 = $_GET['gatunoc_2'];
		$gatunoc_3 = $_GET['gatunoc_3'];
		$user_id = $_SESSION["id"];
		
		$update_store = $link->query("UPDATE `store1_coming` SET `date`='$add_date',`machines_id`='$add_machines',`master_id`='$add_master',`knitting_id`='$add_knitting',`curent_date`='$today',`user_id`='$user_id ' WHERE coming_id='$coming_id'");

		$sql =  $link->query("UPDATE $bag_table SET `article_id`='$add_article', `picture_id`='$add_picture', `size_id`='$add_size', `color_id`='$add_color', `coun1`='$gatunoc_1', `coun2`='$gatunoc_2', `coun3`='$gatunoc_3' WHERE bag_id='$bag_id'");
		
		echo '';

		Include "../../inc/stor/coming_stor1-inc.php";
	}
	elseif ( $do_action =='update_coming') {

		$coming_id = $_GET['coming_id'];
		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];

		$get_coming_modal = $link->query("SELECT * FROM $store1_coming_table WHERE `coming_id`='$coming_id' " );
		while($row_get_coming_modal = $get_coming_modal->fetch_assoc()) {

			$bag_id = $row_get_coming_modal['bag_id'];
	    	$machines_id = $row_get_coming_modal['machines_id'];
	    	$master_id = $row_get_coming_modal['master_id'];
	    	$knitting_id = $row_get_coming_modal['knitting_id'];
	    	$date = $row_get_coming_modal['date'];
		}
		//**selects

		//machines
		$result_machines_small = $link->query("SELECT k.machines_id, k.machines_number, m.model_name, n.needles_name FROM $table_knitting_machines k LEFT JOIN  $table_models m ON k.model_id=m.model_id LEFT JOIN  $table_needles n ON k.needles_id=n.needles_id WHERE k.machines_status='1' AND k.machines_id='$machines_id'");
		$result_machines = $link->query("SELECT k.machines_id, k.machines_number, m.model_name, n.needles_name FROM $table_knitting_machines k LEFT JOIN  $table_models m ON k.model_id=m.model_id LEFT JOIN  $table_needles n ON k.needles_id=n.needles_id WHERE k.machines_status='1'  AND k.machines_id!='$machines_id' ORDER BY k.machines_number");
		//employeers
		$result_master_small = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='2' AND e.employees_id='$master_id'");
		$result_master = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='2' AND e.employees_id!='$master_id' ORDER BY e.employees_id");
		$result_knitting_small = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='3' AND e.employees_id='$knitting_id'");
		$result_knitting = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='3'  AND e.employees_id!='$knitting_id' ORDER BY e.employees_id ");

		//parameters

		$get_bag_modal = $link->query("SELECT * FROM $bag_table WHERE `bag_id`='$bag_id' " );
		while($row_get_bag_modal = $get_bag_modal->fetch_assoc()) {

			$article_id = $row_get_bag_modal['article_id'];
	    	$picture_id = $row_get_bag_modal['picture_id'];
	    	$size_id = $row_get_bag_modal['size_id'];
	    	$color_id = $row_get_bag_modal['color_id'];
	    	$coun1 = $row_get_bag_modal['coun1'];
	    	$coun2 = $row_get_bag_modal['coun2'];
	    	$coun3 = $row_get_bag_modal['coun3'];
		}
		$result_article_small = $link->query("SELECT * FROM $table_article WHERE article_id='$article_id'");
		$result_article = $link->query("SELECT * FROM $table_article WHERE article_id!='$article_id'");
		$result_picture_small = $link->query("SELECT * FROM $table_picture WHERE picture_id='$picture_id'");
		$result_picture = $link->query("SELECT * FROM $table_picture WHERE picture_id!='$picture_id'");
		$result_size_small = $link->query("SELECT * FROM $table_size WHERE size_id='$size_id'");
		$result_size = $link->query("SELECT * FROM $table_size WHERE size_id!='$size_id'");
		$result_color_small = $link->query("SELECT * FROM $table_color WHERE color_id='$color_id'");
		$result_color = $link->query("SELECT * FROM $table_color WHERE color_id!='$color_id'");

		//**return

		//machines
		$all_machines = '';
		while($row_machines = $result_machines_small->fetch_assoc()) {
				
			$all_machines .= '<option value="'. $row_machines['machines_id'].'">#'.$row_machines['machines_number'].' '.$row_machines['model_name'].' ('. $row_machines['needles_name'].' г)</option>';
		}
		
		while($row_machines = $result_machines->fetch_assoc()) {
			
			$all_machines .= '<option value="'. $row_machines['machines_id'].'">#'.$row_machines['machines_number'].' '.$row_machines['model_name'].' ('. $row_machines['needles_name'].' г)</option>';
		}
		
		
		//employeers
		$all_master = '';

		while($row_master = $result_master_small->fetch_assoc()) {
			$full_name = '';
			$first_name = substr($row_master['first_name'], 0, 2);
			$surname =  substr($row_master['surname'], 0, 2);

			$full_name =  $row_master['last_name'].' '.$first_name.'.'. $surname.'.';
			$all_master .= '<option value="'. $row_master['employees_id'].'">'. $full_name .'</option>';
		}
		
		while($row_master = $result_master->fetch_assoc()) {
			$full_name = '';
			$first_name = substr($row_master['first_name'], 0, 2);
			$surname =  substr($row_master['surname'], 0, 2);

			$full_name =  $row_master['last_name'].' '.$first_name.'.'. $surname.'.';
			$all_master .= '<option value="'. $row_master['employees_id'].'">'. $full_name .'</option>';
		}
		
		$all_knitting = '';
		while($row_knitting = $result_knitting_small->fetch_assoc()) {
			$full_name = '';
			$first_name = substr($row_knitting['first_name'], 0, 2);
			$surname =  substr($row_knitting['surname'], 0, 2);

			$full_name =  $row_knitting['last_name'].' '.$first_name.'.'. $surname.'.';
			$all_knitting .= '<option value="'. $row_knitting['employees_id'].'">'. $full_name .'</option>';
		}
		
		while($row_knitting = $result_knitting->fetch_assoc()) {
			$full_name = '';
			$first_name = substr($row_knitting['first_name'], 0, 2);
			$surname =  substr($row_knitting['surname'], 0, 2);

			$full_name =  $row_knitting['last_name'].' '.$first_name.'.'. $surname.'.';
			$all_knitting .= '<option value="'. $row_knitting['employees_id'].'">'. $full_name .'</option>';
		}
		

		//parameters
		$all_article = '';
		while($row_article = $result_article_small->fetch_assoc()) {
			
			$all_article .= '<option value="'. $row_article['article_id'].'">'. $row_article['article_name'] .'</option>';
		}
		
		while($row_article = $result_article->fetch_assoc()) {
			
			$all_article .= '<option value="'. $row_article['article_id'].'">'. $row_article['article_name'] .'</option>';
		}
		

		$all_picture = '';
		
		while($row_picture = $result_picture_small->fetch_assoc()) {
			
			$all_picture .= '<option value="'. $row_picture['picture_id'].'">'. $row_picture['picture_name'] .'</option>';
		}
		while($row_picture = $result_picture->fetch_assoc()) {
			
			$all_picture .= '<option value="'. $row_picture['picture_id'].'">'. $row_picture['picture_name'] .'</option>';
		}
	

		$all_size = '';
		while($row_size = $result_size_small->fetch_assoc()) {
			
			$all_size .= '<option value="'. $row_size['size_id'].'">'. $row_size['size_name'] .'</option>';
		}
	
		while($row_size = $result_size->fetch_assoc()) {
			
			$all_size .= '<option value="'. $row_size['size_id'].'">'. $row_size['size_name'] .'</option>';
		}
	
		$all_color = '';
		while($row_color = $result_color_small->fetch_assoc()) {
			
			$all_color .= '<option value="'. $row_color['color_id'].'">'. $row_color['color_name'] .'</option>';
		}
		while($row_color = $result_color->fetch_assoc()) {
			
			$all_color .= '<option value="'. $row_color['color_id'].'">'. $row_color['color_name'] .'</option>';
		}
	


		Include "../../inc/stor/coming_stor1-inc.php";
	}
	
}