<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$store1_consumption_table = 'store1_consumption';
$store1_coming_table = 'store1_coming';
$stor1_remainder_table = 'stor1_remainder';
$store2_coming_table = 'store2_coming';
$stor2_remainder_table = 'stor2_remainder';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show form
	if ( $do_action =='get_coming_to_stor1') {
		$today = date("Y-m-d",strtotime( "0 days"));
		Include "../../inc/stor/consumption_from_stor1-inc.php";
	}
	//add_new_consumption_from_stor1
	elseif ( $do_action =='add_new_consumption_from_stor1') {

		$add_date = $_GET['add_date'];
		$bag_id = $_GET['bag_id'];
		$add_seamstress = $_GET['add_seamstress'];
		$user_id = $_SESSION["id"];
		

		$today = date("Y-m-d",strtotime( "0 days"));
		
		$coming_check = $link->query("SELECT bag_id FROM $store1_coming_table  WHERE bag_id='$bag_id' ");
		$consumption_check = $link->query("SELECT bag_id FROM $store1_consumption_table  WHERE `bag_id`='$bag_id' ");
		
		if ($coming_check->num_rows < 1) {
			echo 'Мішок з кодом '.$bag_id. ' на ділянку не приходив';
		}
		elseif ($consumption_check->num_rows  > 0 ) {
			echo 'Мішок з кодом '.$bag_id. ' вже в розході';
		}
		else {
			$insert_store = $link->query("INSERT INTO $store1_consumption_table (`bag_id`, `date`,  `area`, `accepted`, `curent_date`, `user_id`) VALUES ('$bag_id',  '$add_date', 'Склад 2', '$add_seamstress',  '$today', '$user_id')");
			$insert_store2 = $link->query("INSERT INTO $store2_coming_table (`bag_id`, `date`,  `area`, `accepted`, `curent_date`, `user_id`) VALUES ('$bag_id',  '$add_date', 'Склад 1', '$add_seamstress',  '$today', '$user_id')");

			$stor2_remainder = $link->query("INSERT INTO $stor2_remainder_table (`bag_id`, `coming_date`) VALUES ('$bag_id', '$add_date')");

			$update_store = $link->query("UPDATE $stor1_remainder_table SET `consumption_date`='$today' WHERE bag_id='$bag_id'");
			echo 'Мішок з кодом '.$bag_id. ' видано швеї';
		}		
		Include "../../inc/stor/consumption_from_stor1-inc.php";
	}
	
}