<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";


$bag_table = 'bag_table';
$store2_consumption_table = 'store2_consumption';
$stor2_remainder_table = 'stor2_remainder';
$store3_coming_table = 'store3_coming';

$table_employees = 'dov_employees';
$table_employees_meta = 'employees_meta';
$table_users = 'users';
$table_article = 'dov_article';
$table_picture = 'dov_picture';
$table_size = 'dov_size';
$table_color = 'dov_color';
$table_class = 'dov_class';
$table_type = 'dov_type';
$table_assortment = 'dov_assortment';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show form
	if ( $do_action =='get_consumption_stor2') {
		$today = date("Y-m-d",strtotime( "0 days"));

		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];

		if ($datepicker_first == ''  ) {
			$datepicker_first = date("Y-m-01", strtotime($today));
			
		}
		if ( $datepicker_last == '' ) {
			$datepicker_last = date("Y-m-t", strtotime($today));
		}

		Include "../../inc/stor/consumption_stor2-inc.php";
	}
	//delete_consumptiong
	elseif ( $do_action =='delete_consumptiong') {

		$bag_id = $_GET['consumption_id'];
		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];

		$update_store = $link->query("UPDATE $stor2_remainder_table SET `consumption_date`=NULL  WHERE bag_id='$bag_id'");
		


		$result_deletet2 = $link->query("DELETE FROM $store3_coming_table WHERE `bag_id`='$bag_id'");
		$result_deletet1 = $link->query("DELETE FROM $store2_consumption_table WHERE `bag_id`='$bag_id'");

		Include "../../inc/stor/consumption_stor2-inc.php";
	}
	elseif ( $do_action =='set_update_consumption') {

		$bag_id = $_GET['consumption_id'];
		$today = date("Y-m-d",strtotime( "0 days"));
		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];
		$user_id = $_SESSION["id"];
		
		$add_date = $_GET['add_date'];
		$add_accepted = $_GET['add_accepted'];
		
		
		$update_store2 = $link->query("UPDATE $store2_consumption_table SET `date`='$add_date',`accepted`='$add_accepted', `curent_date`='$today',`user_id`='$user_id ' WHERE bag_id='$bag_id'");
		$update_store3 = $link->query("UPDATE $store3_coming_table SET `date`='$add_date',`accepted`='$add_accepted', `curent_date`='$today',`user_id`='$user_id ' WHERE bag_id='$bag_id'");			

		Include "../../inc/stor/consumption_stor2-inc.php";
	}
	elseif ( $do_action =='update_consumption') {

		$consumption_id = $_GET['consumption_id'];
		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];

		$get_coming_modal = $link->query("SELECT * FROM $store2_consumption_table WHERE `consumption_id`='$consumption_id' " );
		while($row_get_coming_modal = $get_coming_modal->fetch_assoc()) {

			$accepted = $row_get_coming_modal['accepted'];
	    	$date = $row_get_coming_modal['date'];
		}
		//**selects

		//employeers
		$result_accepted_small = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='4' AND e.employees_id='$accepted'");
		$result_accepted = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='4' AND e.employees_id!='$accepted' ORDER BY e.employees_id");
		
		//**return	
		
		//employeers
		$all_accepted = '';
		
		while($row_accepted = $result_accepted_small->fetch_assoc()) {
			$full_name = '';
			$first_name = substr($row_accepted['first_name'], 0, 2);
			$surname =  substr($row_accepted['surname'], 0, 2);

			$full_name =  $row_accepted['last_name'].' '.$first_name.'.'. $surname.'.';
			$all_accepted .= '<option value="'. $row_accepted['employees_id'].'">'. $full_name .'</option>';
		}
		
		while($row_accepted = $result_accepted->fetch_assoc()) {
			$full_name = '';
			$first_name = substr($row_accepted['first_name'], 0, 2);
			$surname =  substr($row_accepted['surname'], 0, 2);

			$full_name =  $row_accepted['last_name'].' '.$first_name.'.'. $surname.'.';
			$all_accepted .= '<option value="'. $row_accepted['employees_id'].'">'. $full_name .'</option>';
		}


		Include "../../inc/stor/consumption_stor2-inc.php";
	}
	
}