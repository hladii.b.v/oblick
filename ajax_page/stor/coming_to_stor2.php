<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$bag_table = 'bag_table';
$store2_coming_table = 'store2_coming';
$stor2_remainder_table = 'stor2_remainder';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show form
	if ( $do_action =='get_coming_to_stor2') {

		$today = date("Y-m-d",strtotime( "0 days"));
		Include "../../inc/stor/coming_to_stor2-inc.php";
	}
	//add_coming
	elseif ( $do_action =='add_coming_to_stor2') {

		$add_date = $_GET['add_date'];
		$accepted = $_GET['add_seamstress'];
		$add_article = $_GET['add_article'];
		$add_picture = $_GET['add_picture'];
		$add_size = $_GET['add_size'];
		$add_color = $_GET['add_color'];
		$gatunoc_1 = $_GET['gatunoc_1'];
		$gatunoc_2 = $_GET['gatunoc_2'];
		$gatunoc_3 = $_GET['gatunoc_3'];
		$user_id = $_SESSION["id"];

		$today = date("Y-m-d",strtotime( "0 days"));
		if ( $add_date == '') {
			$add_date = $today;
		}			
		
		$sql = ("INSERT INTO $bag_table (`article_id`, `picture_id`, `size_id`, `color_id`, `coun1`, `coun2`, `coun3`) VALUES ('$add_article', '$add_picture', '$add_size', '$add_color', '$gatunoc_1', '$gatunoc_2', '$gatunoc_3')");

		

		if ($link->query($sql) === TRUE) {
		    $last_id = $link->insert_id;
		    echo "Додано мішок з кодoм: " . $last_id;
		} else {
		    echo "Помилка: " . $sql . "<br>" . $link->error;
		}
		
		$insert_store = $link->query("INSERT INTO $store2_coming_table (`bag_id`, `date`, `area`, `accepted`,  `curent_date`, `user_id`) VALUES ('$last_id', '$add_date', 'Внутрішня ділянка', '$accepted', '$today', '$user_id')");
		

		$stor2_remainder = $link->query("INSERT INTO $stor2_remainder_table (`bag_id`, `coming_date`) VALUES ('$last_id', '$add_date')");

		Include "../../inc/stor/coming_to_stor2-inc.php";
	}
	
}