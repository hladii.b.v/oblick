<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";


$bag_table = 'bag_table';
$stor1_remainder_table = 'stor1_remainder';

$table_article = 'dov_article';
$table_picture = 'dov_picture';
$table_size = 'dov_size';
$table_color = 'dov_color';
$table_class = 'dov_class';
$table_type = 'dov_type';
$table_assortment = 'dov_assortment';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show form
	if ( $do_action =='get_remainder_stor1') {
		$today = date("Y-m-d",strtotime( "0 days"));

		$datepicker_first = $_GET['datepicker_first'];

		if ($datepicker_first == ''  ) {
			$datepicker_first = $today;
			
		}

		Include "../../inc/stor/remainder_stor1-inc.php";
	}
	
	
}