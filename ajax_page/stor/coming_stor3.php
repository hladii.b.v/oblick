<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";


$bag_table = 'bag_table';
$store2_coming_table = 'store2_coming';
$store2_consumption_table = 'store2_consumption';
$stor2_remainder_table = 'stor2_remainder';

$table_employees = 'dov_employees';
$table_employees_meta = 'employees_meta';
$table_users = 'users';
$table_article = 'dov_article';
$table_picture = 'dov_picture';
$table_size = 'dov_size';
$table_color = 'dov_color';
$table_class = 'dov_class';
$table_type = 'dov_type';
$table_assortment = 'dov_assortment';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show form
	if ( $do_action =='get_coming_stor2') {
		$today = date("Y-m-d",strtotime( "0 days"));

		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];

		if ($datepicker_first == ''  ) {
			$datepicker_first = date("Y-m-01", strtotime($today));
			
		}
		if ( $datepicker_last == '' ) {
			$datepicker_last = date("Y-m-t", strtotime($today));
		}

		Include "../../inc/stor/coming_stor2-inc.php";
	}
	//delete_comingg
	elseif ( $do_action =='delete_coming') {

		$bag_id = $_GET['coming_id'];
		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];		
		
		$check_to_delete = $link->query("SELECT consumption_id FROM $store2_consumption_table WHERE `bag_id`='$bag_id'");
		if ($check_to_delete->num_rows > 0) {

			echo "Мішок в ". $bag_id." розході";
		}
		else {
			$result_remainder = $link->query("DELETE FROM $stor2_remainder_table WHERE `bag_id`='$bag_id'");
			$sql_delete="DELETE FROM $store2_coming_table WHERE `bag_id`='$bag_id'";
			$result_deletet = $link->query($sql_delete);
		}

		Include "../../inc/stor/coming_stor2-inc.php";
	}
	elseif ( $do_action =='set_update_coming') {

		$coming_id = $_GET['coming_id'];
		$today = date("Y-m-d",strtotime( "0 days"));
		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];
		$user_id = $_SESSION["id"];
		
		$add_date = $_GET['add_date'];
		$add_accepted = $_GET['add_accepted'];
		
		
		$update_store = $link->query("UPDATE $store2_coming_table SET `date`='$add_date',`accepted`='$add_accepted', `curent_date`='$today',`user_id`='$user_id ' WHERE coming_id='$coming_id'");		

		Include "../../inc/stor/coming_stor2-inc.php";
	}
	elseif ( $do_action =='update_coming') {

		$coming_id = $_GET['coming_id'];
		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];

		$get_coming_modal = $link->query("SELECT * FROM $store2_coming_table WHERE `coming_id`='$coming_id' " );
		while($row_get_coming_modal = $get_coming_modal->fetch_assoc()) {

			$accepted = $row_get_coming_modal['accepted'];
	    	$date = $row_get_coming_modal['date'];
		}
		//**selects

		//employeers
		$result_accepted_small = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='4' AND e.employees_id='$accepted'");
		$result_accepted = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='4' AND e.employees_id!='$accepted' ORDER BY e.employees_id");
		
		//**return	
		
		//employeers
		$all_accepted = '';
		
		while($row_accepted = $result_accepted_small->fetch_assoc()) {
			$full_name = '';
			$first_name = substr($row_accepted['first_name'], 0, 2);
			$surname =  substr($row_accepted['surname'], 0, 2);

			$full_name =  $row_accepted['last_name'].' '.$first_name.'.'. $surname.'.';
			$all_accepted .= '<option value="'. $row_accepted['employees_id'].'">'. $full_name .'</option>';
		}
		
		while($row_accepted = $result_accepted->fetch_assoc()) {
			$full_name = '';
			$first_name = substr($row_accepted['first_name'], 0, 2);
			$surname =  substr($row_accepted['surname'], 0, 2);

			$full_name =  $row_accepted['last_name'].' '.$first_name.'.'. $surname.'.';
			$all_accepted .= '<option value="'. $row_accepted['employees_id'].'">'. $full_name .'</option>';
		}


		Include "../../inc/stor/coming_stor2-inc.php";
	}

	elseif ( $do_action =='update_coming_big') {

		$coming_id = $_GET['coming_id'];
		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];

		$get_coming_modal = $link->query("SELECT * FROM $store2_coming_table WHERE `coming_id`='$coming_id' " );
		while($row_get_coming_modal = $get_coming_modal->fetch_assoc()) {
			$bag_id = $row_get_coming_modal['bag_id'];
			$accepted = $row_get_coming_modal['accepted'];
	    	$date = $row_get_coming_modal['date'];
		}
		//**selects

		//employeers
		$result_accepted_small = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='4' AND e.employees_id='$accepted'");
		$result_accepted = $link->query("SELECT e.employees_id, e.first_name, e.last_name, e.surname FROM $table_employees_meta m LEFT JOIN  $table_employees e ON m.employees_id=e.employees_id WHERE meta_key='employees_operation' AND meta_value='4' AND e.employees_id!='$accepted' ORDER BY e.employees_id");
		//parameters

		$get_bag_modal = $link->query("SELECT * FROM $bag_table WHERE `bag_id`='$bag_id' " );
		while($row_get_bag_modal = $get_bag_modal->fetch_assoc()) {

			$article_id = $row_get_bag_modal['article_id'];
	    	$picture_id = $row_get_bag_modal['picture_id'];
	    	$size_id = $row_get_bag_modal['size_id'];
	    	$color_id = $row_get_bag_modal['color_id'];
	    	$coun1 = $row_get_bag_modal['coun1'];
	    	$coun2 = $row_get_bag_modal['coun2'];
	    	$coun3 = $row_get_bag_modal['coun3'];
		}
		$result_article_small = $link->query("SELECT * FROM $table_article WHERE article_id='$article_id'");
		$result_article = $link->query("SELECT * FROM $table_article WHERE article_id!='$article_id'");
		$result_picture_small = $link->query("SELECT * FROM $table_picture WHERE picture_id='$picture_id'");
		$result_picture = $link->query("SELECT * FROM $table_picture WHERE picture_id!='$picture_id'");
		$result_size_small = $link->query("SELECT * FROM $table_size WHERE size_id='$size_id'");
		$result_size = $link->query("SELECT * FROM $table_size WHERE size_id!='$size_id'");
		$result_color_small = $link->query("SELECT * FROM $table_color WHERE color_id='$color_id'");
		$result_color = $link->query("SELECT * FROM $table_color WHERE color_id!='$color_id'");
		
		//**return	
		
		//employeers
		$all_accepted = '';
		
		while($row_accepted = $result_accepted_small->fetch_assoc()) {
			$full_name = '';
			$first_name = substr($row_accepted['first_name'], 0, 2);
			$surname =  substr($row_accepted['surname'], 0, 2);

			$full_name =  $row_accepted['last_name'].' '.$first_name.'.'. $surname.'.';
			$all_accepted .= '<option value="'. $row_accepted['employees_id'].'">'. $full_name .'</option>';
		}
		
		while($row_accepted = $result_accepted->fetch_assoc()) {
			$full_name = '';
			$first_name = substr($row_accepted['first_name'], 0, 2);
			$surname =  substr($row_accepted['surname'], 0, 2);

			$full_name =  $row_accepted['last_name'].' '.$first_name.'.'. $surname.'.';
			$all_accepted .= '<option value="'. $row_accepted['employees_id'].'">'. $full_name .'</option>';
		}

		//parameters
		$all_article = '';
		while($row_article = $result_article_small->fetch_assoc()) {
			
			$all_article .= '<option value="'. $row_article['article_id'].'">'. $row_article['article_name'] .'</option>';
		}
		
		while($row_article = $result_article->fetch_assoc()) {
			
			$all_article .= '<option value="'. $row_article['article_id'].'">'. $row_article['article_name'] .'</option>';
		}
		

		$all_picture = '';
		
		while($row_picture = $result_picture_small->fetch_assoc()) {
			
			$all_picture .= '<option value="'. $row_picture['picture_id'].'">'. $row_picture['picture_name'] .'</option>';
		}
		while($row_picture = $result_picture->fetch_assoc()) {
			
			$all_picture .= '<option value="'. $row_picture['picture_id'].'">'. $row_picture['picture_name'] .'</option>';
		}
	

		$all_size = '';
		while($row_size = $result_size_small->fetch_assoc()) {
			
			$all_size .= '<option value="'. $row_size['size_id'].'">'. $row_size['size_name'] .'</option>';
		}
	
		while($row_size = $result_size->fetch_assoc()) {
			
			$all_size .= '<option value="'. $row_size['size_id'].'">'. $row_size['size_name'] .'</option>';
		}
	
		$all_color = '';
		while($row_color = $result_color_small->fetch_assoc()) {
			
			$all_color .= '<option value="'. $row_color['color_id'].'">'. $row_color['color_name'] .'</option>';
		}
		while($row_color = $result_color->fetch_assoc()) {
			
			$all_color .= '<option value="'. $row_color['color_id'].'">'. $row_color['color_name'] .'</option>';
		}

		Include "../../inc/stor/coming_stor2-inc.php";
	}

	elseif ( $do_action =='set_update_coming_big') {

		$coming_id = $_GET['coming_id'];
		$bag_id = $_GET['bag_id'];
		$today = date("Y-m-d",strtotime( "0 days"));
		$datepicker_first = $_GET['datepicker_first'];
		$datepicker_last = $_GET['datepicker_last'];
		$user_id = $_SESSION["id"];
		
		$add_date = $_GET['add_date'];
		$add_accepted = $_GET['add_accepted'];
		$add_article = $_GET['add_article'];
		$add_picture = $_GET['add_picture'];
		$add_size = $_GET['add_size'];
		$add_color = $_GET['add_color'];
		$gatunoc_1 = $_GET['gatunoc_1'];
		$gatunoc_2 = $_GET['gatunoc_2'];
		$gatunoc_3 = $_GET['gatunoc_3'];
		
		
		$update_store = $link->query("UPDATE $store2_coming_table SET `date`='$add_date',`accepted`='$add_accepted', `curent_date`='$today',`user_id`='$user_id ' WHERE coming_id='$coming_id'");
		$sql =  $link->query("UPDATE $bag_table SET `article_id`='$add_article', `picture_id`='$add_picture', `size_id`='$add_size', `color_id`='$add_color', `coun1`='$gatunoc_1', `coun2`='$gatunoc_2', `coun3`='$gatunoc_3' WHERE bag_id='$bag_id'");
		

		Include "../../inc/stor/coming_stor2-inc.php";
	}
	
}