<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_size = 'dov_size';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show size
	if ( $do_action =='get_size') {
		Include "../../inc/dov/size-inc.php";
	}
	//add_size
	elseif ( $do_action =='add_size') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_size (`size_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/size-inc.php";
	}
	//delete_size
	elseif ( $do_action =='delete_size') {

		$size_id = $_GET['size_id'];
		
		
		$sql_delete="DELETE FROM $table_size WHERE `size_id`='$size_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/size-inc.php";
	}
	elseif ( $do_action =='edit_size') {

		$size_id = $_GET['size_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_size SET  `size_name`='$name' WHERE `size_id` = '$size_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/size-inc.php";
	}
}