<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_packingMaterials = 'dov_packing_materials';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show packingMaterials
	if ( $do_action =='get_packingMaterials') {
		Include "../../inc/dov/packingMaterials-inc.php";
	}
	//add_packingMaterials
	elseif ( $do_action =='add_packingMaterials') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_packingMaterials (`packing_materials_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/packingMaterials-inc.php";
	}
	//delete_packingMaterials
	elseif ( $do_action =='delete_packingMaterials') {

		$packingMaterials_id = $_GET['packing_materials_id'];
		
		
		$sql_delete="DELETE FROM $table_packingMaterials WHERE `packing_materials_id`='$packingMaterials_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/packingMaterials-inc.php";
	}
	elseif ( $do_action =='edit_packingMaterials') {

		$packingMaterials_id = $_GET['packing_materials_id'];
		$name = $_GET['name'];
		
		$sql_update="UPDATE $table_packingMaterials SET  `packing_materials_name`='$name' WHERE `packing_materials_id` = '$packingMaterials_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/packingMaterials-inc.php";
	}
}