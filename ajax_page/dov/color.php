<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_color = 'dov_color';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show color
	if ( $do_action =='get_color') {
		Include "../../inc/dov/color-inc.php";
	}
	//add_color
	elseif ( $do_action =='add_color') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_color (`color_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/color-inc.php";
	}
	//delete_color
	elseif ( $do_action =='delete_color') {

		$color_id = $_GET['color_id'];
		
		
		$sql_delete="DELETE FROM $table_color WHERE `color_id`='$color_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/color-inc.php";
	}
	elseif ( $do_action =='edit_color') {

		$color_id = $_GET['color_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_color SET  `color_name`='$name' WHERE `color_id` = '$color_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/color-inc.php";
	}
}