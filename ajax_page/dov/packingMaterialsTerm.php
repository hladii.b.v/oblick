<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_packingMaterialsTerm = 'dov_packing_materials_term';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show packingMaterialsTerm
	if ( $do_action =='get_packingMaterialsTerm') {
		Include "../../inc/dov/packingMaterialsTerm-inc.php";
	}
	//add_packingMaterialsTerm
	elseif ( $do_action =='add_packingMaterialsTerm') {

		$name = $_GET['name'];
		$packing_materials_id = $_GET['packing_materials_id'];
		
		$sql_add="INSERT INTO $table_packingMaterialsTerm (`term_name`,`packing_materials_id`) VALUES ('$name','$packing_materials_id')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/packingMaterialsTerm-inc.php";
	}
	//delete_packingMaterialsTerm
	elseif ( $do_action =='delete_packingMaterialsTerm') {

		$term_id = $_GET['term_id'];
		
		
		$sql_delete="DELETE FROM $table_packingMaterialsTerm WHERE `term_id`='$term_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/packingMaterialsTerm-inc.php";
	}
	elseif ( $do_action =='edit_packingMaterialsTerm') {

		$term_id = $_GET['term_id'];
		$name = $_GET['name'];
		
		$sql_update="UPDATE $table_packingMaterialsTerm SET  `term_name`='$name' WHERE `term_id` = '$term_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/packingMaterialsTerm-inc.php";
	}
}