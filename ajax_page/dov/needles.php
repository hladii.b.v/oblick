<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_needles = 'dov_needles';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show needles
	if ( $do_action =='get_needles') {
		Include "../../inc/dov/needles-inc.php";
	}
	//add_needles
	elseif ( $do_action =='add_needles') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_needles (`needles_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/needles-inc.php";
	}
	//delete_needles
	elseif ( $do_action =='delete_needles') {

		$needles_id = $_GET['needles_id'];
		
		
		$sql_delete="DELETE FROM $table_needles WHERE `needles_id`='$needles_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/needles-inc.php";
	}
	elseif ( $do_action =='edit_needles') {

		$needles_id = $_GET['needles_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_needles SET  `needles_name`='$name' WHERE `needles_id` = '$needles_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/needles-inc.php";
	}
}