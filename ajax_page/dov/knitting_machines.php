<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_knitting_machines = 'dov_knitting_machines';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show knitting_machines
	if ( $do_action =='get_knitting_machines') {
		Include "../../inc/dov/knitting_machines-inc.php";
	}
	//add_knitting_machines
	elseif ( $do_action =='add_knitting_machines') {

		$add_number = $_GET['add_number'];
		$add_models = $_GET['add_models'];
		$add_needles = $_GET['add_needles'];
		$add_inches = $_GET['add_inches'];	
		$result_check = $link->query( "SELECT * FROM $table_knitting_machines WHERE `machines_number`='$add_number'" );	
		if ($result_check->num_rows > 0) {
			echo 'Машина з номером '.$add_number.' вже існує';
		}
		else {
			$sql_add="INSERT INTO $table_knitting_machines (`machines_number`, `model_id`, `needles_id`, `inches_id`, `machines_status`) VALUES ('$add_number', '$add_models', '$add_needles', '$add_inches', 1)";
			$result_insert = $link->query($sql_add);
		}
		

		Include "../../inc/dov/knitting_machines-inc.php";
	}
	//delete_knitting_machines
	elseif ( $do_action =='delete_knitting_machines') {

		$machines_id = $_GET['machines_id'];
		
		
		$sql_delete="DELETE FROM $table_knitting_machines WHERE `machines_id`='$machines_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/knitting_machines-inc.php";
	}
	elseif ( $do_action =='edit_knitting_machines') {

		$machines_id = $_GET['machines_id'];

		$update_number = $_GET['update_number'];	
		$update_models = $_GET['update_models'];	
		$update_needles = $_GET['update_needles'];	
		$update_inches = $_GET['update_inches'];	
		$update_status = $_GET['update_status'];		

		$result_check = $link->query( "SELECT * FROM $table_knitting_machines WHERE `machines_number`='$update_number'" );	
		if ($result_check->num_rows > 0) {
			echo 'Машина з номером '.$add_number.' вже існує';
		}
		else {
			$update_sql="UPDATE $table_knitting_machines SET  `machines_number`='$update_number', `model_id`='$update_models', `needles_id`='$update_needles', `inches_id`='$update_inches', `machines_status`='$update_status' WHERE `machines_id` = '$machines_id'";
			$result_update = $link->query($update_sql);
		}
		
		

		Include "../../inc/dov/knitting_machines-inc.php";
	}
}