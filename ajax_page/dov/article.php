<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_article = 'dov_article';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show article
	if ( $do_action =='get_article') {
		Include "../../inc/dov/article-inc.php";
	}
	//add_article
	elseif ( $do_action =='add_article') {

		$name = $_GET['name'];
		$type_id = $_GET['type_id'];
		$color_id = $_GET['color_id'];
		$assortment_id = $_GET['assortment_id'];
		$class_id = $_GET['class_id'];
		$season_id = $_GET['season_id'];
		$article_noted = $_GET['article_noted'];
		$sql_add="INSERT INTO $table_article (`article_name`,`type_id`,`color_id`,`assortment_id`,`class_id`,`season_id`,`article_noted`) VALUES ('$name','$type_id','$color_id','$assortment_id','$class_id','$season_id','$article_noted')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/article-inc.php";
	}
	//delete_article
	elseif ( $do_action =='delete_article') {

		$article_id = $_GET['article_id'];
		
		
		$sql_delete="DELETE FROM $table_article WHERE `article_id`='$article_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/article-inc.php";
	}
	elseif ( $do_action =='edit_article') {

		$article_id = $_GET['article_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_article SET  `article_name`='$name' WHERE `article_id` = '$article_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/article-inc.php";
	}
}