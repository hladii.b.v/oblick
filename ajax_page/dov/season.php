<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_season = 'dov_season';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show season
	if ( $do_action =='get_season') {
		Include "../../inc/dov/season-inc.php";
	}
	//add_season
	elseif ( $do_action =='add_season') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_season (`season_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/season-inc.php";
	}
	//delete_season
	elseif ( $do_action =='delete_season') {

		$season_id = $_GET['season_id'];
		
		
		$sql_delete="DELETE FROM $table_season WHERE `season_id`='$season_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/season-inc.php";
	}
	elseif ( $do_action =='edit_season') {

		$season_id = $_GET['season_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_season SET  `season_name`='$name' WHERE `season_id` = '$season_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/season-inc.php";
	}
}