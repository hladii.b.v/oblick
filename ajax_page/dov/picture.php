<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_picture = 'dov_picture';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show picture
	if ( $do_action =='get_picture') {
		Include "../../inc/dov/picture-inc.php";
	}
	//add_picture
	elseif ( $do_action =='add_picture') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_picture (`picture_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/picture-inc.php";
	}
	//delete_picture
	elseif ( $do_action =='delete_picture') {

		$picture_id = $_GET['picture_id'];
		
		
		$sql_delete="DELETE FROM $table_picture WHERE `picture_id`='$picture_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/picture-inc.php";
	}
	elseif ( $do_action =='edit_picture') {

		$picture_id = $_GET['picture_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_picture SET  `picture_name`='$name' WHERE `picture_id` = '$picture_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/picture-inc.php";
	}
}