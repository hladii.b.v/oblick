<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_packingMaterialsProvider = 'dov_packing_materials_provider';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show packingMaterialsProvider
	if ( $do_action =='get_packingMaterialsProvider') {
		Include "../../inc/dov/packingMaterialsProvider-inc.php";
	}
	//add_packingMaterialsProvider
	elseif ( $do_action =='add_packingMaterialsProvider') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_packingMaterialsProvider (`provider_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/packingMaterialsProvider-inc.php";
	}
	//delete_packingMaterialsProvider
	elseif ( $do_action =='delete_packingMaterialsProvider') {

		$packingMaterialsProvider_id = $_GET['provider_id'];
		
		
		$sql_delete="DELETE FROM $table_packingMaterialsProvider WHERE `provider_id`='$packingMaterialsProvider_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/packingMaterialsProvider-inc.php";
	}
	elseif ( $do_action =='edit_packingMaterialsProvider') {

		$packingMaterialsProvider_id = $_GET['provider_id'];
		$name = $_GET['name'];
		
		$sql_update="UPDATE $table_packingMaterialsProvider SET  `provider_name`='$name' WHERE `provider_id` = '$packingMaterialsProvider_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/packingMaterialsProvider-inc.php";
	}
}