<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_models = 'dov_models';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show models
	if ( $do_action =='get_models') {
		Include "../../inc/dov/models-inc.php";
	}
	//add_models
	elseif ( $do_action =='add_models') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_models (`model_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/models-inc.php";
	}
	//delete_models
	elseif ( $do_action =='delete_models') {

		$models_id = $_GET['models_id'];
		
		
		$sql_delete="DELETE FROM $table_models WHERE `model_id`='$models_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/models-inc.php";
	}
	elseif ( $do_action =='edit_models') {

		$models_id = $_GET['models_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_models SET  `model_name`='$name' WHERE `model_id` = '$models_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/models-inc.php";
	}
}