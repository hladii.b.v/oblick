<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_employees = 'dov_employees';
$table_employees_meta = 'employees_meta';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show employees
	if ( $do_action =='get_employees') {
		$get_employees_id =  $_GET['id'];
		
		Include "../../inc/dov/employees-inc.php";
	}
	//add_employees
	elseif ( $do_action =='add_employees') {

		$first_name = $_GET['first_name'];
		$last_name = $_GET['last_name'];
		$surname = $_GET['surname'];
		
		$sql_add="INSERT INTO $table_employees (`first_name`, `last_name`, `surname`, `status`) VALUES ('$first_name', '$last_name', '$surname', 1)";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/employees-inc.php";
	}
	//delete_employees
	elseif ( $do_action =='delete_employees') {

		$employees_id = $_GET['employees_id'];
		
		$sql_delete_meta="DELETE FROM $table_employees_meta WHERE `employees_id`='$employees_id'";
		$result_deletet_meta = $link->query($sql_delete_meta);

		$sql_delete="DELETE FROM $table_employees WHERE `employees_id`='$employees_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/employees-inc.php";
	}
	elseif ( $do_action =='edit_employeesr') {

		$employees_id = $_GET['employees_id'];
		$first_name = $_GET['first_name'];
		$last_name = $_GET['last_name'];
		$surname = $_GET['surname'];
		$status = $_GET['status'];
		
		$sql_update="UPDATE $table_employees SET  `first_name`='$first_name', `last_name`='$last_name', `surname`='$surname', `status`='$status' WHERE `employees_id` = '$employees_id'";
		$result_update = $link->query($sql_update);

		Include "../../inc/dov/employees-inc.php";
	}
	elseif ( $do_action =='add_employees_operation') {

		$id_values = $_GET['values'];
		$id_emps = $_GET['id'];

		$id_array = explode(",", $id_values);
		
		$sql_delete="DELETE FROM $table_employees_meta WHERE `employees_id`='$id_emps'";
		$result_deletet = $link->query($sql_delete);
		if ($id_values != '') {
			foreach ($id_array as $value) {

				$sql_add="INSERT INTO $table_employees_meta (`employees_id`, `meta_key`, `meta_value`) VALUES ('$id_emps', 'employees_operation', '$value')";
				$result_insert = $link->query($sql_add);
			}
		}
		

		Include "../../inc/dov/employees-inc.php";
	}
}