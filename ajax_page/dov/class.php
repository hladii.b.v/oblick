<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_class = 'dov_class';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show class
	if ( $do_action =='get_class') {
		Include "../../inc/dov/class-inc.php";
	}
	//add_class
	elseif ( $do_action =='add_class') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_class (`class_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/class-inc.php";
	}
	//delete_class
	elseif ( $do_action =='delete_class') {

		$class_id = $_GET['class_id'];
		
		
		$sql_delete="DELETE FROM $table_class WHERE `class_id`='$class_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/class-inc.php";
	}
	elseif ( $do_action =='edit_class') {

		$class_id = $_GET['class_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_class SET  `class_name`='$name' WHERE `class_id` = '$class_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/class-inc.php";
	}
}