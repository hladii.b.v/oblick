<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_inches = 'dov_inches';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show inches
	if ( $do_action =='get_inches') {
		Include "../../inc/dov/inches-inc.php";
	}
	//add_inches
	elseif ( $do_action =='add_inches') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_inches (`inches_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/inches-inc.php";
	}
	//delete_inches
	elseif ( $do_action =='delete_inches') {

		$inches_id = $_GET['inches_id'];
		
		
		$sql_delete="DELETE FROM $table_inches WHERE `inches_id`='$inches_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/inches-inc.php";
	}
	elseif ( $do_action =='edit_inches') {

		$inches_id = $_GET['inches_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_inches SET  `inches_name`='$name' WHERE `inches_id` = '$inches_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/inches-inc.php";
	}
}