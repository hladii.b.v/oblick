<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_packingMaterialsTermMeta = 'packing_materials_term_meta';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show packingMaterialsTermMeta
	if ( $do_action =='get_packingMaterialsTermMeta') {
		Include "../../inc/dov/packingMaterialsTermMeta-inc.php";
	}
	//add_packingMaterialsTermMeta
	elseif ( $do_action =='add_packingMaterialsTermMeta') {

		$name = $_GET['name'];
		$term_id = $_GET['packing_materials_term_select'];
		
		
		$sql_add="INSERT INTO $table_packingMaterialsTermMeta (`term_id`,`name`) VALUES ('$term_id','$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/packingMaterialsTermMeta-inc.php";
	}
	//delete_packingMaterialsTermMeta
	elseif ( $do_action =='delete_packingMaterialsTermMeta') {

		$meta_id = $_GET['meta_id'];
		
		
		$sql_delete="DELETE FROM $table_packingMaterialsTermMeta WHERE `meta_id`='$meta_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/packingMaterialsTermMeta-inc.php";
	}
	elseif ( $do_action =='edit_packingMaterialsTermMeta') {

		$meta_id = $_GET['meta_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_packingMaterialsTermMeta SET  `name`='$name' WHERE `meta_id` = '$meta_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/packingMaterialsTermMeta-inc.php";
	}
}