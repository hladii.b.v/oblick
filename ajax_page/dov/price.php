<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_price = 'dov_price';
$table_price_meta = 'price_meta';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show price
	if ( $do_action =='get_price') {
		$price_id = $_GET['price_id'];
		Include "../../inc/dov/price-inc.php";
	}
	//delete_price
	elseif ( $do_action =='delete_price') {

		$price_id_delete = $_GET['price_id'];
		
		
		$sql_delete="DELETE FROM $table_price WHERE `price_id`='$price_id_delete'";
		$result_deletet = $link->query($sql_delete);

		$price_id = 1;

		Include "../../inc/dov/price-inc.php";
	}
	//edit_price
	elseif ( $do_action =='edit_price') {

		$price_id_update = $_GET['price_id'];

		$start_date = $_GET['start_date'];	
		$end_date = $_GET['end_date'];	
		$price_value = $_GET['price_value'];	
		$price_name = $_GET['price_name'];		

		
		$update_sql="UPDATE $table_price SET  `start_date`='$start_date', `end_date`='$end_date', `price_value`='$price_value', `price_name`='$price_name' WHERE `price_id` = '$price_id_update'";
		$result_update = $link->query($update_sql);		
		
		$price_id = $price_id_update;

		Include "../../inc/dov/price-inc.php";
	}
	//add_price
	elseif ( $do_action =='add_price') {

		$start_date = $_GET['start_date'];	
		$end_date = $_GET['end_date'];	
		$price_value = $_GET['price_value'];	
		$price_name = $_GET['price_name'];		

		
		$sql_add="INSERT INTO $table_price (`start_date`, `end_date`, `price_value`, `price_name`) VALUES ('$start_date', '$end_date', '$price_value', '$price_name')";
		$result_insert = $link->query($sql_add);
		
		$price_id = 1;

		Include "../../inc/dov/price-inc.php";
	}
	//delete_price_atribut
	elseif ( $do_action =='delete_price_atribut') {

		$price_id_delete = $_GET['price_id'];
		$key = $_GET['key'];
		$delete_id = $_GET['delete_id'];
		
		$sql_delete="DELETE FROM $table_price_meta WHERE `price_id`='$price_id_delete' AND meta_key='$key' AND meta_value='$delete_id' ";
		$result_deletet = $link->query($sql_delete);

		$price_id = $price_id_delete;

		Include "../../inc/dov/price-inc.php";
	}
	//safe_price_atribut
	elseif ( $do_action =='safe_price_atribut') {

		$id_values = $_GET['values'];
		$price_id = $_GET['price_id'];
		$key = $_GET['key'];

		$id_array = explode(",", $id_values);

		
		
		$sql_delete="DELETE FROM $table_price_meta WHERE `price_id`='$price_id' AND meta_key='$key'";
		$result_deletet = $link->query($sql_delete);
		if ($id_values != '') {
			foreach ($id_array as $value) {

				$sql_add="INSERT INTO $table_price_meta (`price_id`, `meta_key`, `meta_value`) VALUES ('$price_id', '$key', '$value')";
				$result_insert = $link->query($sql_add);
			}
		}
		$price_id = $price_id;

		Include "../../inc/dov/price-inc.php";
	}
}