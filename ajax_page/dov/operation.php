<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_operation = 'dov_operation';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show operation
	if ( $do_action =='get_operation') {
		Include "../../inc/dov/operation-inc.php";
	}
	//add_operation
	elseif ( $do_action =='add_operation') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_operation (`operation_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/operation-inc.php";
	}
	//delete_operation
	elseif ( $do_action =='delete_operation') {

		$operation_id = $_GET['operation_id'];
		
		
		$sql_delete="DELETE FROM $table_operation WHERE `operation_id`='$operation_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/operation-inc.php";
	}
	elseif ( $do_action =='edit_operation') {

		$operation_id = $_GET['operation_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_operation SET  `operation_name`='$name' WHERE `operation_id` = '$operation_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/operation-inc.php";
	}
}