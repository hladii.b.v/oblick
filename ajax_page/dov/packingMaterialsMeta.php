<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_packingMaterialsMeta = 'packing_materials_meta';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show packingMaterialsMeta
	if ( $do_action =='get_packingMaterialsMeta') {
		Include "../../inc/dov/packingMaterialsMeta-inc.php";
	}
	//add_packingMaterialsMeta
	elseif ( $do_action =='add_packingMaterialsMeta') {

		$packing_materials_id = $_GET['packing_materials_id'];
		$meta_key = $_GET['meta_key'];
		$meta_value = $_GET['meta_value'];
		
		$sql_add="INSERT INTO $table_packingMaterialsMeta (`packing_materials_id`,`meta_key`,`meta_value`) VALUES ('$packing_materials_id','$meta_key','$meta_value')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/packingMaterialsMeta-inc.php";
	}
	//delete_packingMaterialsMeta
	elseif ( $do_action =='delete_packingMaterialsMeta') {

		$meta_id = $_GET['meta_id'];
		
		
		$sql_delete="DELETE FROM $table_packingMaterialsMeta WHERE `meta_id`='$meta_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/packingMaterialsMeta-inc.php";
	}
	elseif ( $do_action =='edit_packingMaterialsMeta') {

		// $meta_id = $_GET['meta_id'];
		
		// $sql_update="UPDATE $table_packingMaterialsMeta SET ``='' WHERE `meta_id` = '$meta_id'";
		// $result_deletet = $link->query($sql_update);

		Include "../../inc/dov/packingMaterialsMeta-inc.php";
	}
}