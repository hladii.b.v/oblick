<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_assortment = 'dov_assortment';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show assortment
	if ( $do_action =='get_assortment') {
		Include "../../inc/dov/assortment-inc.php";
	}
	//add_assortment
	elseif ( $do_action =='add_assortment') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_assortment (`assortment_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/assortment-inc.php";
	}
	//delete_assortment
	elseif ( $do_action =='delete_assortment') {

		$assortment_id = $_GET['assortment_id'];
		
		
		$sql_delete="DELETE FROM $table_assortment WHERE `assortment_id`='$assortment_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/assortment-inc.php";
	}
	elseif ( $do_action =='edit_assortment') {

		$assortment_id = $_GET['assortment_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_assortment SET  `assortment_name`='$name' WHERE `assortment_id` = '$assortment_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/assortment-inc.php";
	}
}