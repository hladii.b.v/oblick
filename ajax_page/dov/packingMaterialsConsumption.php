<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_packingMaterialsConsumption = 'dov_packing_materials_consumption';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show packingMaterialsConsumption
	if ( $do_action =='get_packingMaterialsConsumption') {
		Include "../../inc/dov/packingMaterialsConsumption-inc.php";
	}
	//add_packingMaterialsConsumption
	elseif ( $do_action =='add_packingMaterialsConsumption') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_packingMaterialsConsumption (`consumption_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/packingMaterialsConsumption-inc.php";
	}
	//delete_packingMaterialsConsumption
	elseif ( $do_action =='delete_packingMaterialsConsumption') {

		$packingMaterialsConsumption_id = $_GET['consumption_id'];
		
		
		$sql_delete="DELETE FROM $table_packingMaterialsConsumption WHERE `consumption_id`='$packingMaterialsConsumption_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/packingMaterialsConsumption-inc.php";
	}
	elseif ( $do_action =='edit_packingMaterialsConsumption') {

		$packingMaterialsConsumption_id = $_GET['consumption_id'];
		$name = $_GET['name'];
		
		$sql_update="UPDATE $table_packingMaterialsConsumption SET  `consumption_name`='$name' WHERE `consumption_id` = '$packingMaterialsConsumption_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/packingMaterialsConsumption-inc.php";
	}
}