<?php
// Initialize the session
	session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

// Include config file
require_once "../../config.php";
$table_type = 'dov_type';

// ajax requests

if(isset($_POST)){
	
	$do_action = $_GET['show'];

	//show type
	if ( $do_action =='get_type') {
		Include "../../inc/dov/type-inc.php";
	}
	//add_type
	elseif ( $do_action =='add_type') {

		$name = $_GET['name'];
		
		
		$sql_add="INSERT INTO $table_type (`type_name`) VALUES ('$name')";
		$result_insert = $link->query($sql_add);

		Include "../../inc/dov/type-inc.php";
	}
	//delete_type
	elseif ( $do_action =='delete_type') {

		$type_id = $_GET['type_id'];
		
		
		$sql_delete="DELETE FROM $table_type WHERE `type_id`='$type_id'";
		$result_deletet = $link->query($sql_delete);

		Include "../../inc/dov/type-inc.php";
	}
	elseif ( $do_action =='edit_type') {

		$type_id = $_GET['type_id'];
		$name = $_GET['name'];		
		
		$sql_update="UPDATE $table_type SET  `type_name`='$name' WHERE `type_id` = '$type_id'";
		$result_deletet = $link->query($sql_update);

		Include "../../inc/dov/type-inc.php";
	}
}