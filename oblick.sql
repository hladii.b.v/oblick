-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 06 2020 г., 16:47
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `oblick`
--

-- --------------------------------------------------------

--
-- Структура таблицы `bag_table`
--

CREATE TABLE `bag_table` (
  `bag_id` int(10) NOT NULL,
  `article_id` int(10) DEFAULT NULL,
  `picture_id` int(10) DEFAULT NULL,
  `size_id` int(10) DEFAULT NULL,
  `color_id` int(10) DEFAULT NULL,
  `coun1` int(10) DEFAULT NULL,
  `coun2` int(10) DEFAULT NULL,
  `coun3` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `bag_table`
--

INSERT INTO `bag_table` (`bag_id`, `article_id`, `picture_id`, `size_id`, `color_id`, `coun1`, `coun2`, `coun3`) VALUES
(1, 1, 1, 1, 1, 0, 0, 0),
(2, 1, 1, 1, 1, 0, 0, 0),
(3, 1, 1, 1, 1, 0, 0, 0),
(4, 1, 1, 1, 1, 0, 0, 0),
(5, 1, 1, 1, 1, 0, 0, 0),
(6, 1, 1, 1, 1, 0, 0, 0),
(7, 1, 1, 1, 1, 0, 0, 0),
(8, 1, 1, 1, 1, 0, 0, 0),
(9, 1, 1, 1, 1, 50, 5, 0),
(10, 2, 1, 2, 1, 55, 0, 0),
(11, 1, 1, 1, 1, 56, 5, 0),
(12, 1, 1, 1, 1, 11, 0, 0),
(13, 1, 1, 1, 1, 0, 0, 0),
(14, 1, 1, 1, 1, 0, 0, 0),
(15, 1, 1, 1, 1, 11, 2, 1),
(16, 1, 1, 1, 1, 0, 0, 0),
(17, 1, 1, 1, 1, 0, 0, 0),
(18, 1, 1, 1, 2, 123, 2, 2),
(19, 1, 1, 1, 1, 0, 0, 0),
(20, 1, 1, 1, 1, 0, 0, 0),
(21, 1, 1, 1, 1, 0, 0, 0),
(22, 1, 1, 1, 1, 0, 0, 0),
(23, 2, 1, 1, 1, 0, 0, 0),
(24, 1, 1, 1, 1, 50, 0, 0),
(25, 1, 1, 1, 1, 22, 0, 0),
(26, 1, 1, 1, 1, 44, 0, 0),
(27, 1, 1, 1, 1, 33, 0, 0),
(28, 1, 1, 1, 1, 44, 0, 0),
(29, 1, 1, 1, 1, 56, 0, 0),
(30, 1, 1, 1, 1, 66, 0, 0),
(31, 1, 1, 1, 1, 44, 0, 0),
(32, 1, 1, 1, 1, 55, 0, 0),
(33, 1, 1, 1, 1, 55, 2, 2),
(34, 1, 1, 1, 1, 0, 0, 0),
(35, 1, 1, 1, 1, 0, 0, 0),
(36, 1, 1, 1, 1, 0, 0, 0),
(37, 1, 1, 1, 1, 0, 0, 0),
(38, 1, 1, 1, 1, 0, 0, 0),
(39, 1, 1, 1, 1, 0, 0, 0),
(40, 1, 1, 1, 1, 0, 0, 0),
(41, 1, 1, 1, 1, 0, 0, 0),
(42, 1, 1, 1, 1, 0, 0, 0),
(43, 1, 1, 1, 1, 0, 0, 0),
(44, 1, 1, 1, 1, 0, 0, 0),
(45, 1, 1, 1, 1, 0, 0, 0),
(46, 1, 1, 1, 1, 0, 0, 0),
(47, 1, 1, 1, 1, 0, 0, 0),
(48, 1, 1, 1, 1, 0, 0, 0),
(49, 1, 1, 1, 1, 0, 0, 0),
(50, 1, 1, 1, 1, 0, 0, 0),
(51, 1, 1, 1, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `dov_article`
--

CREATE TABLE `dov_article` (
  `article_id` int(10) NOT NULL,
  `article_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `type_id` int(10) DEFAULT NULL,
  `assortment_id` int(10) DEFAULT NULL,
  `class_id` int(10) DEFAULT NULL,
  `season_id` int(10) DEFAULT NULL,
  `article_noted` longtext COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_article`
--

INSERT INTO `dov_article` (`article_id`, `article_name`, `type_id`, `assortment_id`, `class_id`, `season_id`, `article_noted`) VALUES
(1, '710 ', 1, 1, 1, 1, NULL),
(2, '810', 1, 1, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `dov_assortment`
--

CREATE TABLE `dov_assortment` (
  `assortment_id` int(10) NOT NULL,
  `assortment_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_assortment`
--

INSERT INTO `dov_assortment` (`assortment_id`, `assortment_name`) VALUES
(1, 'жін');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_catalogs_colors`
--

CREATE TABLE `dov_catalogs_colors` (
  `catalog_id` int(10) NOT NULL,
  `cmain_color_id` int(10) DEFAULT NULL,
  `catalog_color_id` int(10) DEFAULT NULL,
  `color_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `color_code` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `dov_catalog_color`
--

CREATE TABLE `dov_catalog_color` (
  `color_id` int(10) NOT NULL,
  `color_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `dov_class`
--

CREATE TABLE `dov_class` (
  `class_id` int(10) NOT NULL,
  `class_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_class`
--

INSERT INTO `dov_class` (`class_id`, `class_name`) VALUES
(1, 'бізнес');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_color`
--

CREATE TABLE `dov_color` (
  `color_id` int(10) NOT NULL,
  `color_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_color`
--

INSERT INTO `dov_color` (`color_id`, `color_name`) VALUES
(1, 'Білий'),
(2, 'Чорний');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_employees`
--

CREATE TABLE `dov_employees` (
  `employees_id` int(10) NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `surname` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_employees`
--

INSERT INTO `dov_employees` (`employees_id`, `first_name`, `last_name`, `surname`, `status`) VALUES
(1, 'Арвпарвп113', 'Врврваб', 'Арварвап111', 1),
(2, 'Фівафіва', 'Аіапп', 'Промитми', 1),
(3, 'Аавапрпва', 'Іававіфа', 'Фвафіа', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `dov_inches`
--

CREATE TABLE `dov_inches` (
  `inches_id` int(10) NOT NULL,
  `inches_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_inches`
--

INSERT INTO `dov_inches` (`inches_id`, `inches_name`) VALUES
(1, '3 1/2');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_knitting_machines`
--

CREATE TABLE `dov_knitting_machines` (
  `machines_id` int(10) NOT NULL,
  `machines_number` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `model_id` int(10) DEFAULT NULL,
  `needles_id` int(10) DEFAULT NULL,
  `inches_id` int(10) DEFAULT NULL,
  `machines_status` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_knitting_machines`
--

INSERT INTO `dov_knitting_machines` (`machines_id`, `machines_number`, `model_id`, `needles_id`, `inches_id`, `machines_status`) VALUES
(1, '1', 4, 1, 1, 0),
(2, '3', 1, 1, 1, 1),
(4, '4', 4, 1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `dov_materials_buyers`
--

CREATE TABLE `dov_materials_buyers` (
  `buyers_id` int(10) NOT NULL,
  `buyers_city` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `buyers_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `buyers_representative` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `dov_materials_manufacturers`
--

CREATE TABLE `dov_materials_manufacturers` (
  `manufacturers_id` int(10) NOT NULL,
  `manufacturers_country` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `manufacturers_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `dov_materials_name`
--

CREATE TABLE `dov_materials_name` (
  `name_id` int(10) NOT NULL,
  `materials_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `dov_materials_size`
--

CREATE TABLE `dov_materials_size` (
  `size_id` int(10) NOT NULL,
  `materials_size` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `dov_materials_suppliers`
--

CREATE TABLE `dov_materials_suppliers` (
  `suppliers_id` int(10) NOT NULL,
  `suppliers_country` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `suppliers_city` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `suppliers_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `dov_materials_type`
--

CREATE TABLE `dov_materials_type` (
  `type_id` int(10) NOT NULL,
  `type_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `dov_materials_unite`
--

CREATE TABLE `dov_materials_unite` (
  `unite_id` int(10) NOT NULL,
  `materials_unite` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `dov_models`
--

CREATE TABLE `dov_models` (
  `model_id` int(10) NOT NULL,
  `model_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_models`
--

INSERT INTO `dov_models` (`model_id`, `model_name`) VALUES
(1, 'Анга15'),
(4, 'asdfdasf');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_needles`
--

CREATE TABLE `dov_needles` (
  `needles_id` int(10) NOT NULL,
  `needles_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_needles`
--

INSERT INTO `dov_needles` (`needles_id`, `needles_name`) VALUES
(1, '152');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_operation`
--

CREATE TABLE `dov_operation` (
  `operation_id` int(10) NOT NULL,
  `operation_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_operation`
--

INSERT INTO `dov_operation` (`operation_id`, `operation_name`) VALUES
(2, 'Майстер'),
(3, 'Вязання'),
(4, 'Шиття'),
(5, 'Сортування'),
(6, 'wrtwertwert'),
(7, 'adasdas'),
(8, 'sfgsdgfs2342'),
(10, 'dsfsd');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_packing_materials`
--

CREATE TABLE `dov_packing_materials` (
  `packing_materials_id` int(10) NOT NULL,
  `packing_materials_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_packing_materials`
--

INSERT INTO `dov_packing_materials` (`packing_materials_id`, `packing_materials_name`) VALUES
(1, 'Етикетка');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_packing_materials_consumption`
--

CREATE TABLE `dov_packing_materials_consumption` (
  `consumption_id` int(10) NOT NULL,
  `consumption_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_packing_materials_consumption`
--

INSERT INTO `dov_packing_materials_consumption` (`consumption_id`, `consumption_name`) VALUES
(1, 'Склад 1');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_packing_materials_provider`
--

CREATE TABLE `dov_packing_materials_provider` (
  `provider_id` int(10) NOT NULL,
  `provider_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_packing_materials_provider`
--

INSERT INTO `dov_packing_materials_provider` (`provider_id`, `provider_name`) VALUES
(1, 'Тест');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_packing_materials_term`
--

CREATE TABLE `dov_packing_materials_term` (
  `term_id` int(10) NOT NULL,
  `term_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `packing_materials_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_packing_materials_term`
--

INSERT INTO `dov_packing_materials_term` (`term_id`, `term_name`, `packing_materials_id`) VALUES
(1, 'Розмір', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `dov_picture`
--

CREATE TABLE `dov_picture` (
  `picture_id` int(10) NOT NULL,
  `picture_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_picture`
--

INSERT INTO `dov_picture` (`picture_id`, `picture_name`) VALUES
(1, '1'),
(2, '10');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_price`
--

CREATE TABLE `dov_price` (
  `price_id` int(10) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `price_value` double DEFAULT NULL,
  `price_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_price`
--

INSERT INTO `dov_price` (`price_id`, `start_date`, `end_date`, `price_value`, `price_name`) VALUES
(1, '2020-02-19', '2020-04-16', 1, 'ісаів11'),
(3, '2020-02-21', '2020-02-28', 1.2, 'sdfasfdas2'),
(4, '2020-02-21', '2020-02-29', 1, 'fsgdg');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_season`
--

CREATE TABLE `dov_season` (
  `season_id` int(10) NOT NULL,
  `season_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_season`
--

INSERT INTO `dov_season` (`season_id`, `season_name`) VALUES
(1, 'Літо');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_size`
--

CREATE TABLE `dov_size` (
  `size_id` int(10) NOT NULL,
  `size_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_size`
--

INSERT INTO `dov_size` (`size_id`, `size_name`) VALUES
(1, '23-25'),
(2, '21-23');

-- --------------------------------------------------------

--
-- Структура таблицы `dov_type`
--

CREATE TABLE `dov_type` (
  `type_id` int(10) NOT NULL,
  `type_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `dov_type`
--

INSERT INTO `dov_type` (`type_id`, `type_name`) VALUES
(1, 'шкарп');

-- --------------------------------------------------------

--
-- Структура таблицы `employees_meta`
--

CREATE TABLE `employees_meta` (
  `meta_id` int(11) NOT NULL,
  `employees_id` int(11) NOT NULL,
  `meta_key` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `meta_value` varchar(50) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `employees_meta`
--

INSERT INTO `employees_meta` (`meta_id`, `employees_id`, `meta_key`, `meta_value`) VALUES
(54, 1, 'employees_operation', '2'),
(55, 1, 'employees_operation', '5'),
(56, 2, 'employees_operation', '3'),
(57, 2, 'employees_operation', '4'),
(58, 3, 'employees_operation', '2'),
(59, 3, 'employees_operation', '3'),
(60, 3, 'employees_operation', '4');

-- --------------------------------------------------------

--
-- Структура таблицы `packing_materials_meta`
--

CREATE TABLE `packing_materials_meta` (
  `meta_id` int(10) NOT NULL,
  `packing_materials_id` int(10) DEFAULT NULL,
  `meta_key` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `meta_value` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `packing_materials_term_meta`
--

CREATE TABLE `packing_materials_term_meta` (
  `meta_id` int(10) NOT NULL,
  `term_id` int(10) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `packing_materials_term_meta`
--

INSERT INTO `packing_materials_term_meta` (`meta_id`, `term_id`, `name`) VALUES
(1, 1, '21');

-- --------------------------------------------------------

--
-- Структура таблицы `price_meta`
--

CREATE TABLE `price_meta` (
  `meta_id` int(10) NOT NULL,
  `price_id` int(10) NOT NULL,
  `meta_key` varchar(20) COLLATE utf8mb4_bin NOT NULL,
  `meta_value` varchar(20) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `price_meta`
--

INSERT INTO `price_meta` (`meta_id`, `price_id`, `meta_key`, `meta_value`) VALUES
(77, 1, 'article_id', '1'),
(78, 1, 'operation_id', '3'),
(79, 1, 'operation_id', '4'),
(85, 1, 'type_id', '1'),
(86, 1, 'assortment_id', '1'),
(87, 1, 'class_id', '1'),
(89, 1, 'size_id', '1'),
(91, 1, 'needles_id', '1'),
(93, 1, 'gatunock', '1'),
(94, 1, 'gatunock', '2');

-- --------------------------------------------------------

--
-- Структура таблицы `stor1_remainder`
--

CREATE TABLE `stor1_remainder` (
  `remainder_id` int(10) NOT NULL,
  `bag_id` int(10) DEFAULT NULL,
  `coming_date` date DEFAULT NULL,
  `consumption_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `stor1_remainder`
--

INSERT INTO `stor1_remainder` (`remainder_id`, `bag_id`, `coming_date`, `consumption_date`) VALUES
(4, 30, '2020-03-03', NULL),
(5, 31, '2020-03-03', '2020-03-04'),
(6, 32, '2020-03-02', '2020-03-04');

-- --------------------------------------------------------

--
-- Структура таблицы `stor2_remainder`
--

CREATE TABLE `stor2_remainder` (
  `remainder_id` int(10) NOT NULL,
  `bag_id` int(10) DEFAULT NULL,
  `coming_date` date DEFAULT NULL,
  `consumption_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `stor2_remainder`
--

INSERT INTO `stor2_remainder` (`remainder_id`, `bag_id`, `coming_date`, `consumption_date`) VALUES
(7, 51, '2020-03-06', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `store1_coming`
--

CREATE TABLE `store1_coming` (
  `coming_id` int(10) NOT NULL,
  `bag_id` int(10) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `machines_id` int(10) DEFAULT NULL,
  `master_id` int(10) DEFAULT NULL,
  `knitting_id` int(10) DEFAULT NULL,
  `curent_date` date DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `store1_coming`
--

INSERT INTO `store1_coming` (`coming_id`, `bag_id`, `date`, `machines_id`, `master_id`, `knitting_id`, `curent_date`, `user_id`) VALUES
(2, 12, '2020-02-25', 2, 1, 2, '2020-02-25', 1),
(3, 19, '2020-02-25', 2, 1, 1, '2020-02-25', 1),
(5, 21, '2020-02-25', 2, 1, 1, '2020-02-25', 1),
(6, 22, '2020-02-25', 2, 1, 2, '2020-02-25', 1),
(8, 24, '2020-02-25', 4, 1, 2, '2020-02-26', 1),
(9, 25, '2020-02-28', 2, 1, 2, '2020-02-28', 1),
(10, 26, '2020-02-28', 2, 1, 2, '2020-02-28', 1),
(14, 30, '2020-03-03', 2, 1, 2, '2020-03-03', 1),
(15, 31, '2020-03-03', 2, 1, 2, '2020-03-03', 1),
(16, 32, '2020-03-02', 2, 1, 2, '2020-03-03', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `store1_consumption`
--

CREATE TABLE `store1_consumption` (
  `consumption_id` int(10) NOT NULL,
  `bag_id` int(10) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `area` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `accepted` int(10) DEFAULT NULL,
  `curent_date` date DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `store1_consumption`
--

INSERT INTO `store1_consumption` (`consumption_id`, `bag_id`, `date`, `area`, `accepted`, `curent_date`, `user_id`) VALUES
(13, 11, '2020-02-28', 'Склад 2', 3, '2020-02-28', 1),
(15, 12, '2020-02-28', 'Склад 2', 2, '2020-02-28', 1),
(18, 31, '2020-03-04', 'Склад 2', 2, '2020-03-04', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `store2_coming`
--

CREATE TABLE `store2_coming` (
  `coming_id` int(10) NOT NULL,
  `bag_id` int(10) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `area` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL,
  `accepted` int(10) DEFAULT NULL,
  `curent_date` date DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `store2_coming`
--

INSERT INTO `store2_coming` (`coming_id`, `bag_id`, `date`, `area`, `accepted`, `curent_date`, `user_id`) VALUES
(13, 11, '2020-02-28', 'Склад 2', 3, '2020-02-28', 1),
(15, 12, '2020-02-28', 'Склад 2', 2, '2020-02-28', 1),
(18, 31, '2020-03-04', 'Склад 1', 2, '2020-03-04', 1),
(19, 35, NULL, NULL, NULL, NULL, NULL),
(20, 33, NULL, NULL, NULL, NULL, NULL),
(21, 33, NULL, NULL, NULL, NULL, NULL),
(22, 39, NULL, NULL, NULL, NULL, NULL),
(23, 42, NULL, NULL, NULL, NULL, 1),
(24, 43, '2020-03-06', NULL, NULL, NULL, 1),
(25, 45, '2020-03-06', 'asd', NULL, NULL, 1),
(26, 46, '2020-03-06', 'Внутрішня', NULL, NULL, 1),
(27, 48, '2020-03-06', 'Внутрішня ділянка', NULL, NULL, 1),
(28, 49, '2020-03-06', 'Внутрішня ділянка', 2, '2020-03-06', 1),
(29, 50, '2020-03-06', 'Внутрішня ділянка', 2, '2020-03-06', 1),
(30, 51, '2020-03-06', 'Внутрішня ділянка', 2, '2020-03-06', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `store2_consumption`
--

CREATE TABLE `store2_consumption` (
  `consumption_id` int(10) NOT NULL,
  `bag_id` int(10) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `area` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL,
  `accepted` int(10) DEFAULT NULL,
  `curent_date` date DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`) VALUES
(1, 'admin', '$2y$10$rpi9V.VnSULzcbhbNBoa5.HvZdndOhU.Z3E2kUiTu3bJ5ss4nHUFO', '2020-01-20 16:08:46'),
(2, 'test1', '$2y$10$yGgITDFobu0vTb9LbGX/de1Al/MepxZIIVygqbJiCkTcTrBZYPPRS', '2020-02-04 15:32:04');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `bag_table`
--
ALTER TABLE `bag_table`
  ADD PRIMARY KEY (`bag_id`);

--
-- Индексы таблицы `dov_article`
--
ALTER TABLE `dov_article`
  ADD PRIMARY KEY (`article_id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `assortment_id` (`assortment_id`),
  ADD KEY `class_id` (`class_id`),
  ADD KEY `season_id` (`season_id`);

--
-- Индексы таблицы `dov_assortment`
--
ALTER TABLE `dov_assortment`
  ADD PRIMARY KEY (`assortment_id`);

--
-- Индексы таблицы `dov_catalogs_colors`
--
ALTER TABLE `dov_catalogs_colors`
  ADD PRIMARY KEY (`catalog_id`),
  ADD KEY `cmain_color_id` (`cmain_color_id`),
  ADD KEY `catalog_color_id` (`catalog_color_id`);

--
-- Индексы таблицы `dov_catalog_color`
--
ALTER TABLE `dov_catalog_color`
  ADD PRIMARY KEY (`color_id`);

--
-- Индексы таблицы `dov_class`
--
ALTER TABLE `dov_class`
  ADD PRIMARY KEY (`class_id`);

--
-- Индексы таблицы `dov_color`
--
ALTER TABLE `dov_color`
  ADD PRIMARY KEY (`color_id`);

--
-- Индексы таблицы `dov_employees`
--
ALTER TABLE `dov_employees`
  ADD PRIMARY KEY (`employees_id`);

--
-- Индексы таблицы `dov_inches`
--
ALTER TABLE `dov_inches`
  ADD PRIMARY KEY (`inches_id`);

--
-- Индексы таблицы `dov_knitting_machines`
--
ALTER TABLE `dov_knitting_machines`
  ADD PRIMARY KEY (`machines_id`);

--
-- Индексы таблицы `dov_materials_buyers`
--
ALTER TABLE `dov_materials_buyers`
  ADD PRIMARY KEY (`buyers_id`);

--
-- Индексы таблицы `dov_materials_manufacturers`
--
ALTER TABLE `dov_materials_manufacturers`
  ADD PRIMARY KEY (`manufacturers_id`);

--
-- Индексы таблицы `dov_materials_name`
--
ALTER TABLE `dov_materials_name`
  ADD PRIMARY KEY (`name_id`);

--
-- Индексы таблицы `dov_materials_size`
--
ALTER TABLE `dov_materials_size`
  ADD PRIMARY KEY (`size_id`);

--
-- Индексы таблицы `dov_materials_suppliers`
--
ALTER TABLE `dov_materials_suppliers`
  ADD PRIMARY KEY (`suppliers_id`);

--
-- Индексы таблицы `dov_materials_type`
--
ALTER TABLE `dov_materials_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Индексы таблицы `dov_materials_unite`
--
ALTER TABLE `dov_materials_unite`
  ADD PRIMARY KEY (`unite_id`);

--
-- Индексы таблицы `dov_models`
--
ALTER TABLE `dov_models`
  ADD PRIMARY KEY (`model_id`);

--
-- Индексы таблицы `dov_needles`
--
ALTER TABLE `dov_needles`
  ADD PRIMARY KEY (`needles_id`);

--
-- Индексы таблицы `dov_operation`
--
ALTER TABLE `dov_operation`
  ADD PRIMARY KEY (`operation_id`);

--
-- Индексы таблицы `dov_packing_materials`
--
ALTER TABLE `dov_packing_materials`
  ADD PRIMARY KEY (`packing_materials_id`);

--
-- Индексы таблицы `dov_packing_materials_consumption`
--
ALTER TABLE `dov_packing_materials_consumption`
  ADD PRIMARY KEY (`consumption_id`);

--
-- Индексы таблицы `dov_packing_materials_provider`
--
ALTER TABLE `dov_packing_materials_provider`
  ADD PRIMARY KEY (`provider_id`);

--
-- Индексы таблицы `dov_packing_materials_term`
--
ALTER TABLE `dov_packing_materials_term`
  ADD PRIMARY KEY (`term_id`);

--
-- Индексы таблицы `dov_picture`
--
ALTER TABLE `dov_picture`
  ADD PRIMARY KEY (`picture_id`);

--
-- Индексы таблицы `dov_price`
--
ALTER TABLE `dov_price`
  ADD PRIMARY KEY (`price_id`);

--
-- Индексы таблицы `dov_season`
--
ALTER TABLE `dov_season`
  ADD PRIMARY KEY (`season_id`);

--
-- Индексы таблицы `dov_size`
--
ALTER TABLE `dov_size`
  ADD PRIMARY KEY (`size_id`);

--
-- Индексы таблицы `dov_type`
--
ALTER TABLE `dov_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Индексы таблицы `employees_meta`
--
ALTER TABLE `employees_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Индексы таблицы `packing_materials_meta`
--
ALTER TABLE `packing_materials_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Индексы таблицы `packing_materials_term_meta`
--
ALTER TABLE `packing_materials_term_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Индексы таблицы `price_meta`
--
ALTER TABLE `price_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Индексы таблицы `stor1_remainder`
--
ALTER TABLE `stor1_remainder`
  ADD PRIMARY KEY (`remainder_id`);

--
-- Индексы таблицы `stor2_remainder`
--
ALTER TABLE `stor2_remainder`
  ADD PRIMARY KEY (`remainder_id`);

--
-- Индексы таблицы `store1_coming`
--
ALTER TABLE `store1_coming`
  ADD PRIMARY KEY (`coming_id`);

--
-- Индексы таблицы `store1_consumption`
--
ALTER TABLE `store1_consumption`
  ADD PRIMARY KEY (`consumption_id`);

--
-- Индексы таблицы `store2_coming`
--
ALTER TABLE `store2_coming`
  ADD PRIMARY KEY (`coming_id`);

--
-- Индексы таблицы `store2_consumption`
--
ALTER TABLE `store2_consumption`
  ADD PRIMARY KEY (`consumption_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `bag_table`
--
ALTER TABLE `bag_table`
  MODIFY `bag_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT для таблицы `dov_article`
--
ALTER TABLE `dov_article`
  MODIFY `article_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `dov_assortment`
--
ALTER TABLE `dov_assortment`
  MODIFY `assortment_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `dov_catalogs_colors`
--
ALTER TABLE `dov_catalogs_colors`
  MODIFY `catalog_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `dov_catalog_color`
--
ALTER TABLE `dov_catalog_color`
  MODIFY `color_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `dov_class`
--
ALTER TABLE `dov_class`
  MODIFY `class_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `dov_color`
--
ALTER TABLE `dov_color`
  MODIFY `color_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `dov_employees`
--
ALTER TABLE `dov_employees`
  MODIFY `employees_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `dov_inches`
--
ALTER TABLE `dov_inches`
  MODIFY `inches_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `dov_knitting_machines`
--
ALTER TABLE `dov_knitting_machines`
  MODIFY `machines_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `dov_materials_buyers`
--
ALTER TABLE `dov_materials_buyers`
  MODIFY `buyers_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `dov_materials_manufacturers`
--
ALTER TABLE `dov_materials_manufacturers`
  MODIFY `manufacturers_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `dov_materials_name`
--
ALTER TABLE `dov_materials_name`
  MODIFY `name_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `dov_materials_size`
--
ALTER TABLE `dov_materials_size`
  MODIFY `size_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `dov_materials_suppliers`
--
ALTER TABLE `dov_materials_suppliers`
  MODIFY `suppliers_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `dov_materials_type`
--
ALTER TABLE `dov_materials_type`
  MODIFY `type_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `dov_materials_unite`
--
ALTER TABLE `dov_materials_unite`
  MODIFY `unite_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `dov_models`
--
ALTER TABLE `dov_models`
  MODIFY `model_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `dov_needles`
--
ALTER TABLE `dov_needles`
  MODIFY `needles_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `dov_operation`
--
ALTER TABLE `dov_operation`
  MODIFY `operation_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `dov_packing_materials`
--
ALTER TABLE `dov_packing_materials`
  MODIFY `packing_materials_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `dov_packing_materials_consumption`
--
ALTER TABLE `dov_packing_materials_consumption`
  MODIFY `consumption_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `dov_packing_materials_provider`
--
ALTER TABLE `dov_packing_materials_provider`
  MODIFY `provider_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `dov_packing_materials_term`
--
ALTER TABLE `dov_packing_materials_term`
  MODIFY `term_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `dov_picture`
--
ALTER TABLE `dov_picture`
  MODIFY `picture_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `dov_price`
--
ALTER TABLE `dov_price`
  MODIFY `price_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `dov_season`
--
ALTER TABLE `dov_season`
  MODIFY `season_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `dov_size`
--
ALTER TABLE `dov_size`
  MODIFY `size_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `dov_type`
--
ALTER TABLE `dov_type`
  MODIFY `type_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `employees_meta`
--
ALTER TABLE `employees_meta`
  MODIFY `meta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT для таблицы `packing_materials_meta`
--
ALTER TABLE `packing_materials_meta`
  MODIFY `meta_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `packing_materials_term_meta`
--
ALTER TABLE `packing_materials_term_meta`
  MODIFY `meta_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `price_meta`
--
ALTER TABLE `price_meta`
  MODIFY `meta_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT для таблицы `stor1_remainder`
--
ALTER TABLE `stor1_remainder`
  MODIFY `remainder_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `stor2_remainder`
--
ALTER TABLE `stor2_remainder`
  MODIFY `remainder_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `store1_coming`
--
ALTER TABLE `store1_coming`
  MODIFY `coming_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `store1_consumption`
--
ALTER TABLE `store1_consumption`
  MODIFY `consumption_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT для таблицы `store2_coming`
--
ALTER TABLE `store2_coming`
  MODIFY `coming_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT для таблицы `store2_consumption`
--
ALTER TABLE `store2_consumption`
  MODIFY `consumption_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `dov_article`
--
ALTER TABLE `dov_article`
  ADD CONSTRAINT `dov_article_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `dov_type` (`type_id`),
  ADD CONSTRAINT `dov_article_ibfk_3` FOREIGN KEY (`assortment_id`) REFERENCES `dov_assortment` (`assortment_id`),
  ADD CONSTRAINT `dov_article_ibfk_4` FOREIGN KEY (`class_id`) REFERENCES `dov_class` (`class_id`),
  ADD CONSTRAINT `dov_article_ibfk_5` FOREIGN KEY (`season_id`) REFERENCES `dov_season` (`season_id`);

--
-- Ограничения внешнего ключа таблицы `dov_catalogs_colors`
--
ALTER TABLE `dov_catalogs_colors`
  ADD CONSTRAINT `dov_catalogs_colors_ibfk_1` FOREIGN KEY (`cmain_color_id`) REFERENCES `dov_color` (`color_id`),
  ADD CONSTRAINT `dov_catalogs_colors_ibfk_2` FOREIGN KEY (`catalog_color_id`) REFERENCES `dov_catalog_color` (`color_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
